package util;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

public class CheckboxListUils {
	
	public static <T> void initCheckboxFeature(JList<CheckListItem<T>> list) {
		list.setModel(new DefaultListModel<CheckListItem<T>>());
		list.setCellRenderer(new CheckListRenderer<>());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
				int index = list.locationToIndex(event.getPoint());
				CheckListItem<T> item = (CheckListItem<T>) list.getModel().getElementAt(index);
				item.setSelected(!item.isSelected());
				list.repaint(list.getCellBounds(index, index));
			}
		});
	}
	
	public static <T> void addElement(JList<CheckListItem<T>> list, T newElement) {
		((DefaultListModel<CheckListItem<T>>) list.getModel()).addElement(new CheckListItem<T>(newElement));
	}
	
	public static <T> void addElement(JList<CheckListItem<T>> list, T newElement,boolean isSelected) {
		CheckListItem<T> checkListItem = new CheckListItem<T>(newElement);
		checkListItem.setSelected(isSelected);
		((DefaultListModel<CheckListItem<T>>) list.getModel()).addElement(checkListItem);
	}
	
	public static <T> List<T> getCheckedValues(JList<CheckListItem<T>> list) {
		LinkedList<T> checkedValues= new LinkedList<>();
		DefaultListModel<CheckListItem<T>> model=(DefaultListModel<CheckListItem<T>>) list.getModel();
		for (int i=0;i<model.size();i++) {
			if (model.get(i).isSelected) {
				checkedValues.add(model.get(i).item);
			}
		}
		return checkedValues;
	}
	
	public static class CheckListItem<T> {

		private T item;
		private boolean isSelected = false;

		public CheckListItem(T label) {
			this.item = label;
		}

		public boolean isSelected() {
			return isSelected;
		}

		public void setSelected(boolean isSelected) {
			this.isSelected = isSelected;
		}

		@Override
		public String toString() {
			return item.toString();
		}
	}

	private static class CheckListRenderer<T> extends JCheckBox implements ListCellRenderer<CheckListItem<T>> {

		public Component getListCellRendererComponent(JList<? extends CheckListItem<T>> list, CheckListItem<T> value, int index, boolean isSelected, boolean hasFocus) {
			setEnabled(list.isEnabled());
			setSelected(((CheckListItem<T>) value).isSelected());
			setFont(list.getFont());
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			setText(value.toString());
			return this;
		}	
	}
}
