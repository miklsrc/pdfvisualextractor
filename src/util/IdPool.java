package util;

import java.util.HashSet;

public class IdPool {
	private  HashSet<Integer> inUse = new HashSet<>();
	private  HashSet<Integer> free = new HashSet<>();
	private  int counter = 0;
	
	public int getId() {
		if (free.size()>0) {
			Integer next = free.iterator().next();
			free.remove(next);
			inUse.add(next);
			return next;
		} else {
			counter++;
			inUse.add(counter);
			return counter;
		}
	}
	
	public void freeId(int id) {
		inUse.remove(id);
		free.add(id);
	}
	public void clear() {
		inUse.clear();
		free.clear();
		counter=0;
	}
	

}
