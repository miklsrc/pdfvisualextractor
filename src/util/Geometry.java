package util;

import java.awt.Rectangle;
import java.util.List;

public class Geometry {

	public static Rectangle merge(List<Rectangle> rects) {
		Rectangle boundingBox=null;
		for (Rectangle pdfImage : rects) {
			if (boundingBox==null)
			{
				boundingBox=pdfImage;
			}
			boundingBox = boundingBox.union(pdfImage);
		}
		return boundingBox;
	}
	
	public static boolean verticalIntersect(Rectangle rect1, Rectangle rect2,int toleranceY)
	{
		return (rect1.y < rect2.y + rect2.getHeight() + toleranceY && rect2.y < rect1.y + rect1.getHeight() + toleranceY);
	}
	
	public static boolean verticalIntersect(Rectangle rect1, Rectangle rect2)
	{
		return verticalIntersect(rect1,rect2,0);
	}
	
	public static Rectangle scale(Rectangle rect, double scale)
	{
		return new Rectangle((int)(rect.x*scale),(int)(rect.y*scale),(int)(rect.width*scale),(int)(rect.height*scale));
	
	}
}
