package util.interfaces;

import java.util.Collection;

public interface Collector<ReturnType,ParameterType> {

	Collection<ReturnType> collect(ParameterType parameter);
}
