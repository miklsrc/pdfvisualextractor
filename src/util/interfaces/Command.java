package util.interfaces;

public interface Command {
	public void execute();
}
