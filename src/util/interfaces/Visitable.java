package util.interfaces;

public interface Visitable<T> {
	void visit(T o);
}