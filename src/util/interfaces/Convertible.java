package util.interfaces;

public interface Convertible<Source,Destination> {
	Destination convert(Source source);
}