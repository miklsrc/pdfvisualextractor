package util.interfaces;

public interface Checker<T> {
	boolean check(T parameter);
}
