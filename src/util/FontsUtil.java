package util;

import java.awt.Font;
import java.awt.GraphicsEnvironment;

public class FontsUtil {

	public static void installedFonts() {
		Font[] allFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();

		for (int i = 0; i < allFonts.length; i++) {
			Font font = allFonts[i];

			System.out.println(font.getName() + " " + font.getFontName() + " " + font.getPSName());
		}
	}
}
