package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtil {
	
	public static List<Integer> otherWhitespaceCodes = new ArrayList<>(Arrays.asList(160));  //160=NO-BREAK SPACE, http://www.fileformat.info/info/unicode/char/00a0/index.htm
	
	//alternative: String.replaceAll("^\\s+", "");
	public static String trimFront(String input) {
	    if (input == null) return input;
	    for (int i = 0; i < input.length(); i++) {
	        int ch = input.codePointAt(i);
			if (!isWhitespace(ch)){
	            String str = input.substring(i);
				return str;
			}
	    }
	    return "";
		
//		return input.replaceAll("^\\s+", "");
	}
	
	public static String trimEnd(String input) {
	    if (input == null) return input;
	    for (int i = input.length()-1; i >= 0; i--) {
	        int ch = input.codePointAt(i);
			if (!isWhitespace(ch))
	            return input.substring(0,i+1);
	    }
	    return "";
		
//		return input.replaceAll("^\\s+", "");
	}
	


	public static String trim(String s){
		return trimFront(trimEnd(s));
	}

	public static String replaceWhiteSpaces(String str,char replacement){
		for (Integer whitespaceCode : otherWhitespaceCodes) {
			str=str.replace((char)whitespaceCode.intValue(), replacement);
		}
		str=str.replaceAll("\\s+", replacement+"");
		str= str.replaceAll("\\p{Cntrl}", replacement+"");
		return str;
	}
	
	public static String removeWhiteSpaces(String str){
		for (Integer whitespaceCode : otherWhitespaceCodes) {
			str=str.replace((char)whitespaceCode.intValue(), ' ');
		}
		str=str.replaceAll("\\s+", "");
		str= str.replaceAll("\\p{Cntrl}", "");
		return str;
	}
	
	public static boolean isWhitespace(int code) {
		boolean ws=Character.isWhitespace(code);
		ws = ws || code<32; //control char
		for (Integer whitespaceCode : otherWhitespaceCodes) {
			ws = ws || code==whitespaceCode.intValue();
		}
		return ws;
	}
	
	public static boolean isVisibleString(String str) {
		for (int i = 0; i < str.length(); i++) {
			int code = str.codePointAt(i);
			if (!StringUtil.isWhitespace(code)) {
				return true;
			}
		}
		return false;
	}
}
