package util;

import java.util.LinkedList;

import util.interfaces.Checker;
import util.interfaces.Convertible;

public class MatrixUtils {

	public static <T> LinkedList<Integer> searchSpecificRows(T[][] costMatrix, Checker<T> specificChecker) {
		return seachSpecificSequence(costMatrix, specificChecker, true);
	}

	public static <T> LinkedList<Integer> searchSpecificCols(T[][] costMatrix, Checker<T> specificChecker) {
		return seachSpecificSequence(costMatrix, specificChecker, false);
	}

	private static <T> LinkedList<Integer> seachSpecificSequence(T[][] costMatrix, Checker<T> specificChecker, boolean isRow) {
		int dim1 = 0;
		int dim2 = 0;
		if (isRow == true) {
			dim1 = costMatrix.length;
			dim2 = costMatrix[0].length;
		} else {
			dim1 = costMatrix[0].length;
			dim2 = costMatrix.length;
		}

		LinkedList<Integer> emptys = new LinkedList<>();

		for (int cnt1 = 0; cnt1 < dim1; cnt1++) {
			boolean allValuesAreSpecific = true;
			for (int cnt2 = 0; cnt2 < dim2; cnt2++) {
				T value;
				if (isRow == true) {
					value = costMatrix[cnt1][cnt2];
				} else {
					value = costMatrix[cnt2][cnt1];
				}
				// check if value is 0
				if (!specificChecker.check(value)) {
					allValuesAreSpecific = false;
					break;
				}
			}
			if (allValuesAreSpecific) {
				emptys.add(cnt1);
			}
		}
		return emptys;
	}

	/**
	 * shrinks a array by removing specific rows and cols
	 * 
	 * !IMPORTANT!
	 * shrinkedMAtrix must have this size: [ sourceMatrix.length - removeRows.size()][ sourceMatrix[0].length - removeCols.size()]
	 * 
	 * @param sourceMatrix
	 * @param shrinkedMatrix
	 * @param removeRows
	 * @param removeCols
	 */
	public static <T> void shrinkMatrix(T[][] sourceMatrix, T[][] shrinkedMatrix, LinkedList<Integer> removeRows, LinkedList<Integer> removeCols) {
		int maxRows = sourceMatrix.length;
		int maxCols = sourceMatrix[0].length;

		int removedRowsCnt = 0;
		int removedColsCnt = 0;

		for (int row = 0; row < maxRows; row++) {
			if (removeRows.contains(row)) {
				removedRowsCnt++;
				continue;
			}
			removedColsCnt = 0;
			for (int col = 0; col < maxCols; col++) {
				if (removeCols.contains(col)) {
					removedColsCnt++;
					continue;
				}
				shrinkedMatrix[row - removedRowsCnt][col - removedColsCnt] = sourceMatrix[row][col];
			}

		}
	}
	
	public static Object[][] convertToObjectArray(double[][] doubleMatrix) {
		int lengthX = doubleMatrix.length;
		int lengthY = doubleMatrix[0].length;
		Object objMatrix[][] = new Object[lengthX][lengthY];

		for (int x = 0; x < lengthX; x++) {
			for (int y = 0; y < lengthY; y++) {

				objMatrix[x][y] =new Double(doubleMatrix[x][y]);
			}
		}
		return objMatrix;
	}
	
	public static double[][] transposeMatrix(double [][] m){
        double[][] temp = new double[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	
	public static <T> double[][] convertToPrimitveDoubleArray(T[][] objectMatrix,Convertible<T, Double> doubleConverter) {
		int lengthX = objectMatrix.length;
		int lengthY = objectMatrix[0].length;
		double objMatrix[][] = new double[lengthX][lengthY];

		for (int x = 0; x < lengthX; x++) {
			for (int y = 0; y < lengthY; y++) {

				objMatrix[x][y] =doubleConverter.convert(objectMatrix[x][y]).doubleValue();
			}
		}
		return objMatrix;
	}
}
