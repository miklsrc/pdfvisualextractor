package util;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.pdfbox.util.Matrix;

public class Debug {

	
	 public static boolean equals(double a, double b, double eps) {
		    if (a==b) return true;
		    return Math.abs(a - b) < eps;
		  }
	static int cnt=0;
	
	
	public static boolean toString(Object o)
	{
		System.out.println(o);
		return true;
	}
	
	public static int saveImg(BufferedImage bi)
	{
		cnt++;
		File outputfile = new File("d:/imgs/2/image"+cnt+".jpg");

		try {
			ImageIO.write(bi, "jpg", outputfile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return cnt;
	
	}
	
	static int cnt2=0;
	public static boolean printImg(Graphics2D graphics2d,BufferedImage bi,AffineTransform transfor)
	{
		cnt2++;
		BufferedImage img = new BufferedImage(842,595,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		g.rotate(Math.toRadians(90));  
		//g.translate(0, 595);
        g.scale(1, -1);
        
        Rectangle rectangle = new Rectangle(0, 0,bi.getWidth(), bi.getHeight());
        Shape tshape = transfor.createTransformedShape(rectangle);
		System.out.println("bounds "+cnt2+":"+tshape.getBounds2D());
		
		AffineTransform tx = new AffineTransform();
		tx.translate(-595/2,-842/2);
		tshape = tx.createTransformedShape(tshape);
		System.out.println("after move:"+tshape.getBounds2D());
		
		tx = new AffineTransform();
		tx.rotate(Math.toRadians(90),0,0);
		tshape = tx.createTransformedShape(tshape);
		System.out.println("after rotate:"+tshape.getBounds2D());
		
		tx = AffineTransform.getScaleInstance(-1, 1);
		tshape = tx.createTransformedShape(tshape);
		
		tx = new AffineTransform();
		tx.translate(+842/2,+595/2);
		tshape = tx.createTransformedShape(tshape);
		
		System.out.println("after remove:"+tshape.getBounds2D());
		

		System.out.println("bounds -:"+tshape.getBounds2D());
      
        
		g.drawImage(bi, transfor, null);

		g.dispose();
		File outputfile = new File("d:/imgs/3/image"+cnt2+".jpg");

		try {
			ImageIO.write(img, "jpg", outputfile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return true;
	}
	
	public static boolean printMatrix(Matrix matrix)
	{
		System.out.println(matrix.getShearX());
		System.out.println(matrix.getShearY());
		return true;
	}
	
	public static boolean log(String s)
	{
		System.out.println(s);
		return true;
	}
}
