package util;


public class SwingUtil {
	
	public static interface SwingAction {
		public void executeSwingInteraction();
	}
	
	public static void invokeLater(SwingAction sa)
	{
		java.awt.EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				sa.executeSwingInteraction();
			}
		});
	}
	
}
