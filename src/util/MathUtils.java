package util;

import java.util.List;

public class MathUtils {

	public static boolean isNear(int value1, int value2, int tolerance) {
		return value1 > value2 - tolerance && value1 < value2 + tolerance;
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static double calcStandardDeviation(List<Double> data) {
		// double[] data = {10.0,20.0,30.0,40.0,50.0,60.0,70.0,80,0,90.0,100.0};

		// The mean average
		double mean = 0.0;
		for (int i = 0; i < data.size(); ++i) {
			mean += data.get(i);
		}
		mean /= data.size();

		// The variance
		double variance=0;
		for (int i = 0; i < data.size(); ++i) {
			variance += (data.get(i) - mean) * (data.get(i) - mean);
		}
		variance /= data.size();

		// Standard Deviation
		double std = Math.sqrt(variance);
		
		return std;
	}
}
