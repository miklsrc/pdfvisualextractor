package util;

import java.awt.geom.Rectangle2D;
import java.util.List;

import org.apache.pdfbox.text.TextPosition;

public class Pdf {

	public static Rectangle2D.Float calculateBoundingBox(List<TextPosition> textPositions) {
		Rectangle2D.Float boundingRect = null;
		for (TextPosition textPos : textPositions) {
			Rectangle2D.Float rect = new Rectangle2D.Float(textPos.getX(), textPos.getY() - textPos.getHeightDir(), textPos.getWidth(), textPos.getHeightDir());
			if (boundingRect == null) {
				boundingRect = rect;
			}
			Rectangle2D.Float.union(boundingRect, rect, boundingRect);
		}
		return boundingRect;
	}
}
