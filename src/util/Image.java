package util;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class Image {

	public static BufferedImage convertRenderedImage(RenderedImage img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}
		ColorModel cm = img.getColorModel();
		int width = img.getWidth();
		int height = img.getHeight();
		WritableRaster raster = cm.createCompatibleWritableRaster(width, height);
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		Hashtable properties = new Hashtable();
		String[] keys = img.getPropertyNames();
		if (keys != null) {
			for (int i = 0; i < keys.length; i++) {
				properties.put(keys[i], img.getProperty(keys[i]));
			}
		}
		BufferedImage result = new BufferedImage(cm, raster, isAlphaPremultiplied, properties);
		img.copyData(raster);
		return result;
	}

	public static BufferedImage rotate(BufferedImage image, double angle) {
		double sin = Math.abs(Math.sin(angle)), cos = Math.abs(Math.cos(angle));
		int w = image.getWidth(), h = image.getHeight();
		int neww = (int) Math.floor(w * cos + h * sin), newh = (int) Math.floor(h * cos + w * sin);
		GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
		BufferedImage result = gc.createCompatibleImage(neww, newh, Transparency.TRANSLUCENT);
		Graphics2D g = result.createGraphics();
		g.translate((neww - w) / 2, (newh - h) / 2);
		g.rotate(angle, w / 2, h / 2);
		g.drawRenderedImage(image, null);
		g.dispose();
		return result;
	}

	public static Rectangle toRect(PDImageXObject pdImageXObject) {
		return new Rectangle(0, 0, pdImageXObject.getWidth(), pdImageXObject.getHeight());
	}

	public static BufferedImage dropAlphaChannel(BufferedImage src) {
		BufferedImage convertedImg = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
		convertedImg.getGraphics().drawImage(src, 0, 0, null);

		return convertedImg;
	}

	public static BufferedImage getSubImage(BufferedImage pdfImage, Rectangle compositionArea) {
		int protrudeX = (compositionArea.x + compositionArea.width) - pdfImage.getWidth();
		int protrudeY = (compositionArea.y + compositionArea.height) - pdfImage.getHeight();

		if (protrudeX > 0)
			compositionArea.width -= protrudeX;
		if (protrudeY > 0)
			compositionArea.height -= protrudeY;

		if (compositionArea.x < 0)
			compositionArea.x = 0;
		if (compositionArea.y < 0)
			compositionArea.y = 0;

		return pdfImage.getSubimage(compositionArea.x, compositionArea.y, compositionArea.width, compositionArea.height);
	}
}
