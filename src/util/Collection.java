package util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Collection {

	public static <R, V extends R> void addAllToList(List<R> list1, List<V> list2) {
	    list1.addAll(list2);
	}
	
	public static <T> List<T> createOneItemList(T item) {
		LinkedList<T> list = new LinkedList<T>();
		list.add(item);
		return list;		
	}
	
	public static <T> String implode(String glue, java.util.Collection<T> collection) {
	    if (collection == null || collection.isEmpty()) {
	        return "";
	    }
	    Iterator<T> iter = collection.iterator();

	    StringBuilder sb = new StringBuilder();
	    sb.append(iter.next());

	    while (iter.hasNext()) {
	        sb.append(glue).append(iter.next());
	    }
	    return sb.toString();
	}
	

	public static <T> void increaseValue(Map<T,Integer> map, T key) {
		if (!map.containsKey(key))
			map.put(key, 0);
		map.put(key, map.get(key) + 1);
	}

	
	
	
}
