package gui.imagepanel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.HashMap;

import util.IdPool;

public class GeomImagePanel extends ImagePanel {

	public interface Paintable {
		public void paint(double xScale, double yScale, Graphics2D g2d);
	}

	public class ImagePainter implements Paintable {

		Image image;
		int x;
		int y;
		public static final int space=2;

		public ImagePainter(Image image, int x, int y) {
			super();
			this.image = image;
			this.x = x;
			this.y = y;
		}

		public void paint(double xScale, double yScale, Graphics2D g2d) {
			Rectangle actualArea = new Rectangle((int)(x* xScale), (int)(y* yScale), image.getWidth(null)+2*space, image.getHeight(null)+2*space);
			g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2d.setColor(Color.lightGray);
			g2d.fill(actualArea);
			g2d.setColor(Color.BLACK);
			g2d.drawRect(actualArea.x, actualArea.y, actualArea.width, actualArea.height);
			g2d.drawImage(image,(int) (x* xScale+space),(int)( y* yScale+space), null);
		}
	}

	public class RectanglePainter implements Paintable {
		Rectangle rectangle;
		Color color;
		BasicStroke stroke;

		public RectanglePainter(Rectangle rectangle, Color color, BasicStroke stroke) {
			super();
			this.rectangle = rectangle;
			this.color = color;
			this.stroke = stroke;
		}

		public void paint(double xScale, double yScale, Graphics2D g2d) {
			Rectangle actualArea = new Rectangle((int) (rectangle.x * xScale), (int) (rectangle.y * yScale), (int) (rectangle.width * xScale), (int) (rectangle.height * yScale));
			g2d.setStroke(stroke);
			g2d.setColor(color);
			g2d.drawRect(actualArea.x, actualArea.y, actualArea.width, actualArea.height);
		}
	}

	private HashMap<Integer,Paintable> paintables = new HashMap<>();
	private IdPool idPool=new IdPool();
	
	public int addImage(Image img, int x, int y)
	{
		int id = idPool.getId();
		paintables.put(id,new ImagePainter(img, x, y));
		return id;
	}
	
	public void removeGeom(int id) {
		if (paintables.containsKey(id))
		{
			paintables.remove(id);
			idPool.freeId(id);
		}
	}

	public int addRectangle(Rectangle area, Color color, BasicStroke stroke) {
		int id = idPool.getId();
		paintables.put(id,new RectanglePainter(area, color, stroke));
		return id;
	}

	public void clear() {
		paintables.clear();
		idPool.clear();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null) {

			double xScale = (double) getPreferredSize().width / (double) image.getWidth();
			double yScale = (double) getPreferredSize().height / (double) image.getHeight();

			Graphics2D g2d = (Graphics2D) g.create();

			for (Paintable geom : paintables.values()) {
				geom.paint(xScale, yScale, g2d);
			}

		}
	}

}
