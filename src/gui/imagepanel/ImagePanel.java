package gui.imagepanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import gui.imagepanel.size.ImageSizeStrategy;
import gui.imagepanel.size.NoStretchImageSizeStrategy;

public class ImagePanel extends JPanel {

	// //////////////////// mouse hover // ////////////////////

	private class ImageMouseHoverConverter extends MouseAdapter implements MouseMotionListener {
		private boolean mouseHover = false;

		
		
		@Override
		public void mouseClicked(MouseEvent e) {
			if (image == null) {
				return;
			}
			Dimension size = imageSizeStrategy.getSize(image);
			int sizeWidth = size.width;
			int sizeHeight = size.height;
			if (e.getX() <= sizeWidth && e.getY() <= sizeHeight) {
				if (mouseHover == false) {
					mouseHover = true;
				}
				Point imagePoint = convertToImageCoords(new Point(e.getX(),e.getY()));
				imageMouseHoverListeners.forEach(listener -> listener.mouseClicked(imagePoint));
			}
		}

		@Override
		public void mouseExited(MouseEvent e) {
			if (mouseHover) {
				mouseHover = false;
				imageMouseHoverListeners.forEach(listener -> listener.mouseExited());
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			if (image == null) {
				return;
			}
			Dimension size = imageSizeStrategy.getSize(image);
			int sizeWidth = size.width;
			int sizeHeight = size.height;
			if (e.getX() <= sizeWidth && e.getY() <= sizeHeight) {
				if (mouseHover == false) {
					mouseHover = true;
				}
				Point imagePoint = convertToImageCoords(new Point(e.getX(),e.getY()));
				imageMouseHoverListeners.forEach(listener -> listener.mouseMoved(imagePoint));
			} else if (mouseHover) {
				mouseHover = false;
				imageMouseHoverListeners.forEach(listener -> listener.mouseExited());
			}
		}
	}

	private class MouseAreaSelector extends MouseAdapter {
		private Point start = new Point();
		
		public void mouseReleased(MouseEvent e) {
			if (captureRect != null) {
				Rectangle imageRect = convertToImageCoords(captureRect);
				captureRect = null;
				repaint();
				areaSelectionListeners.forEach(listener -> listener.areaSelected(imageRect));

			}
		}
		public void mousePressed(MouseEvent me) {
			start = me.getPoint();
		}
		@Override
		public void mouseDragged(MouseEvent me) {
			Point end = me.getPoint();
			captureRect = new Rectangle(start, new Dimension(end.x - start.x, end.y - start.y));
			repaint();
		}
		
	};
	
	protected BufferedImage image;
	private ImageSizeStrategy imageSizeStrategy = new NoStretchImageSizeStrategy();
	private List<ImageMouseHoverListener> imageMouseHoverListeners = new ArrayList<ImageMouseHoverListener>();
	private List<AreaSelectionListener> areaSelectionListeners = new ArrayList<AreaSelectionListener>();
	private Rectangle captureRect;

	public void addAreaSelectionListener(AreaSelectionListener areaSelectionListener) {
		areaSelectionListeners.add(areaSelectionListener);
	}
	
	public void removeAreaSelectionListener(AreaSelectionListener areaSelectionListener) {
		areaSelectionListeners.remove(areaSelectionListener);
	}
	
	public void addImageMouseListener(ImageMouseHoverListener hoverListener) {
		imageMouseHoverListeners.add(hoverListener);
	}

	public ImagePanel() {
		ImageMouseHoverConverter mouseHoverConverter = new ImageMouseHoverConverter();
		this.addMouseMotionListener(mouseHoverConverter);
		this.addMouseListener(mouseHoverConverter);

		
		MouseAdapter mouseAreaSelector = new MouseAreaSelector();
		this.addMouseListener(mouseAreaSelector);
		this.addMouseMotionListener(mouseAreaSelector);
	}

	public void setImageSizeStrategy(ImageSizeStrategy imageSizeStrategy) {
		this.imageSizeStrategy = imageSizeStrategy;
		forceRepaint();
	}

	public void setImage(BufferedImage bi) {
		image = bi;
		forceRepaint();
	}

	public void forceRepaint() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				repaint();
			}
		});
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null) {
			this.revalidate();
			Dimension size = imageSizeStrategy.getSize(image);
			g.drawImage(image, 0, 0, (int) size.getWidth(), (int) size.getHeight(), null);
			setPreferredSize(size);
		}
		if (captureRect != null) {
			g.setColor(Color.RED);
			((Graphics2D) g).draw(captureRect);
			g.setColor(new Color(255, 255, 255, 150));
			((Graphics2D) g).fill(captureRect);
		}
	}

	private Point convertToImageCoords(Point displayCordsPt)
	{
		Dimension size = imageSizeStrategy.getSize(image);
		int sizeWidth = size.width;
		int sizeHeight = size.height;
		
		final int resizedX = (int) (displayCordsPt.getX() * ((float) image.getWidth() / (float) sizeWidth));
		final int resizedY = (int) (displayCordsPt.getY() * ((float) image.getHeight() / (float) sizeHeight));
		return new Point(resizedX, resizedY);
	}
	
	private Rectangle convertToImageCoords(Rectangle displayCordsRect){
		Point imagePt = convertToImageCoords(new Point(displayCordsRect.x,displayCordsRect.y));
		Point imageDim = convertToImageCoords(new Point(displayCordsRect.width,displayCordsRect.height));
		return new Rectangle(imagePt.x, imagePt.y, imageDim.x, imageDim.y);
		
	}
}