package gui.imagepanel;

import java.awt.Rectangle;

public interface AreaSelectionListener {
	public void areaSelected(Rectangle selection);
}