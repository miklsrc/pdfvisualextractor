package gui.imagepanel;

import java.awt.Point;

public interface ImageMouseHoverListener {
	public void mouseMoved(Point imagePoint);
	public void mouseExited();
	public void mouseClicked(Point imagePoint);
}