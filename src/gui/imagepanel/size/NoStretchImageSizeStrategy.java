package gui.imagepanel.size;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

public class NoStretchImageSizeStrategy implements ImageSizeStrategy {
	public Dimension getSize(BufferedImage image) {
		return new Dimension(image.getWidth(), image.getHeight());
	}
}