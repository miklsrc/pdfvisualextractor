package gui.imagepanel.size;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

public interface ImageSizeStrategy {
	public Dimension getSize(BufferedImage image);
}