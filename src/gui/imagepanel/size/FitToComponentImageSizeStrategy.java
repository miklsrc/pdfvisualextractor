package gui.imagepanel.size;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

public class FitToComponentImageSizeStrategy implements ImageSizeStrategy {

	private Component sizeDefiner;

	public FitToComponentImageSizeStrategy(Component sizeDefiner) {
		this.sizeDefiner = sizeDefiner;
	}

	public Dimension getSize(BufferedImage image) {
		Dimension size = sizeDefiner.getSize();

		double v_fact = size.getHeight() / image.getHeight();
		double h_fact = size.getWidth() / image.getWidth();

		double im_fact = Math.min(v_fact, h_fact);
		size.height = (int) (image.getHeight() * im_fact);
		size.width = (int) (image.getWidth() * im_fact);

		return size;
	}
}