package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.border.EmptyBorder;

public class StringListEditorDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField tfNewItem;
	private JList<String> list = new JList<String>(new DefaultListModel<String>());
	private List<String> items;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {

			List<String> items = new ArrayList<String>();
			items.add("tst");
			items.add("fdsgsfdg");
			items.add("ereeeeee");

			StringListEditorDialog dialog = new StringListEditorDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.bindList(items);

			dialog.setVisible(true);

			System.out.println("----------");
			for (String string : items) {
				System.out.println(string);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bindList(List<String> items) {
		this.items = items;
		listModel().clear();
		for (String item : items) {
			listModel().addElement(item);
		}
	}

	protected void addNewItem() {
		listModel().addElement(tfNewItem.getText());
	}

	/**
	 * Create the dialog.
	 */
	public StringListEditorDialog() {
		this.setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		contentPanel.add(list);
		{
			list.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent e) {
					listKeyPressed(e);
				}
			});
		}
		{
			JPanel pnlAdd = new JPanel();
			contentPanel.add(pnlAdd, BorderLayout.SOUTH);
			pnlAdd.setLayout(new BoxLayout(pnlAdd, BoxLayout.X_AXIS));
			{
				tfNewItem = new JTextField();
				pnlAdd.add(tfNewItem);
				tfNewItem.setColumns(10);
			}
			{
				JButton btnAdd = new JButton("Add");
				pnlAdd.add(btnAdd);
				btnAdd.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						addNewItem();
					}
				});
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						okPressed();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						cancelPressed();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	protected void cancelPressed() {
		dispose();
	}

	protected void okPressed() {
		items.clear();
		ListModel<String> model = list.getModel();

		for (int i = 0; i < model.getSize(); i++) {
			String item = model.getElementAt(i);
			items.add(item);
		}

		dispose();
	}

	protected void listKeyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_DELETE) {
			for (String item : list.getSelectedValuesList())
				listModel().removeElement(item);
		}
	}

	private DefaultListModel<String> listModel() {
		return (DefaultListModel<String>) list.getModel();
	}

}
