package gui;

import javax.swing.JFrame;

public class PleaseWaitThreadHandler implements ThreadHandler {

	private JFrame frame;
	
	public PleaseWaitThreadHandler(JFrame frame) {
		this.frame = frame;
	}

	public PleaseWaitThreadHandler() {
		this(null);
	}

	@Override
	public void process(Runnable runnable) {

		PleaseWaitDialog pleaseWaitDialog = new PleaseWaitDialog();
		pleaseWaitDialog.setVisible(true);

		Thread thread = new Thread(runnable);
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pleaseWaitDialog.setVisible(false);
		pleaseWaitDialog.dispose();

		if (frame != null) {
			frame.setEnabled(true);
			frame.toFront();
		}
	}

	
}