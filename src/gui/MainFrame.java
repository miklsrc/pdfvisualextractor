package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import gui.imagepanel.GeomImagePanel;
import gui.imagepanel.ImagePanel;
import gui.imagepanel.size.FitToComponentImageSizeStrategy;
import pdf.elements.PdfComposition;
import pdf.elements.PdfImage;
import pdf.elements.PdfLinepath;
import pdf.elements.PdfParagraph;
import pdf.elements.PdfPicture;
import pdf.elements.PdfRectangle;
import pdf.elements.PdfString;

public class MainFrame extends JFrame {

	private GeomImagePanel imgPanel;
	private JButton btnUp;
	private JButton btnDown;
	private JScrollPane imgScrollPane;
	private JPanel panel_1;
	private JPanel pnlElements;
	private JRadioButton rbImgSizeFull;
	private JRadioButton rbImgSizeStretched;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JLabel lblCurrentImg;
	private JButton btnJumpFront;
	private JButton btnJumpEnd;
	private JTextField tfPageNumber;
	private JButton btnJumpToPage;
	private JList<PdfString> listStrings;
	private JPanel pnlStrings;
	private JTabbedPane tabbedPaneElements;
	private JList<PdfParagraph> listParagraphs;
	private JButton btnAddUnionParagrpah;
	private JSplitPane splitPane;
	private JList<PdfImage> listImages;
	private JList<PdfPicture> listPictures;
	private JSplitPane imageSplitPane;
	private ImagePanel imgPreviewPanel;
	private ImagePanel picturePreviewPanel;
	private JPanel pnlHighlightElements;
	private JPanel imgListPanel;
	private JButton btnAddPicture;
	private JSplitPane pictureSplitPane;
	private JButton btnSave;
	private JPanel pnlParagraphs;
	private JButton btnDeleteParagraph;
	private JList<PdfRectangle> listRectangles;
	private JPanel pnlParagraphButtons;
	private JButton btnParagrapgOptions;
	private JPanel panel_2;
	private JButton btnDeletePicture;
	private JButton btnListIndicators;
	private JPanel panel_4;
	private JButton btnAddIndividualParagrpah;
	private JPanel pnlParagraphAnnotations;
	private JPanel pnlCompositions;
	private JList<PdfComposition> listCompositions;
	private JPanel panel_3;
	private JToggleButton btnAddComposition;
	private JSplitPane compositionSplitPane;
	private ImagePanel compositionPreviewPanel;
	private JButton btnDeleteComposition;
	private JPanel panel_5;
	private JPanel pnlCompositionAnnotations;
	private JList<PdfLinepath> listLinepaths;
	private JButton btnScreenshot;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
					frame.setLookAndFeel();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void setLookAndFeel() {
		int lAF = 3;
		String plaf = "";
		if (lAF == 1) {
			plaf = "javax.swing.plaf.metal.MetalLookAndFeel";
		} else if (lAF == 2) {
			plaf = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
		} else if (lAF == 3) {
			plaf = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
		}

		try {
			UIManager.setLookAndFeel(plaf);
			SwingUtilities.updateComponentTreeUI(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 835, 360);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		this.setTitle("PDF-Visual-Extractor � Michael Klenner");
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(panel, BorderLayout.NORTH);

		btnDown = new JButton("<");

		btnJumpFront = new JButton("<<");

		panel.add(btnJumpFront);
		panel.add(btnDown);

		lblCurrentImg = new JLabel("[current page]");
		panel.add(lblCurrentImg);

		btnUp = new JButton(">");
		panel.add(btnUp);

		btnJumpEnd = new JButton(">>");
		panel.add(btnJumpEnd);

		tfPageNumber = new JTextField();
		panel.add(tfPageNumber);
		tfPageNumber.setColumns(10);

		btnJumpToPage = new JButton("->");
		panel.add(btnJumpToPage);

		panel_1 = new JPanel();
		panel.add(panel_1);

		rbImgSizeFull = new JRadioButton("full");
		rbImgSizeFull.setSelected(true);
		buttonGroup.add(rbImgSizeFull);
		panel_1.add(rbImgSizeFull);

		rbImgSizeStretched = new JRadioButton("stretched");
		buttonGroup.add(rbImgSizeStretched);
		panel_1.add(rbImgSizeStretched);

		btnSave = new JButton("save");
		panel.add(btnSave);

		btnListIndicators = new JButton("list indicators");
		panel.add(btnListIndicators);
		
		btnScreenshot = new JButton("screenshot");
		panel.add(btnScreenshot);

		splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);

		pnlElements = new JPanel();
		pnlElements.setLayout(new BorderLayout(0, 0));
		tabbedPaneElements = new JTabbedPane(JTabbedPane.TOP);
		pnlElements.add(tabbedPaneElements, BorderLayout.CENTER);
		splitPane.setRightComponent(pnlElements);

		listStrings = new JList<PdfString>(new DefaultListModel<PdfString>());

		pnlStrings = new JPanel();
		pnlStrings.setLayout(new BorderLayout(0, 0));
		pnlStrings.add(new JScrollPane(listStrings), BorderLayout.CENTER);

		tabbedPaneElements.addTab(PdfString.TYPE_NAME, null, pnlStrings, null);

		panel_4 = new JPanel();
		pnlStrings.add(panel_4, BorderLayout.SOUTH);

		btnAddUnionParagrpah = new JButton("add as Union Paragraph");
		panel_4.add(btnAddUnionParagrpah);
		btnAddUnionParagrpah.setMnemonic('s');

		btnAddIndividualParagrpah = new JButton("add as Individual Paragraphs");
		btnAddIndividualParagrpah.setMnemonic('i');
		panel_4.add(btnAddIndividualParagrpah);

		pnlParagraphs = new JPanel();
		tabbedPaneElements.addTab(PdfParagraph.TYPE_NAME, null, pnlParagraphs, null);
		pnlParagraphs.setLayout(new BorderLayout(0, 0));

		listParagraphs = new JList<PdfParagraph>(new DefaultListModel<PdfParagraph>());
		JScrollPane scrollPane = new JScrollPane(listParagraphs);
		pnlParagraphs.add(scrollPane);

		pnlParagraphButtons = new JPanel();
		pnlParagraphs.add(pnlParagraphButtons, BorderLayout.SOUTH);
		pnlParagraphButtons.setLayout(new BoxLayout(pnlParagraphButtons, BoxLayout.Y_AXIS));

		panel_2 = new JPanel();
		pnlParagraphButtons.add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		btnDeleteParagraph = new JButton("Delete Paragraph");
		panel_2.add(btnDeleteParagraph);

		btnParagrapgOptions = new JButton("Paragraph Options");
		panel_2.add(btnParagrapgOptions);

		pnlParagraphAnnotations = new JPanel();
		pnlParagraphAnnotations.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Annotations", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlParagraphButtons.add(pnlParagraphAnnotations);

		imageSplitPane = new JSplitPane();
		imageSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		tabbedPaneElements.addTab(PdfImage.TYPE_NAME, null, imageSplitPane, null);

		listImages = new JList<PdfImage>(new DefaultListModel<PdfImage>());

		imgPreviewPanel = new ImagePanel();
		imgPreviewPanel.setImageSizeStrategy(new FitToComponentImageSizeStrategy(imgPreviewPanel));

		imgPanel = new GeomImagePanel();
		imgScrollPane = new JScrollPane(imgPanel);

		imgListPanel = new JPanel();
		imgListPanel.setLayout(new BorderLayout(0, 0));
		imgListPanel.add(new JScrollPane(listImages));

		imageSplitPane.setRightComponent(imgPreviewPanel);
		imageSplitPane.setLeftComponent(imgListPanel);

		btnAddPicture = new JButton("add as Picture");
		btnAddPicture.setMnemonic('d');
		imgListPanel.add(btnAddPicture, BorderLayout.SOUTH);
		imageSplitPane.setResizeWeight(0.5);

		{
			pictureSplitPane = new JSplitPane();
			pictureSplitPane.setResizeWeight(0.5);
			pictureSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			tabbedPaneElements.addTab(PdfPicture.TYPE_NAME, null, pictureSplitPane, null);

			picturePreviewPanel = new ImagePanel();
			picturePreviewPanel.setImageSizeStrategy(new FitToComponentImageSizeStrategy(picturePreviewPanel));

			JPanel pnlListPictures = new JPanel();
			pnlListPictures.setLayout(new BorderLayout(0, 0));

			btnDeletePicture = new JButton("Delete Picture");
			pnlListPictures.add(btnDeletePicture, BorderLayout.SOUTH);

			listPictures = new JList<PdfPicture>(new DefaultListModel<PdfPicture>());
			pnlListPictures.add(listPictures);

			pictureSplitPane.setRightComponent(picturePreviewPanel);
			pictureSplitPane.setLeftComponent(pnlListPictures);
		}
		{
			listRectangles = new JList<PdfRectangle>(new DefaultListModel<PdfRectangle>());
			tabbedPaneElements.addTab("Rectangles", null, new JScrollPane(listRectangles), null);
		}
		{
			listLinepaths = new JList<PdfLinepath>(new DefaultListModel<PdfLinepath>());
			tabbedPaneElements.addTab("LinePaths", null, new JScrollPane(listLinepaths), null);
		}
		{
			compositionSplitPane = new JSplitPane();
			compositionSplitPane.setResizeWeight(0.5);
			compositionSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			tabbedPaneElements.addTab(PdfComposition.TYPE_NAME, null, compositionSplitPane, null);

			compositionPreviewPanel = new ImagePanel();
			compositionPreviewPanel.setImageSizeStrategy(new FitToComponentImageSizeStrategy(compositionPreviewPanel));
			compositionSplitPane.setRightComponent(compositionPreviewPanel);

			pnlCompositions = new JPanel();
			pnlCompositions.setLayout(new BorderLayout(0, 0));

			listCompositions = new JList<PdfComposition>(new DefaultListModel<PdfComposition>());
			pnlCompositions.add(new JScrollPane(listCompositions));

			compositionSplitPane.setLeftComponent(pnlCompositions);

			panel_5 = new JPanel();
			pnlCompositions.add(panel_5, BorderLayout.SOUTH);
			panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.Y_AXIS));

			panel_3 = new JPanel();
			panel_5.add(panel_3);

			btnAddComposition = new JToggleButton("Add Composition");
			btnAddComposition.setMnemonic('a');
			panel_3.add(btnAddComposition);

			btnDeleteComposition = new JButton("Delete Composition");
			panel_3.add(btnDeleteComposition);

			pnlCompositionAnnotations = new JPanel();
			pnlCompositionAnnotations.setBorder(new TitledBorder(null, "Annotations", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			panel_5.add(pnlCompositionAnnotations);
		}
		pnlHighlightElements = new JPanel();
		pnlHighlightElements.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Highlight Elements", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlElements.add(pnlHighlightElements, BorderLayout.NORTH);
		pnlHighlightElements.setLayout(new GridLayout(0, 4, 0, 0));

		splitPane.setLeftComponent(imgScrollPane);
		splitPane.setResizeWeight(0.7);
	}

	public JButton getBtnParagraphOptions() {
		return btnParagrapgOptions;
	}

	public JList<PdfRectangle> getListRectangles() {
		return listRectangles;
	}

	public JTabbedPane getTabbedPaneElements() {
		return tabbedPaneElements;
	}

	public GeomImagePanel getImagePanel() {
		return imgPanel;
	}

	public JButton getBtnUp() {
		return btnUp;
	}

	public JButton getBtnDown() {
		return btnDown;
	}

	public JScrollPane getImgScrollPane() {
		return imgScrollPane;
	}

	public JRadioButton getRbImgSizeFull() {
		return rbImgSizeFull;
	}

	public JRadioButton getRbImgSizeStretched() {
		return rbImgSizeStretched;
	}

	public JLabel getLblCurrentImg() {
		return lblCurrentImg;
	}

	public JButton getBtnJumpEnd() {
		return btnJumpEnd;
	}

	public JTextField getTfPageNumber() {
		return tfPageNumber;
	}

	public JButton getBtnJumpToPage() {
		return btnJumpToPage;
	}

	public JButton getBtnJumpFront() {
		return btnJumpFront;
	}

	public JList<PdfString> getListStrings() {
		return listStrings;
	}

	public JButton getBtnAddUnionParagrpah() {
		return btnAddUnionParagrpah;
	}

	public JButton getBtnAddIndividualParagrpah() {
		return btnAddIndividualParagrpah;
	}

	public JList<PdfParagraph> getListParagraphs() {
		return listParagraphs;
	}

	public JList<PdfImage> getListImages() {
		return listImages;
	}

	public ImagePanel getImgPreviewPanel() {
		return imgPreviewPanel;
	}

	public JPanel getPnlHighlightElements() {
		return pnlHighlightElements;
	}

	public JButton getBtnAddPicture() {
		return btnAddPicture;
	}

	public JList<PdfPicture> getListPictures() {
		return listPictures;
	}

	public ImagePanel getPicturePreviewPanel() {
		return picturePreviewPanel;
	}

	public JButton getBtnSave() {
		return btnSave;
	}

	public JButton getBtnDeleteParagraph() {
		return btnDeleteParagraph;
	}

	public JButton getBtnDeletePicture() {
		return btnDeletePicture;
	}

	public JButton getBtnListIndicators() {
		return btnListIndicators;
	}

	public JPanel getPnlParagraphAnnotations() {
		return pnlParagraphAnnotations;
	}

	public JToggleButton getBtnAddComposition() {
		return btnAddComposition;
	}

	public JList<PdfComposition> getListCompositions() {
		return listCompositions;
	}

	public ImagePanel getCompositionPreviewPanel() {
		return compositionPreviewPanel;
	}

	public JButton getBtnDeleteComposition() {
		return btnDeleteComposition;
	}

	public JPanel getPnlCompositionAnnotations() {
		return pnlCompositionAnnotations;
	}

	public JList<PdfLinepath> getListLinepaths() {
		return listLinepaths;
	}

	public JButton getBtnScreenshot() {
		return btnScreenshot;
	}

	
	
}
