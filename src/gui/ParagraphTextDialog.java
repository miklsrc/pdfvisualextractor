package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;

import pdf.elements.PdfParagraph;
import pdf.elements.PdfString;

public class ParagraphTextDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private PdfParagraph pdfParagraph;
	private JTextPane tfParagraph;
	private JPanel pnlParagraph;
	private JButton btnManuallyInput;

	public ParagraphTextDialog(PdfParagraph paragraph) {
		this();
		this.pdfParagraph = paragraph;
		refreshUi();
		this.pack();
	}

	public void refreshUi() {
		updateParagraphText();
		createList();
	}

	private void updateParagraphText() {
		tfParagraph.setText(pdfParagraph.getParagraphText());
	}

	private void createList() {
		contentPanel.removeAll();

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		contentPanel.add(panel, BorderLayout.CENTER); // add the parent to the JFrame

		List<JTextField> textFields = new LinkedList<>();
		double mostWidth = 0;

		for (int i = 0; i < pdfParagraph.pdfStrings.size(); i++) {

			if (i >= 1) {
				panel.add(createAdditionalSpacePanel(i));
			}

			PdfString pdfString = pdfParagraph.pdfStrings.get(i);
			JTextField jTextField = new JTextField(pdfString.toString());
			textFields.add(jTextField);
			jTextField.setEditable(false);
			mostWidth = Math.max(jTextField.getPreferredSize().getWidth(), mostWidth);
			panel.add(createStringPanel(i, jTextField));
		}
		for (JTextField jTextField : textFields) {
			Dimension dimension = new Dimension((int) mostWidth + 10, (int) jTextField.getMinimumSize().getHeight());
			jTextField.setPreferredSize(dimension);
		}
		contentPanel.updateUI();
	}

	private JPanel createStringPanel(int i, JTextField jTextField) {
		JPanel pnlTextPart = new JPanel();
		pnlTextPart.setBorder(BorderFactory.createLineBorder(Color.black));
		pnlTextPart.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		pnlTextPart.add(jTextField);
		if (pdfParagraph.pdfStrings.size() > 1) {

			JButton btnUp = new JButton(new ImageIcon("src/img/Up.gif"));
			JButton btnDown = new JButton(new ImageIcon("src/img/Down.gif"));
			JButton btnRemove = new JButton(new ImageIcon("src/img/Delete.gif"));

			pnlTextPart.add(btnUp);
			pnlTextPart.add(btnDown);
			pnlTextPart.add(btnRemove);

			final int index = i;
			btnUp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					moveUp(index);
				}
			});
			btnDown.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					moveDown(index);
				}
			});
			btnRemove.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					removePdfString(index);
				}
			});
		}
		return pnlTextPart;
	}

	private JPanel createAdditionalSpacePanel(int i) {
		JPanel pnlAdditionalSpace = new JPanel();
		pnlAdditionalSpace.setBorder(BorderFactory.createLineBorder(Color.black));
		pnlAdditionalSpace.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));

		boolean additionalSpace = pdfParagraph.additionalSpaces[i - 1];
		JButton btnSwitchSpaceStatus;
		if (additionalSpace) {
			btnSwitchSpaceStatus = new JButton(new ImageIcon("src/img/Remove.gif"));
		} else {
			btnSwitchSpaceStatus = new JButton(new ImageIcon("src/img/Add.gif"));
		}
		btnSwitchSpaceStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pdfParagraph.additionalSpaces[i - 1] = !additionalSpace;
				refreshUi();
			}
		});
		pnlAdditionalSpace.add(btnSwitchSpaceStatus);
		return pnlAdditionalSpace;
	}

	protected void removePdfString(int index) {
		// tricky and ugly "remove element from array" stuff
		List<Integer> list = Arrays.stream(pdfParagraph.pdfStringIds).boxed().collect(Collectors.toList());
		list.remove(index);
		pdfParagraph.pdfStringIds = list.stream().mapToInt(i -> i).toArray();

		pdfParagraph.pdfStrings.remove(index);

		refreshUi();
	}

	protected void moveDown(int index) {
		if (index < pdfParagraph.pdfStringIds.length - 1) {
			swapElements(index, index + 1);
		}
	}

	protected void moveUp(int index) {
		if (index >= 1) {
			swapElements(index, index - 1);
		}
	}

	private void swapElements(int index1, int index2) {
		int temp = pdfParagraph.pdfStringIds[index1];
		pdfParagraph.pdfStringIds[index1] = pdfParagraph.pdfStringIds[index2];
		pdfParagraph.pdfStringIds[index2] = temp;

		PdfString s = pdfParagraph.pdfStrings.get(index1);
		pdfParagraph.pdfStrings.set(index1, pdfParagraph.pdfStrings.get(index2));
		pdfParagraph.pdfStrings.set(index2, s);

		refreshUi();
	}

	/**
	 * Create the dialog.
	 */
	public ParagraphTextDialog() {
		this.setModal(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		getContentPane().add(new JScrollPane(contentPanel), BorderLayout.CENTER);
		contentPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		{
			JPanel pnlButtons = new JPanel();
			pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(pnlButtons, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				pnlButtons.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			pnlParagraph = new JPanel();
			getContentPane().add(pnlParagraph, BorderLayout.NORTH);
			pnlParagraph.setLayout(new BorderLayout(0, 0));
			{
				tfParagraph = new JTextPane();
				pnlParagraph.add(tfParagraph);
				tfParagraph.setEditable(false);
			}

			btnManuallyInput = new JButton("...");
			btnManuallyInput.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					inputOrderManually();
				}
			});
			pnlParagraph.add(btnManuallyInput, BorderLayout.EAST);
		}
	}

	protected void inputOrderManually() {
		String input = JOptionPane.showInputDialog(this, "Type in the String Order manually");

		for (int i = 0; i < pdfParagraph.pdfStrings.size(); i++) {
			int bestSuitableStringId = getBestSuitableString(input, pdfParagraph.pdfStrings, i);
			if (bestSuitableStringId == -1) {
				return;
			}
			String str = pdfParagraph.pdfStrings.get(bestSuitableStringId).string;
			input = input.substring(str.length());
			swapElements(i, bestSuitableStringId);
		}
	}

	private int getBestSuitableString(String input, List<PdfString> pdfStrings, int leftBound) {
		if (input !=null && input.length() > 0) {
			for (int i = leftBound; i < pdfParagraph.pdfStrings.size(); i++) {
				if (input.startsWith(pdfParagraph.pdfStrings.get(i).string)) {
					return i;
				}
			}
		}
		return -1;
	}

}
