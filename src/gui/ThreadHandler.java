package gui;

public interface ThreadHandler {

	public abstract void process(Runnable runnable);

}