package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import util.Colors;
import util.interfaces.Convertible;

public  class TableViewer extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private Convertible<Object,Double> stringConverter;

	private TableViewer(Convertible<Object, Double> stringConverter) {
		this.stringConverter=stringConverter;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		table = new JTable() {
			public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
				Component comp = super.prepareRenderer(renderer, row, col);
				Double value = stringConverter.convert(getModel().getValueAt(row, col));
				comp.setBackground(Colors.interpolate(Color.red,Color.green,value.floatValue()));
				return comp;
			}
		};
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	System.out.println(table.getModel().getValueAt(row, col));
		        }
		    }
		});
		contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
	}


	public JTable getTable() {
		return table;
	}
	
	public static void showInViewer(Object[][] simMatrix,Convertible<Object,Double>  stringConverter) {

		Object[] indices = new Object[simMatrix[0].length];
		Arrays.setAll(indices, i -> i + 1);

		TableViewer tv = new TableViewer(stringConverter);
		DefaultTableModel defaultTableModel = new DefaultTableModel(simMatrix, indices);
		tv.getTable().setModel(defaultTableModel);
		tv.setVisible(true);
	}
}
