package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import evaluation.assignment.ParagraphAssignmentStrategy;
import evaluation.redundancy.RedundancyStrategy;
import evaluation.similarity.metrics.NormalizedStringSimilarity;
import util.CheckboxListUils;
import util.CheckboxListUils.CheckListItem;
import util.KeyValuePair;

public class MaterialsBrowser extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private CellBtnActionListener btnActionListener;
	private JButton btnEvaluate;
	private JComboBox<RedundancyStrategy> cbRedundancyStrategies;
	private JPanel panel_1;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JTextField tfThreshold;
	private JLabel lblNewLabel_2;
	private JComboBox<NormalizedStringSimilarity> cbStringMetrics;
	private JLabel lblIntertextualAssignmentStrategy;
	private JComboBox<ParagraphAssignmentStrategy> cbAssignmentStrategy;
	private JPanel panel_2;
	private JCheckBox chkWriteIconics;
	private JPanel panel_3;
	private JLabel lblNewLabel_3;
	private JList<CheckListItem<String>> listTextProcessors;
	
	public static  interface CellBtnActionListener {
		void clicked(String cellStr);
	}
	
	
	public void setCellBtnActionListener(CellBtnActionListener btnActionListener) {
		this.btnActionListener = btnActionListener;
	}

	/**
	 * Create the frame.
	 * @param data 
	 */
	public MaterialsBrowser(List<KeyValuePair<String, String>> data) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 549, 453);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		createTable(data);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		btnEvaluate = new JButton("evaluate");
		
		panel.add(btnEvaluate, BorderLayout.EAST);
		
		panel_3 = new JPanel();
		panel.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		panel_1 = new JPanel();
		panel_3.add(panel_1, BorderLayout.CENTER);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "verbal text evaluation", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{168, 168, 0};
		gbl_panel_1.rowHeights = new int[]{20, 0, 20, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		lblNewLabel = new JLabel("Redundancy Strategy");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		cbRedundancyStrategies = new JComboBox<>();
		GridBagConstraints gbc_listRedundancyStrategies = new GridBagConstraints();
		gbc_listRedundancyStrategies.fill = GridBagConstraints.BOTH;
		gbc_listRedundancyStrategies.insets = new Insets(0, 0, 5, 0);
		gbc_listRedundancyStrategies.gridx = 1;
		gbc_listRedundancyStrategies.gridy = 0;
		panel_1.add(cbRedundancyStrategies, gbc_listRedundancyStrategies);
		
		lblNewLabel_3 = new JLabel("Text PreProcessors");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 1;
		panel_1.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		listTextProcessors = new JList<>();
		CheckboxListUils.initCheckboxFeature(listTextProcessors);
		GridBagConstraints gbc_listTextProcessors = new GridBagConstraints();
		gbc_listTextProcessors.insets = new Insets(0, 0, 5, 0);
		gbc_listTextProcessors.fill = GridBagConstraints.BOTH;
		gbc_listTextProcessors.gridx = 1;
		gbc_listTextProcessors.gridy = 1;
		panel_1.add(listTextProcessors, gbc_listTextProcessors);
		
		lblNewLabel_2 = new JLabel("String Similarity Metric");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		panel_1.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		cbStringMetrics = new JComboBox<>();
		GridBagConstraints gbc_cbStringMetrics = new GridBagConstraints();
		gbc_cbStringMetrics.insets = new Insets(0, 0, 5, 0);
		gbc_cbStringMetrics.fill = GridBagConstraints.BOTH;
		gbc_cbStringMetrics.gridx = 1;
		gbc_cbStringMetrics.gridy = 2;
		panel_1.add(cbStringMetrics, gbc_cbStringMetrics);
		
		lblNewLabel_1 = new JLabel("Intertextual Threshold");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 3;
		panel_1.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		tfThreshold = new JTextField();
		GridBagConstraints gbc_tfThreshold = new GridBagConstraints();
		gbc_tfThreshold.insets = new Insets(0, 0, 5, 0);
		gbc_tfThreshold.fill = GridBagConstraints.BOTH;
		gbc_tfThreshold.gridx = 1;
		gbc_tfThreshold.gridy = 3;
		panel_1.add(tfThreshold, gbc_tfThreshold);
		tfThreshold.setColumns(10);
		
		lblIntertextualAssignmentStrategy = new JLabel("Intertextual Assignment Strategy");
		GridBagConstraints gbc_lblIntertextualAssignmentStrategy = new GridBagConstraints();
		gbc_lblIntertextualAssignmentStrategy.anchor = GridBagConstraints.EAST;
		gbc_lblIntertextualAssignmentStrategy.insets = new Insets(0, 0, 5, 5);
		gbc_lblIntertextualAssignmentStrategy.gridx = 0;
		gbc_lblIntertextualAssignmentStrategy.gridy = 4;
		panel_1.add(lblIntertextualAssignmentStrategy, gbc_lblIntertextualAssignmentStrategy);
		
		cbAssignmentStrategy = new JComboBox<>();
		GridBagConstraints gbc_cbAssignmentStrategy = new GridBagConstraints();
		gbc_cbAssignmentStrategy.insets = new Insets(0, 0, 5, 0);
		gbc_cbAssignmentStrategy.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbAssignmentStrategy.gridx = 1;
		gbc_cbAssignmentStrategy.gridy = 4;
		panel_1.add(cbAssignmentStrategy, gbc_cbAssignmentStrategy);
		
		panel_2 = new JPanel();
		panel_3.add(panel_2, BorderLayout.SOUTH);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "iconic evaluation", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		panel_2.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		chkWriteIconics = new JCheckBox("Write Iconcs File");
		panel_2.add(chkWriteIconics);
		

	}

	

	public void createTable(List<KeyValuePair<String, String>> materials) {

		Object[][] data = new Object[materials.size()][5];
		int i = 0;
		for (KeyValuePair<String, String> bothMats : materials) {

			JButton btnEditMat1 = new JButton("edit");

			btnEditMat1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (btnActionListener!=null) {
						btnActionListener.clicked(bothMats.getKey());
					}
				}
			});

			JButton btnEditMat2 = new JButton("edit");
			btnEditMat2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (btnActionListener!=null) {
						btnActionListener.clicked(bothMats.getValue());
					}
				}
			});

			data[i][0] = (i+1);
			
			data[i][1] = bothMats.getKey();
			data[i][2] = btnEditMat1;

			data[i][3] = bothMats.getValue();
			data[i][4] = btnEditMat2;
			i++;
		}

		String[] columnNames = {"Nr", "Material 1", "Visual Editor", "Material 2", "Visual Editor" };
		table = new JTable(data, columnNames);
		table.setDefaultRenderer(Object.class, new ButtonRenderer());

		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int row = table.rowAtPoint(e.getPoint());
				int column = table.columnAtPoint(e.getPoint());
				Object valueAt = table.getValueAt(row, column);
				if (valueAt instanceof JButton) {
					((JButton) valueAt).doClick();
				}

			}
		});

		setTableColWith(0, 40);
		setTableColWith(2, 80);
		setTableColWith(4, 80);
	}

	private void setTableColWith(int colNr, int width) {
		TableColumn col = table.getColumnModel().getColumn(colNr);
		col.setMinWidth(width);
		col.setMaxWidth(width);
		col.setPreferredWidth(width);
	}

	class ButtonRenderer extends DefaultTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
			if (value instanceof JButton) {
				return (JButton) value;
			}
			super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
			return this;
		}
	}

	public JTable getTable() {
		return table;
	}

	public JButton getBtnEvaluate() {
		return btnEvaluate;
	}

	public JComboBox<RedundancyStrategy> getCbRedundancyStrategies() {
		return cbRedundancyStrategies;
	}

	public JTextField getTfThreshold() {
		return tfThreshold;
	}

	public JComboBox<NormalizedStringSimilarity> getCbStringMetrics() {
		return cbStringMetrics;
	}

	public JComboBox<ParagraphAssignmentStrategy> getCbAssignmentStrategy() {
		return cbAssignmentStrategy;
	}

	public JCheckBox getChkWriteIconics() {
		return chkWriteIconics;
	}

	public JList<CheckListItem<String>> getListTextProcessors() {
		return listTextProcessors;
	}
	
}
