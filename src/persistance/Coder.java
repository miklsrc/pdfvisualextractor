package persistance;

import java.awt.Rectangle;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import logic.PdfConnection;
import pdf.elements.PdfComposition;
import pdf.elements.PdfPage;
import pdf.elements.PdfParagraph;
import pdf.elements.PdfPicture;
import util.Geometry;

public class Coder {

	public static File getXml(File pdfSourceFile) {
		return new File(pdfSourceFile.getAbsolutePath() + ".xml");
	}

	public static void save(PdfConnection pdfConnection) {
		PersistDocument document = new PersistDocument();
		document.pdfSpecificListIndicators.addAll(pdfConnection.pdfSpecificListIndicators);		
		
		for (int pageNr = 0; pageNr < pdfConnection.getPageCount(); pageNr++) {
			if (pdfConnection.containsPage(pageNr)) {
				PdfPage pdfPage = pdfConnection.getPdfPage(pageNr);
				PersistPage persistPage = new PersistPage();

				for (PdfParagraph pdfParagraph : pdfPage.pdfParagraphs) {
					PersistParagraph persistParagraph = new PersistParagraph();
					persistParagraph.stringIds = pdfParagraph.getPdfStringIds();
					persistParagraph.additionalSpaces = pdfParagraph.additionalSpaces;
					persistParagraph.annotations = pdfParagraph.annotations;

					persistPage.paragraphs.add(persistParagraph);
				}
				for (PdfPicture pdfPicture : pdfPage.pdfPictures) {
					PersistPicture persistPicture = new PersistPicture();
					persistPicture.imageIds = pdfPicture.getPdfImagesIds();
					persistPage.pictures.add(persistPicture);
				}
				for (PdfComposition pdfComposition : pdfPage.pdfCompositions) {
					
					Rectangle normalizedArea = Geometry.scale(pdfComposition.getRectangle(), 1./pdfConnection.getScale());
					PersistComposition persistComposition=new PersistComposition();
					persistComposition.area=normalizedArea;
					persistComposition.annotations=pdfComposition.annotations;
					persistPage.compositions.add(persistComposition);
				}
				
				document.pages.put(pageNr, persistPage);
			}
		}
		encode(document, getXml(pdfConnection.getPdfSourceFile()));
	}

	public static void load(PdfConnection pdfConnection) {

		File xmlFile = getXml(pdfConnection.getPdfSourceFile());
		if (!xmlFile.exists()) {
			return;
		}

		PersistDocument document = decode(xmlFile, PersistDocument.class);
		
		if (document.pdfSpecificListIndicators != null && document.pdfSpecificListIndicators.size()>0) {
			pdfConnection.pdfSpecificListIndicators.addAll(document.pdfSpecificListIndicators);
			
		}
		
		
		for (int pageNr : document.pages.keySet()) {
			if (!pdfConnection.containsPage(pageNr)) {
				pdfConnection.parsePage(pageNr);
			}
			PdfPage pdfPage = pdfConnection.getPdfPage(pageNr);
			PersistPage persistPage = document.pages.get(pageNr);
			for (PersistParagraph persistParagraph : persistPage.paragraphs) {
				PdfParagraph pdfParagraph = new PdfParagraph(pdfPage.pdfStrings,persistParagraph.stringIds,pdfConnection.getParagraphTextExtractor());
				pdfParagraph.additionalSpaces = persistParagraph.additionalSpaces;
				pdfParagraph.annotations = persistParagraph.annotations;
				pdfPage.pdfParagraphs.add(pdfParagraph);
			}
			for (PersistPicture persistPicture : persistPage.pictures) {
				pdfPage.pdfPictures.add(new PdfPicture(pdfPage.pdfImages, persistPicture.imageIds));
			}
			for (PersistComposition persistComposition : persistPage.compositions) {
				Rectangle scaledArea=Geometry.scale(persistComposition.area, pdfConnection.getScale());
				PdfComposition pdfComposition = new PdfComposition(pdfPage.pageImage,scaledArea);
				pdfComposition.annotations=persistComposition.annotations;
				pdfPage.pdfCompositions.add(pdfComposition);
			}
		}
	}

	public static <T> void encode(T persistObject, File filePath) {
		XMLEncoder encoder = null;
		try {
			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(filePath)));
		} catch (FileNotFoundException fileNotFound) {
			System.out.println("ERROR: While Creating or Opening the File dvd.xml");
		}
		encoder.writeObject(persistObject);
		encoder.close();
	}

	public static <T> T decode(File filePath, Class<T> clazz) {
		XMLDecoder decoder = null;
		T result = null;
		try {
			decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(filePath)));
			result = (T) decoder.readObject();
			decoder.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}
}
