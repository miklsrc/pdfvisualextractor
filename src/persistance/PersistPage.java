package persistance;

import java.util.ArrayList;

public class PersistPage
{	
	public ArrayList<PersistParagraph> paragraphs= new ArrayList<>();
	public ArrayList<PersistPicture> pictures= new ArrayList<>();
	public ArrayList<PersistComposition> compositions= new ArrayList<>();
}