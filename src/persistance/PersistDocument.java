package persistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PersistDocument {

	public HashMap<Integer, PersistPage> pages= new HashMap<>();
	public List<String> pdfSpecificListIndicators = new ArrayList<String>();
}
