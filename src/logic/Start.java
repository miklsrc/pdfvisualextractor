package logic;

import javax.swing.JOptionPane;

public class Start {

	public static void main(String[] args) {	
		String path=""; //path to pdf
		String scaleInput = JOptionPane.showInputDialog(null, "Type in scaling factor.", "1");
		try {
			int scale = Integer.parseInt(scaleInput);
			new MainFrameController(path, scale);
		} catch (Exception e) {
			// nothing to do
		}
	}

}
