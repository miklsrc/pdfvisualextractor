package logic;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import gui.PleaseWaitThreadHandler;
import gui.ThreadHandler;
import pdf.PdfElementsParser;
import pdf.elements.PdfPage;
import pdf.text.ParagraphTextExtractor;

public class PdfConnection {

	private PDDocument document;
	private File pdfSourceFile;
	private ThreadHandler threadHandler;
	private HashMap<Integer, PdfPage> pdfPages = new HashMap<Integer, PdfPage>();
	private int scale;
	private int pageCount;
	private ParagraphTextExtractor paragraphTextExtractor = new ParagraphTextExtractor();
	public List<String> pdfSpecificListIndicators = new ArrayList<String>();
	
	public PdfConnection(String pdfPath, int scale, ThreadHandler threadHandler) {
		this.threadHandler = threadHandler;
		this.scale = scale;
		loadPdf(pdfPath);
	}

	public int getScale() {
		return scale;
	}

	public PdfConnection(String pdfPath, PleaseWaitThreadHandler threadHandler) {
		this(pdfPath, 1, threadHandler);
	}

	public void closePdf() {
		try {
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadPdf(String pdfPath) {
		try {
			pdfSourceFile = new File(pdfPath);
			document = PDDocument.load(pdfSourceFile);
			pageCount = document.getNumberOfPages();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private BufferedImage createPdfPageImage(int pageNr) throws IOException {
		PDFRenderer pdfRenderer = new PDFRenderer(document);
		BufferedImage renderImage = pdfRenderer.renderImage(pageNr, scale);
		return renderImage;
	}

	public void parsePage(int pageNr) {
		Runnable run = new Runnable() {
			public void run() {
				try {
					PdfElementsParser pdfParser = new PdfElementsParser(document, scale);
					PdfPage pdfPage = pdfParser.parsePage(pageNr);
					pdfPage.pageImage = createPdfPageImage(pageNr);

					pdfPages.put(pageNr, pdfPage);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		this.threadHandler.process(run);
	}

	public File getPdfSourceFile() {
		return pdfSourceFile;
	}

	public int getPageCount() {
		return pageCount;
	}

	public PdfPage getPdfPage(int pageNr) {
		return pdfPages.get(pageNr);
	}

	public boolean containsPage(int pageNr) {
		return pdfPages.containsKey(pageNr);
	}

	public ParagraphTextExtractor getParagraphTextExtractor() {
		return paragraphTextExtractor ;
	}
}
