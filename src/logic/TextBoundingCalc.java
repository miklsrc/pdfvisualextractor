package logic;

import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.List;

import pdf.elements.PdfGlyph;

public class TextBoundingCalc {

	private List<PdfGlyph> pdfGlyphs;

	public TextBoundingCalc(List<PdfGlyph> pdfPosGlyphs) {
		this.pdfGlyphs = pdfPosGlyphs;
	}

	public LinkedList<PdfGlyph> searchGlyphsForString(Rectangle2D boundingRect) {
		LinkedList<PdfGlyph> glyphs = new LinkedList<PdfGlyph>();
		for (PdfGlyph pdfGlyph : pdfGlyphs) {
			Rectangle2D pdfGlyphBounds = pdfGlyph.shape.getBounds2D();
			if (pdfGlyphBounds.intersects(boundingRect)) {
				glyphs.add(pdfGlyph);
			}
		}
		
		glyphs.sort((x,y)->x.getRectangle().x-y.getRectangle().x);
		return glyphs;
	}

	public Rectangle2D GetBorderForString(Rectangle2D boundingRect) {
		LinkedList<PdfGlyph> glyphsForString=searchGlyphsForString(boundingRect);
		
		
		for (PdfGlyph pdfGlyph : glyphsForString) {
			Rectangle2D bounds = pdfGlyph.shape.getBounds2D();
			Rectangle2D.union(boundingRect, bounds, boundingRect);
		}

		return boundingRect;
	}

	

}
