package logic;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;

import annotation.Annotation;
import annotation.AnnotationManager;
import annotation.AnnotationValue;
import gui.MainFrame;
import gui.ParagraphTextDialog;
import gui.PleaseWaitThreadHandler;
import gui.StringListEditorDialog;
import gui.imagepanel.AreaSelectionListener;
import gui.imagepanel.ImageMouseHoverListener;
import gui.imagepanel.size.FitToComponentImageSizeStrategy;
import gui.imagepanel.size.NoStretchImageSizeStrategy;
import pdf.elements.AbstractPdfElement;
import pdf.elements.PdfComposition;
import pdf.elements.PdfGlyph;
import pdf.elements.PdfImage;
import pdf.elements.PdfLinepath;
import pdf.elements.PdfPage;
import pdf.elements.PdfParagraph;
import pdf.elements.PdfPicture;
import pdf.elements.PdfRectangle;
import pdf.elements.PdfString;
import pdf.elements.PdfUnderline;
import pdf.text.RemoveListIndicatorProc;
import pdf.text.TweakSpacesProc;
import persistance.Coder;
import util.MathUtils;
import util.SwingUtil;

public class MainFrameController {

	final static Logger logger = Logger.getLogger(MainFrameController.class);
	private MainFrame frame;
	private int currentPageIndex = 0;
	private HighlightManager highlightManager;
	private List<JCheckBox> checkBoxes = new LinkedList<JCheckBox>();
	private HashMap<String, List<JToggleButton>> annotationButtons = new HashMap<>();
	private PdfConnection pdfConnection;
	private boolean blockListChangeEvents = false;
	private CompositionAdder compositionAdder = new CompositionAdder();

	public MainFrameController(String pdfPath, int scale) {
		setupUi();
		highlightManager = new HighlightManager(frame.getImagePanel());
		pdfConnection = new PdfConnection(pdfPath, scale, new PleaseWaitThreadHandler(frame));
		pdfConnection.getParagraphTextExtractor().paragraphTextProcessors.add(new RemoveListIndicatorProc(pdfConnection.pdfSpecificListIndicators));
		pdfConnection.getParagraphTextExtractor().paragraphTextProcessors.add(new TweakSpacesProc());

		Coder.load(pdfConnection);
		jumpToImage(0);
	}

	private void setupElementSelectionBoxes() {
		createCheckboxAction(PdfParagraph.TYPE_NAME);
		createCheckboxAction(PdfString.TYPE_NAME);
		createCheckboxAction(PdfImage.TYPE_NAME);
		createCheckboxAction(PdfPicture.TYPE_NAME);
		createCheckboxAction(PdfGlyph.TYPE_NAME);
		createCheckboxAction(PdfRectangle.TYPE_NAME);
		createCheckboxAction(PdfUnderline.TYPE_NAME);
		createCheckboxAction(PdfComposition.TYPE_NAME);
		createCheckboxAction(PdfLinepath.TYPE_NAME);
	}

	private void jumpToImage(int newImgIndex) {

		if (newImgIndex >= 0 && newImgIndex < pdfConnection.getPageCount()) {
			currentPageIndex = newImgIndex;
			if (!pdfConnection.containsPage(newImgIndex)) {
				pdfConnection.parsePage(newImgIndex);
			}

			PdfPage pdfPage = currentPdfPage();

			frame.getImagePanel().clear();
			frame.getImgPreviewPanel().setImage(null);

			Collections.sort(pdfPage.pdfParagraphs, (a, b) -> Integer.compare(a.getRectangle().y, b.getRectangle().y));

			fillList(pdfPage.pdfStrings, frame.getListStrings());

			fillList(pdfPage.pdfParagraphs, frame.getListParagraphs());
			fillList(pdfPage.pdfImages, frame.getListImages());
			fillList(pdfPage.pdfPictures, frame.getListPictures());
			fillList(pdfPage.pdfRectangles, frame.getListRectangles());
			fillList(pdfPage.pdfLinePaths, frame.getListLinepaths());
			fillList(pdfPage.pdfCompositions, frame.getListCompositions());

			frame.getImagePanel().setImage(pdfPage.pageImage);
			SwingUtil.invokeLater(() -> {
				frame.getLblCurrentImg().setText((currentPageIndex + 1) + "/" + pdfConnection.getPageCount());
			});
			updateElementHighlights();
		}
	}

	interface SuitableChecker {
		public boolean check(Rectangle rect);
	}

	protected LinkedList<AbstractPdfElement> getSuitableBoundaryElements(SuitableChecker containChecker) {
		LinkedList<AbstractPdfElement> selections = new LinkedList<AbstractPdfElement>();
		for (AbstractPdfElement boundaryElement : currentPdfPage().getBoundaryElements()) {
			if (containChecker.check(boundaryElement.getRectangle())) {
				selections.add(boundaryElement);
			}
		}
		return selections;
	}

	private PdfPage currentPdfPage() {
		return pdfConnection.getPdfPage(currentPageIndex);
	}

	protected void highlightElements(Rectangle area) {
		LinkedList<AbstractPdfElement> selectedElements = getSuitableBoundaryElements(new SuitableChecker() {
			public boolean check(Rectangle rect) {
				return area.contains(rect);
			}
		});
		selectInLists(selectedElements);
		highlightManager.updateSelectedHighlights(selectedElements);
	}

	private void selectInLists(List<? extends AbstractPdfElement> selectedElements) {
		SwingUtil.invokeLater(() -> {
			blockListChangeEvents = true;
			JList[] lists = new JList[] { frame.getListStrings(), frame.getListParagraphs(), frame.getListImages() };
			for (JList jList : lists) {
				ArrayList<Integer> selectionIndices = new ArrayList<Integer>();
				DefaultListModel listModel = (DefaultListModel) jList.getModel();
				for (int j = 0; j < listModel.size(); j++) {
					for (AbstractPdfElement boundaryPrintable : selectedElements) {
						if (listModel.get(j).equals(boundaryPrintable)) {
							selectionIndices.add(j);
						}
					}
				}
				int[] indices = selectionIndices.stream().mapToInt(i -> i).toArray();

				jList.setSelectedIndices(indices);

			}

			// select tab with the most selected items
			Optional<? extends AbstractPdfElement> minSizeElement = selectedElements.stream().min(Comparator.comparingDouble(i -> i.getRectangle().getWidth() * i.getRectangle().getHeight()));
			if (minSizeElement.isPresent()) {
				selectTab(minSizeElement.get().getTypeName());
			}
			// when paragraph is involved in selection -> jump to paragraph tab
			if (frame.getListParagraphs().getSelectedIndices().length > 0) {
				selectTab(PdfParagraph.TYPE_NAME);
			}
			blockListChangeEvents = false;
		});
	}

	private void selectTab(String mostSelectedType) {
		for (int i = 0; i < frame.getTabbedPaneElements().getTabCount(); i++) {
			String title = frame.getTabbedPaneElements().getTitleAt(i);
			if (title.equals(mostSelectedType)) {
				frame.getTabbedPaneElements().setSelectedIndex(i);
				return;
			}
		}
	}

	private void hoverElements(Point point) {
		highlightManager.updateHoverHighlights(getSuitableBoundaryElements(new SuitableChecker() {
			public boolean check(Rectangle rect) {
				return rect.contains(point);
			}
		}));
	}

	private <K> void fillList(List<K> elements, JList<K> list) {
		SwingUtil.invokeLater(() -> {
			((DefaultListModel<K>) list.getModel()).clear();
			for (K pdfString : elements) {
				((DefaultListModel<K>) list.getModel()).addElement(pdfString);
			}
		});
	}

	protected void addSelectedStringsAsUnionParagraph() {
		int[] selectedIndices = frame.getListStrings().getSelectedIndices();
		if (selectedIndices.length > 0) {
			PdfPage renderedPage = currentPdfPage();
			PdfParagraph pdfParagraph = new PdfParagraph(renderedPage.pdfStrings, selectedIndices, pdfConnection.getParagraphTextExtractor());
			renderedPage.pdfParagraphs.add(pdfParagraph);
			updateElementHighlights();
			fillList(currentPdfPage().pdfParagraphs, frame.getListParagraphs());
			selectInLists(new ArrayList<PdfParagraph>() {
				{
					add(pdfParagraph);
				}
			});
		}
	}

	protected void addSelectedStringsAsIndividualParagraphs() {
		int[] selectedIndices = frame.getListStrings().getSelectedIndices();
		if (selectedIndices.length > 0) {
			PdfPage renderedPage = currentPdfPage();
			List<PdfParagraph> paragraphs = new LinkedList<>();
			for (int selectedIndex : selectedIndices) {
				PdfParagraph pdfParagraph = new PdfParagraph(renderedPage.pdfStrings, new int[] { selectedIndex }, pdfConnection.getParagraphTextExtractor());
				paragraphs.add(pdfParagraph);
				renderedPage.pdfParagraphs.add(pdfParagraph);
			}
			updateElementHighlights();
			fillList(currentPdfPage().pdfParagraphs, frame.getListParagraphs());
			selectInLists(paragraphs);
		}
	}

	protected void addSelectedImgsAsPicture() {
		int[] selectedIndices = frame.getListImages().getSelectedIndices();
		if (selectedIndices.length > 0) {
			PdfPicture picture = new PdfPicture(currentPdfPage().pdfImages, selectedIndices);
			List<PdfPicture> pagePdfPictures = currentPdfPage().pdfPictures;
			pagePdfPictures.add(picture);
			fillList(pagePdfPictures, frame.getListPictures());
			updateElementHighlights();
		}
	}

	private <K, V extends AbstractPdfElement> void highlightSelectedElements(JList<V> list) {
		SwingUtil.invokeLater(() -> {

			LinkedList<AbstractPdfElement> selectedElements = new LinkedList<>();
			util.Collection.addAllToList(selectedElements, list.getSelectedValuesList());
			highlightManager.updateSelectedHighlights(selectedElements);
		});
	}

	private void paragraphOptionsClicked() {
		PdfParagraph paragraph = frame.getListParagraphs().getSelectedValue();
		if (paragraph != null) {
			ParagraphTextDialog paragraphOptions = new ParagraphTextDialog(paragraph);
			paragraphOptions.setVisible(true);
			frame.getListParagraphs().updateUI();
		}
	}

	protected void manageListIndicatorsClicked() {
		StringListEditorDialog dialog = new StringListEditorDialog();
		dialog.bindList(pdfConnection.pdfSpecificListIndicators);
		dialog.setVisible(true);
	}

	private void saveScreenshot() {
		Component component = frame.getImagePanel();
		Dimension componentSize = component.getPreferredSize();
		component.setSize(componentSize); // need to make sure that both sizes are equal
		BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics g = image.createGraphics();
		g.fillRect(0, 0, image.getWidth(), image.getHeight());
		component.print(g);
		try {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setSelectedFile(new File("screenshot.jpg"));
			if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
				File fileToSave = fileChooser.getSelectedFile();
				ImageIO.write(image, "jpg", fileToSave);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void setupUi() {
		frame = new MainFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		setupElementSelectionBoxes();

		// ///// lists

		frame.getListRectangles().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				highlightSelectedElements(frame.getListRectangles());
			}
		});
		frame.getListLinepaths().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				highlightSelectedElements(frame.getListLinepaths());
			}
		});
		frame.getListPictures().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (blockListChangeEvents)
					return;

				highlightSelectedElements(frame.getListPictures());
				PdfPicture selectedPic = frame.getListPictures().getSelectedValue();
				BufferedImage mergedImg = selectedPic == null ? null : selectedPic.getPicture();
				frame.getPicturePreviewPanel().setImage(mergedImg);
			}
		});
		frame.getListCompositions().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (blockListChangeEvents)
					return;

				highlightSelectedElements(frame.getListCompositions());
				PdfComposition selectedComposition = frame.getListCompositions().getSelectedValue();
				BufferedImage image = selectedComposition != null ? selectedComposition.compositionImage : null;
				frame.getCompositionPreviewPanel().setImage(image);
			}
		});

		frame.getListImages().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				List<PdfImage> pdfImages = frame.getListImages().getSelectedValuesList();
				BufferedImage mergedImg = PdfPicture.merge(pdfImages);
				frame.getImgPreviewPanel().setImage(mergedImg);

				if (blockListChangeEvents)
					return;

				highlightSelectedElements(frame.getListImages());

			}
		});
		frame.getListStrings().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (blockListChangeEvents)
					return;

				highlightSelectedElements(frame.getListStrings());

				PdfString selectedValue = frame.getListStrings().getSelectedValue();
				if (selectedValue != null)
					selectedValue.analyzeString();

			}
		});
		// /////////
		frame.getBtnParagraphOptions().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				paragraphOptionsClicked();
			}
		});
		frame.getBtnListIndicators().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manageListIndicatorsClicked();
			}
		});

		frame.getBtnDeleteParagraph().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<PdfParagraph> paragraphs = frame.getListParagraphs().getSelectedValuesList();
				currentPdfPage().pdfParagraphs.removeAll(paragraphs);
				fillList(currentPdfPage().pdfParagraphs, frame.getListParagraphs());

				updateElementHighlights();
			}
		});
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				pdfConnection.closePdf();
			}
		});
		frame.getBtnSave().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Coder().save(pdfConnection);
			}
		});
		frame.getImagePanel().addImageMouseListener(new ImageMouseHoverListener() {
			public void mouseMoved(Point point) {
				hoverElements(point);
			}

			public void mouseExited() {
				highlightManager.updateHoverHighlights(new LinkedList<AbstractPdfElement>());
			}

			public void mouseClicked(Point point) {
				LinkedList<AbstractPdfElement> boundaryElements = getSuitableBoundaryElements(new SuitableChecker() {
					public boolean check(Rectangle rect) {
						return rect.contains(point);
					}
				});
				selectInLists(boundaryElements);
				highlightManager.updateSelectedHighlights(boundaryElements);
			}
		});
		frame.getImagePanel().addAreaSelectionListener(new AreaSelectionListener() {
			public void areaSelected(Rectangle selection) {
				highlightElements(selection);
			}
		});

		frame.getBtnAddUnionParagrpah().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addSelectedStringsAsUnionParagraph();
			}
		});
		frame.getBtnAddIndividualParagrpah().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addSelectedStringsAsIndividualParagraphs();
			}
		});
		frame.getBtnUp().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jumpToImage(currentPageIndex + 1);
			}
		});
		frame.getBtnDown().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jumpToImage(currentPageIndex - 1);
			}
		});
		frame.getBtnJumpFront().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jumpToImage(0);
			}
		});
		frame.getBtnJumpEnd().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jumpToImage(pdfConnection.getPageCount() - 1);
			}
		});
		frame.getRbImgSizeFull().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getImagePanel().setImageSizeStrategy(new NoStretchImageSizeStrategy());
			}
		});
		frame.getRbImgSizeStretched().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getImagePanel().setImageSizeStrategy(new FitToComponentImageSizeStrategy(frame.getImgScrollPane()));
			}
		});
		frame.getBtnJumpToPage().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String pageNrStr = frame.getTfPageNumber().getText();
				if (MathUtils.isInteger(pageNrStr)) {
					jumpToImage(Integer.parseInt(pageNrStr) - 1);
				}
			}
		});
		frame.getBtnScreenshot().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveScreenshot();
			}
		});
		frame.getBtnAddPicture().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addSelectedImgsAsPicture();
			}
		});
		frame.getBtnDeletePicture().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<PdfPicture> pdfPictures = frame.getListPictures().getSelectedValuesList();
				currentPdfPage().pdfPictures.removeAll(pdfPictures);
				fillList(currentPdfPage().pdfPictures, frame.getListPictures());

				updateElementHighlights();
			}
		});
		frame.getImagePanel().addAreaSelectionListener(compositionAdder);
		frame.getBtnAddComposition().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				compositionAdder.activateSelection();
			}
		});
		frame.getBtnDeleteComposition().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<PdfComposition> pdfCompositions = frame.getListCompositions().getSelectedValuesList();
				currentPdfPage().pdfCompositions.removeAll(pdfCompositions);
				fillList(currentPdfPage().pdfCompositions, frame.getListCompositions());

				updateElementHighlights();
			}
		});
		frame.getListParagraphs().addListSelectionListener(new AnnotationListSelectionListener(frame.getListParagraphs()));
		frame.getListCompositions().addListSelectionListener(new AnnotationListSelectionListener(frame.getListCompositions()));

		addAnnotationControls(PdfParagraph.TYPE_NAME, frame.getPnlParagraphAnnotations(), frame.getListParagraphs());
		addAnnotationControls(PdfComposition.TYPE_NAME, frame.getPnlCompositionAnnotations(), frame.getListCompositions());

		for (JCheckBox checkBox : checkBoxes) {
			frame.getPnlHighlightElements().add(checkBox);
		}
	}

	private class AnnotationListSelectionListener implements ListSelectionListener {
		private JList<? extends AbstractPdfElement> list;

		public AnnotationListSelectionListener(JList<? extends AbstractPdfElement> list) {
			this.list = list;
		}

		public void valueChanged(ListSelectionEvent arg0) {
			if (list.getSelectedIndices().length == 1) {
				setAnnotationState(list.getSelectedValue().annotations);
			} else {
				setAnnotationState(new HashSet<String>());
			}

			if (blockListChangeEvents)
				return;

			highlightSelectedElements(list);
		}
	}

	private class CompositionAdder implements AreaSelectionListener {

		private boolean selectionActive = false;

		public void activateSelection() {
			selectionActive = true;
		}

		public void areaSelected(Rectangle selection) {
			if (selectionActive) {
				selectionActive = false;
				frame.getBtnAddComposition().setSelected(false);

				BufferedImage pageImage = currentPdfPage().pageImage;
				if (selection.width > 0 && selection.height > 0 && selection.x < pageImage.getWidth() && selection.y < pageImage.getHeight()) {
					PdfComposition composition = new PdfComposition(pageImage, selection);
					currentPdfPage().pdfCompositions.add(composition);
					fillList(currentPdfPage().pdfCompositions, frame.getListCompositions());
					updateElementHighlights();
				}
			}
		}
	}

	private void setAnnotationState(HashSet<String> annotationIds) {
		for (String pdfElementType : annotationButtons.keySet()) {
			for (JToggleButton pdfElementAnnotationButton : annotationButtons.get(pdfElementType)) {
				boolean isSelected = annotationIds.contains(pdfElementAnnotationButton.getText());
				pdfElementAnnotationButton.setSelected(isSelected);
			}
		}
	}

	private void addAnnotationControls(String typeName, JPanel panel, JList<? extends AbstractPdfElement> list) {
		LinkedList<JToggleButton> pdfElementAnnotationButtons = new LinkedList<>();
		for (Annotation anno : AnnotationManager.getInstance().getAnnotations(typeName)) {
			for (AnnotationValue annoValue : anno.values) {
				JToggleButton button = new JToggleButton(annoValue.id);
				button.setIcon(new ImageIcon(annoValue.img));
				button.setMnemonic(annoValue.key);
				button.setToolTipText(annoValue.name);
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						annotationChange(annoValue.id, list);
					}
				});
				panel.add(button);
				pdfElementAnnotationButtons.add(button);
			}
		}
		annotationButtons.put(typeName, pdfElementAnnotationButtons);
	}

	protected void annotationChange(String annotationId, JList<? extends AbstractPdfElement> list) {
		List<? extends AbstractPdfElement> pdfElements = list.getSelectedValuesList();
		for (AbstractPdfElement pdfElement : pdfElements) {
			AnnotationManager.getInstance().changeAnnotationValue(pdfElement, annotationId);
		}
		list.updateUI();
		highlightManager.refresh();
		if (pdfElements.size() > 0)
			setAnnotationState(pdfElements.get(0).annotations);
	}

	// ///////////////

	public void createCheckboxAction(String label) {
		JCheckBox checkBox = new JCheckBox(label);
		checkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateElementHighlights();
			}
		});
		checkBoxes.add(checkBox);
	}

	public void updateElementHighlights() {
		HashSet<String> names = new HashSet<String>();
		for (JCheckBox checkBox : checkBoxes) {
			if (checkBox.isSelected()) {
				names.add(checkBox.getText());
			}
		}
		LinkedList<AbstractPdfElement> activeElements = new LinkedList<AbstractPdfElement>();
		for (AbstractPdfElement elements : currentPdfPage().getBoundaryElements()) {
			if (names.contains(elements.getTypeName())) {
				activeElements.add(elements);
			}
		}
		highlightManager.updateDurableHighlights(activeElements);
	}
}
