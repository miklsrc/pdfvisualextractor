package logic;

import java.awt.BasicStroke;
import java.awt.Image;
import java.util.LinkedList;

import annotation.AnnotationManager;
import gui.imagepanel.GeomImagePanel;
import gui.imagepanel.GeomImagePanel.ImagePainter;
import pdf.elements.AbstractPdfElement;
import util.SwingUtil;

public class HighlightManager {

	private GeomImagePanel imagePanel;

	private BasicStroke strokeSelect = new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
	private BasicStroke strokeDurable = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
	private BasicStroke strokeHover = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 9 }, 0);

	private LinkedList<AbstractPdfElement> durableHighlights = new LinkedList<>();
	private LinkedList<AbstractPdfElement> selectedHighlights = new LinkedList<>();
	private LinkedList<AbstractPdfElement> hoverHighlights = new LinkedList<>();

	public HighlightManager(GeomImagePanel imagePanel) {
		this.imagePanel = imagePanel;
	}

	public void updateHoverHighlights(LinkedList<AbstractPdfElement> newHoverHighlights) {
		hoverHighlights = newHoverHighlights;
		refresh();
	}

	public void updateSelectedHighlights(LinkedList<AbstractPdfElement> newSelectedHighlights) {
		selectedHighlights = newSelectedHighlights;
		refresh();
	}

	public void updateDurableHighlights(LinkedList<AbstractPdfElement> durableElements) {
		durableHighlights = durableElements;
		refresh();
	}

	public void refresh() {
		SwingUtil.invokeLater(() -> {
			imagePanel.clear();

			AddGeomsToImagePanel(hoverHighlights, strokeHover);
			AddGeomsToImagePanel(durableHighlights, strokeDurable);
			AddGeomsToImagePanel(selectedHighlights, strokeSelect);

			imagePanel.forceRepaint();
		});
	}

	private void AddGeomsToImagePanel(LinkedList<AbstractPdfElement> olds, BasicStroke stroke) {
		for (AbstractPdfElement boundaryPrintable : olds) {
			imagePanel.addRectangle(boundaryPrintable.getRectangle(), boundaryPrintable.getColor(), stroke);

			int x = boundaryPrintable.getRectangle().x + boundaryPrintable.getRectangle().width;
			for (String annotation : boundaryPrintable.annotations) {
				Image img = AnnotationManager.getInstance().getAnnotationIconByValue(annotation);
				imagePanel.addImage(img, x, boundaryPrintable.getRectangle().y);
				x += img.getWidth(null) + ImagePainter.space * 2;
			}
		}
	}
}
