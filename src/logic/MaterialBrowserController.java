package logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import data.Materials;
import evaluation.Config;
import evaluation.EvalResultContainer;
import evaluation.Evaluator;
import evaluation.assignment.ParagraphAssignmentStrategies;
import evaluation.assignment.ParagraphAssignmentStrategy;
import evaluation.excel.ExcelCreator;
import evaluation.excel.PdfIconics2ExcelExporter;
import evaluation.excel.sheetwriter.TextStatisticsWriter;
import evaluation.redundancy.RedundancyStrategies;
import evaluation.redundancy.RedundancyStrategy;
import evaluation.similarity.StringSimilarityMetrics;
import evaluation.similarity.metrics.NormalizedStringSimilarity;
import gui.MaterialsBrowser;
import gui.MaterialsBrowser.CellBtnActionListener;
import gui.PleaseWaitThreadHandler;
import pdf.text.TextProcessors;
import persistance.Coder;
import util.CheckboxListUils;
import util.LoggerUtil;

public class MaterialBrowserController {

	private MaterialsBrowser frame;
	private PleaseWaitThreadHandler threadHandler = new PleaseWaitThreadHandler();;

	public MaterialBrowserController() {
		setupUi();
	}

	private void setupUi() {
		frame = new MaterialsBrowser(Materials.getMaterials());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);

		frame.setCellBtnActionListener(new CellBtnActionListener() {
			public void clicked(String cellStr) {
				openVisualEditor(cellStr);
			}
		});

		frame.getBtnEvaluate().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				evaluate();
			}
		});
		frame.getTfThreshold().setText(Config.intertextualThreshold + "");

		for (RedundancyStrategy redundancyStrategy : RedundancyStrategies.strategies) {
			frame.getCbRedundancyStrategies().addItem(redundancyStrategy);
		}

		for (ParagraphAssignmentStrategy assignmentStrategy : ParagraphAssignmentStrategies.strategies) {
			frame.getCbAssignmentStrategy().addItem(assignmentStrategy);
		}

		for (NormalizedStringSimilarity strSimMetric : StringSimilarityMetrics.metrics) {
			frame.getCbStringMetrics().addItem(strSimMetric);
		}

		for (String textProcessorLabel : TextProcessors.processorLabels) {
			CheckboxListUils.addElement(frame.getListTextProcessors(), textProcessorLabel, true);
		}

	}

	private void evaluate() {
		int[] selectedRows = frame.getTable().getSelectedRows();

		String thresholdStr = frame.getTfThreshold().getText();
		float threshold = -1;
		try {
			threshold = Float.parseFloat(thresholdStr);
		} catch (Exception e) {
			// nothing to do
		}
		if (threshold < 0 || threshold > 1.0) {
			JOptionPane.showMessageDialog(frame, "threshold must be a float value between 0 and 1", "error", 1);
			return;
		}
		if (selectedRows.length == 0) {
			JOptionPane.showMessageDialog(frame, "no materials selected", "error", 1);
			return;
		}

		RedundancyStrategy redundancyStrategy = frame.getCbRedundancyStrategies().getModel().getElementAt(frame.getCbRedundancyStrategies().getSelectedIndex());
		NormalizedStringSimilarity strSimMetric = frame.getCbStringMetrics().getModel().getElementAt(frame.getCbStringMetrics().getSelectedIndex());
		ParagraphAssignmentStrategy assignmentStrategy = frame.getCbAssignmentStrategy().getModel().getElementAt(frame.getCbAssignmentStrategy().getSelectedIndex());
		Evaluator evaluator = new Evaluator(redundancyStrategy, strSimMetric, threshold, assignmentStrategy);
		boolean writeIconics = frame.getChkWriteIconics().isSelected();
		List<String> textProcessorLabels = CheckboxListUils.getCheckedValues(frame.getListTextProcessors());

		String folderPath =Materials.inputFileListPath.getParent()+ "/export/evaluation " + getDateString();
		File folder = new File(folderPath);
		folder.mkdirs();

		String logFilePath = folderPath + "/log.txt";
		writeLog(logFilePath, "RedundancyStrategy : " + redundancyStrategy);
		writeLog(logFilePath, "NormalizedStringSimilarity : " + strSimMetric);
		writeLog(logFilePath, "ParagraphAssignmentStrategy : " + assignmentStrategy);
		writeLog(logFilePath, "intertextual threshold : " + threshold);
		textProcessorLabels.stream().forEach(e -> writeLog(logFilePath, "TextPreProcessor : " + e));

		LinkedList<LinkedHashMap<String, Integer>> results = new LinkedList<>();

		for (int selectedRow : selectedRows) {
			String mat1Path = (String) frame.getTable().getValueAt(selectedRow, 1).toString();
			String mat2Path = (String) frame.getTable().getValueAt(selectedRow, 3).toString();
			writeLog(logFilePath, "Material " + (selectedRow + 1) + ": " + mat1Path + " , " + mat2Path);
			PdfConnection pdfConnection1 = loadPdfConnection(mat1Path);
			PdfConnection pdfConnection2 = loadPdfConnection(mat2Path);
			for (String textProcessorLabel : textProcessorLabels) {
				pdfConnection1.getParagraphTextExtractor().paragraphTextProcessors.add(TextProcessors.create(textProcessorLabel, pdfConnection1));
				pdfConnection2.getParagraphTextExtractor().paragraphTextProcessors.add(TextProcessors.create(textProcessorLabel, pdfConnection2));
			}

			String xlsBasePath = folderPath + "/" + (selectedRow + 1);

			// calculate and export verbal

			EvalResultContainer result = evaluator.run(pdfConnection1, pdfConnection2, xlsBasePath);
			results.add(result.getResults());

			// export iconics
			if (writeIconics) {
				PdfIconics2ExcelExporter pdfIconicsExporter = new PdfIconics2ExcelExporter();
				pdfIconicsExporter.exportToExcel(pdfConnection1, pdfConnection2);
				pdfIconicsExporter.saveAsFile(xlsBasePath + "exp_iconics.xls");
			}
		}

		ExcelCreator excelCreator = new ExcelCreator();
		TextStatisticsWriter exporter = new TextStatisticsWriter(results);
		exporter.writeSheet(excelCreator.CreateSheet("statistics"));
		excelCreator.saveAsFile(folderPath + "/stats " + redundancyStrategy.getLabel() + ".xls");
	}

	private void writeLog(String logPath, String msg) {
		try {
			FileWriter writer = new FileWriter(logPath, true);
			writer.write(msg + "\n");
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private PdfConnection loadPdfConnection(String path) {
		PdfConnection pdfConnection = new PdfConnection(path, threadHandler);
		Coder.load(pdfConnection);
		pdfConnection.closePdf();
		return pdfConnection;
	}

	private String getDateString() {
		SimpleDateFormat dateFomat = new SimpleDateFormat("yyy-MM-dd_hh-mm-ss");
		return dateFomat.format(new Date());
	}

	private void openVisualEditor(String path) {
		String scaleInput = JOptionPane.showInputDialog(frame, "Type in scaling factor.", "1");
		try {
			int scale = Integer.parseInt(scaleInput);
			new MainFrameController(path, scale);
		} catch (Exception e) {
			// nothing to do
		}
	}

	public static void main(String[] args) {
		LoggerUtil.setLogLevel(Level.WARNING);
		File inputFile;
		if (args.length == 1 && (inputFile = new File(args[0])).exists()) {
			Materials.readInputFiles(inputFile);
		} else {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Select File Input List");
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				inputFile = fileChooser.getSelectedFile();
				Materials.readInputFiles(inputFile);
			}
		}
		if (Materials.getMaterials().size()==0)
		{
			JOptionPane.showMessageDialog(null, "no input files found!");
		}
		new MaterialBrowserController();
	}
}
