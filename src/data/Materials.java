package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import evaluation.PdfElementBrowser.TextCharacteristic;
import util.KeyValuePair;

public class Materials {

	public static LinkedList<File> ignoreFormattings = new LinkedList<>();
	public static  List<KeyValuePair<String, String>> documents = new ArrayList<>();
	private static String doc1Path=null;
	public static File inputFileListPath;

	public static void readInputFiles(File path) {
		Materials.inputFileListPath=path;
		try(BufferedReader br = new BufferedReader(new FileReader(path))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		    	processLine(line);
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void processLine(String line) {
		String[] parts = line.split(",");
		String docPath=inputFileListPath.getParent()+"/"+parts[0];
		
		if (parts.length>=2) {
			if (parts[1].equals("IGNORE_FORMATTINGS")) {
				ignoreFormattings.add(new File(docPath));
			}
		}
		if (doc1Path==null) {
			doc1Path=docPath;
		} else {
			String doc2Path=docPath;
			documents.add(new KeyValuePair<String, String>(doc1Path, doc2Path));
			doc1Path=null; //reset
		}
	}
	
	public static List<TextCharacteristic> getTextCharacteristicsWithoutFormattings() {
		List<TextCharacteristic> tcs=new LinkedList<>();
		tcs.add(TextCharacteristic.allChar);
		tcs.add(TextCharacteristic.visibleChar);
		return tcs;
	}
	
	public static List<TextCharacteristic> getCountableTextCharacteristics() {
		List<TextCharacteristic> tcs=new LinkedList<>();
		tcs.add(TextCharacteristic.allChar);
		tcs.add(TextCharacteristic.visibleChar);
		tcs.add(TextCharacteristic.bold);
		tcs.add(TextCharacteristic.italic);
		tcs.add(TextCharacteristic.underline);
		return tcs;
	}
	
	public static boolean isIgnoreFormatting(File file) {
		return ignoreFormattings.stream().anyMatch(scanFile->scanFile.compareTo(file)==0);
	}
	
	
	public static List<KeyValuePair<String,String>> getMaterials() {
		return documents;
	}
	

	

}
