package data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import pdf.FontExtractor;

public class FontCollection {

	public static HashMap<String,HashSet<String>> fontFamilys = new HashMap<>();
	
	static {
		
		addFontFamily("Arial",Arrays.asList("Arial","Arial-BoldItalicMT","Arial-BoldMT","Arial,Bold","ArialMT","Arial,BoldItalic","Arial-ItalicMT","Aller,Italic","Arial,Italic"));
		addFontFamily("Aller",Arrays.asList("Aller Light","Aller,Bold","Aller Display","Aller,BoldItalic","Aller Light,Italic","Aller"));
		addFontFamily("BookAntiqua",Arrays.asList("BookAntiqua","BookAntiqua-Bold"));
		addFontFamily("Brush Script MT",Arrays.asList("Brush Script MT-Bold","Brush Script MT"));
		addFontFamily("CenturyGothic",Arrays.asList("CenturyGothic-Bold","CenturyGothic","CenturyGothic-Italic"));
		addFontFamily("Cambria",Arrays.asList("Cambria","Cambria-Italic","Cambria-Bold"));
		addFontFamily("Calibri",Arrays.asList("Calibri","Calibri,Italic","Calibri-Bold","Calibri,BoldItalic","Calibri-Italic","Calibri-BoldItalic","Calibri,Bold"));
		addFontFamily("CalistoMT",Arrays.asList("CalistoMT","CalistoMT-Italic"));
		addFontFamily("Courier",Arrays.asList("CourierNewPSMT","Courier New"));
		addFontFamily("Constantia",Arrays.asList("Constantia"));
		addFontFamily("Candara,Italic",Arrays.asList("Candara,Italic"));
		addFontFamily("Comic Sans MS",Arrays.asList("Comic Sans MS"));
		addFontFamily("Franklin Gothic",Arrays.asList("Franklin Gothic Book","FranklinGothic-Medium","Franklin Gothic Medium"));
		addFontFamily("Georgia",Arrays.asList("Georgia-Bold","Georgia-Italic","Georgia,Italic","Georgia","Georgia,Bold","Georgia-BoldItalic"));
		addFontFamily("Helvetica",Arrays.asList("Helvetica-Oblique","Helvetica-BoldOblique","Helvetica-Bold","Helvetica"));
		addFontFamily("Humanst521 BT",Arrays.asList("Humanst521 Lt BT","Humanst521 BT"));
		addFontFamily("Handwriting-Dakota",Arrays.asList("Handwriting-Dakota"));
		addFontFamily("Impact",Arrays.asList("Impact"));
		addFontFamily("LucidaSansUnicode",Arrays.asList("LucidaSansUnicode"));
		addFontFamily("OpenSymbol",Arrays.asList("OpenSymbol"));
		addFontFamily("SymbolMT",Arrays.asList("SymbolMT"));
		addFontFamily("SegoeUI",Arrays.asList("SegoeUI"));
		addFontFamily("TrebuchetMS",Arrays.asList("TrebuchetMS-Italic","TrebuchetMS-Bold","TrebuchetMS"));
		addFontFamily("Times New Roman",Arrays.asList("Times New Roman","Times New Roman,Bold","Times-Bold","Times-Roman","Times New Roman,Italic","TimesNewRomanPS-BoldMT","TimesNewRomanPSMT","Times-Italic","Times-BoldItalic","TimesNewRomanPS-ItalicMT"));
		addFontFamily("Tahoma",Arrays.asList("Tahoma","Tahoma-Bold"));
		addFontFamily("Tw Cen MT",Arrays.asList("TwCenMT","Tw Cen MT,Bold","Tw Cen MT","TwCenMT,Bold","Tw Cen MT,BoldItalic"));
		addFontFamily("Verdana",Arrays.asList("Verdana","Verdana,Bold"));
		addFontFamily("Wingdings",Arrays.asList("Wingdings","Wingdings-Regular","Wingdings2","Wingdings 2"));
		
	}

	private static void addFontFamily(String fontName, List<String> subFonts) {
		if (fontFamilys.containsKey(fontName))
			throw new RuntimeException("font family already exist:"+fontName);
		fontFamilys.put(fontName, new HashSet<String>(subFonts));
	}
	
	public static String getFontFamily(String subFontToSearch) {
		subFontToSearch=FontExtractor.removeFontPrefixTag(subFontToSearch);
		for (String fontFamily  : fontFamilys.keySet()) {
			HashSet<String> subFonts = fontFamilys.get(fontFamily);
			if (subFonts.contains(subFontToSearch)) {
				return fontFamily;
			}
		}
		return "UNKNOWN FONT ("+subFontToSearch+")";
	}
}
