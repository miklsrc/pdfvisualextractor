package evaluation.similarity;

import java.util.LinkedList;
import java.util.List;

import evaluation.similarity.metrics.Cosine;
import evaluation.similarity.metrics.Jaccard;
import evaluation.similarity.metrics.JaroWinkler;
import evaluation.similarity.metrics.NormalizedDamerauLevenshtein;
import evaluation.similarity.metrics.NormalizedStringSimilarity;

public class StringSimilarityMetrics {

public static List<NormalizedStringSimilarity> metrics=new LinkedList<>();
	
	static {
		metrics.add(new NormalizedDamerauLevenshtein());
		metrics.add(new Cosine());
		metrics.add(new Jaccard());
		metrics.add(new JaroWinkler());
	}
	
}
