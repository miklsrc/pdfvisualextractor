package evaluation;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import annotation.definitions.Annotations;
import data.FontCollection;
import logic.PdfConnection;
import pdf.elements.PdfPage;
import pdf.elements.PdfParagraph;
import pdf.text.ParagraphTextExtractor.PdfStreamChar;
import util.Collection;
import util.interfaces.Collector;
import util.interfaces.Visitable;

public class PdfElementBrowser {

	public static Collector<BufferedImage, PdfPage> pictureCollector = pdfPage -> {
		List<BufferedImage> pictures = new LinkedList<>();
		pictures.addAll(pdfPage.pdfPictures.stream().map(e -> e.getPicture()).collect(Collectors.toList()));
		pictures.addAll(pdfPage.pdfCompositions.stream().filter(e -> e.annotations.contains(Annotations.COMPOSITION_PICTURE)).map(e -> e.compositionImage).collect(Collectors.toList()));
		return pictures;
	};

	public static Collector<BufferedImage, PdfPage> tableCollector = pdfPage -> {
		return pdfPage.pdfCompositions.stream().filter(e -> e.annotations.contains(Annotations.COMPOSITION_TABLE)).map(e -> e.compositionImage).collect(Collectors.toList());
	};

	public static Collector<BufferedImage, PdfPage> chartCollector = pdfPage -> {
		return pdfPage.pdfCompositions.stream().filter(e -> e.annotations.contains(Annotations.COMPOSITION_CHART)).map(e -> e.compositionImage).collect(Collectors.toList());
	};

	public static int count(Collector<?, PdfPage> elementCollector, PdfConnection pdfConnection) {
		int size = 0;

		for (int i = 0; i < pdfConnection.getPageCount(); i++) {
			PdfPage pdfPage = pdfConnection.getPdfPage(i);
			size += elementCollector.collect(pdfPage).size();
		}
		return size;
	}

	public static int findContainingPage(PdfParagraph pdfParagraph, PdfConnection pdfConnection) {
		for (int i = 0; i < pdfConnection.getPageCount(); i++) {
			PdfPage pdfPage = pdfConnection.getPdfPage(i);
			if (pdfPage.pdfParagraphs.contains(pdfParagraph)) {
				return i;
			}
		}
		throw new RuntimeException("not found");
	}

	public static ArrayList<PdfParagraph> collectParagraphs(PdfConnection pdfConnection) {
		ArrayList<PdfParagraph> pdfParagraphs = new ArrayList<>();
		for (int i = 0; i < pdfConnection.getPageCount(); i++) {
			if (pdfConnection.containsPage(i)) {
				PdfPage pdfPage = pdfConnection.getPdfPage(i);
				pdfParagraphs.addAll(pdfPage.pdfParagraphs);
			}
		}
		return pdfParagraphs;
	}

	// //////

	public enum TextCharacteristic {
		bold, italic, underline, allChar, visibleChar, font, fontSize,color,arrow, listelement,enumeration;
	}

	public static void serialisePdfChars(Visitable<PdfStreamChar> visitor, List<PdfParagraph> paras) {
		for (PdfParagraph paragraph : paras) {
			for (PdfStreamChar pdfChar : paragraph.getPdfChars()) {
				visitor.visit(pdfChar);
			}
		}
	}

	public static Map<TextCharacteristic, Integer> countTextCharacteristics(List<PdfParagraph> paras) {
		HashMap<TextCharacteristic, Integer> stats = new HashMap<>();
		stats.put(TextCharacteristic.bold, 0);
		stats.put(TextCharacteristic.italic, 0);
		stats.put(TextCharacteristic.underline, 0);
		stats.put(TextCharacteristic.allChar, 0);
		stats.put(TextCharacteristic.visibleChar, 0);

		serialisePdfChars(pdfChar -> {
			if (pdfChar.isVisible()) {
				if (pdfChar.pdfChar.isBold)
					Collection.increaseValue(stats, TextCharacteristic.bold);
				if (pdfChar.pdfChar.isItalic)
					Collection.increaseValue(stats, TextCharacteristic.italic);
				if (pdfChar.pdfChar.isUnderlined)
					Collection.increaseValue(stats, TextCharacteristic.underline);
				Collection.increaseValue(stats, TextCharacteristic.visibleChar);
			}
			Collection.increaseValue(stats, TextCharacteristic.allChar);
		}, paras);

		return stats;
	}

	public static Map<String, Integer> getFontStats(List<PdfParagraph> paragraphs) {
		HashMap<String, Integer> stats = new HashMap<>();
		PdfElementBrowser.serialisePdfChars(pdfChar -> {
			if (pdfChar.isVisible()) {
				Collection.increaseValue(stats, FontCollection.getFontFamily(pdfChar.pdfChar.fontName));
			}
		}, paragraphs);
		return stats;
	}

	public static Map<Float, Integer> getFontSizeStats(List<PdfParagraph> paragraphs) {
		HashMap<Float, Integer> stats = new HashMap<>();
		PdfElementBrowser.serialisePdfChars(pdfChar -> {
			if (pdfChar.isVisible()) {
				Collection.increaseValue(stats, pdfChar.pdfChar.fontSizeInPt);
			}
		}, paragraphs);
		return stats;
	}

	public static Map<Integer, Integer> getColorStats(List<PdfParagraph> paragraphs) {
		HashMap<Integer, Integer> stats = new HashMap<>();
		PdfElementBrowser.serialisePdfChars(pdfChar -> {
			if (pdfChar.isVisible()) {
				Collection.increaseValue(stats, pdfChar.pdfChar.rgbColor);
			}
		}, paragraphs);
		return stats;
	}
	
	

}
