package evaluation.redundancy;

import java.util.ArrayList;

import pdf.elements.PdfParagraph;

public class RedundancyKeepAll extends RedundancyStrategy {

	@Override
	public ArrayList<PdfParagraph> handleRedundantParagraphs(ArrayList<PdfParagraph> pdfParagraphs) {
		return pdfParagraphs;
	}

	@Override
	public String getLabel() {
		return "KEEP_ALL_REDUNDANT";
	}

}
