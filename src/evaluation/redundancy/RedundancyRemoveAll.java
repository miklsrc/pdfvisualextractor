package evaluation.redundancy;

import java.util.ArrayList;
import java.util.HashSet;

import pdf.elements.PdfParagraph;
import util.StringUtil;

public class RedundancyRemoveAll extends RedundancyStrategy {

	@Override
	public ArrayList<PdfParagraph> handleRedundantParagraphs(ArrayList<PdfParagraph> pdfParagraphs) {
		
		HashSet<String> allParagraphsHashed = new HashSet<>();
		HashSet<String> duplicateParagraphsHashed = new HashSet<>();
		
		for (PdfParagraph paragraph : pdfParagraphs) {
			String paragraphText = StringUtil.removeWhiteSpaces(paragraph.getParagraphText());
			if (allParagraphsHashed.contains(paragraphText)) {
				duplicateParagraphsHashed.add(paragraphText);
			}
			allParagraphsHashed.add(paragraphText);
		}
		
		int i = 0;
		ArrayList<PdfParagraph> paragraphsWithoutDuplicates = new ArrayList<PdfParagraph>(pdfParagraphs);
		while (i < paragraphsWithoutDuplicates.size()) {
			PdfParagraph paragraph = paragraphsWithoutDuplicates.get(i);
			String paragraphText = StringUtil.removeWhiteSpaces(paragraph.getParagraphText());
			if (duplicateParagraphsHashed.contains(paragraphText)) {
				paragraphsWithoutDuplicates.remove(i);

				continue;
			}
			i++;
		}
		return paragraphsWithoutDuplicates;
	}
	
	@Override
	public String getLabel() {
		return "REMOVE_ALL_REDUNDANT";
	}

}
