package evaluation.redundancy;

import java.util.ArrayList;
import java.util.HashSet;

import pdf.elements.PdfParagraph;
import util.StringUtil;

public class RedundancyKeepOnlyOne extends RedundancyStrategy {

	@Override
	public ArrayList<PdfParagraph> handleRedundantParagraphs(ArrayList<PdfParagraph> pdfParagraphs) {
		ArrayList<PdfParagraph> paragraphsWithOneDuplicates = new ArrayList<PdfParagraph>(pdfParagraphs);

		int i = 0;
		HashSet<String> duplicateHashes = new HashSet<>();
		while (i < paragraphsWithOneDuplicates.size()) {
			PdfParagraph paragraph = paragraphsWithOneDuplicates.get(i);
			String paragraphText = StringUtil.removeWhiteSpaces(paragraph.getParagraphText());
			if (duplicateHashes.contains(paragraphText)) {
				paragraphsWithOneDuplicates.remove(i);

				continue;
			}
			duplicateHashes.add(paragraphText);
			i++;
		}
		return paragraphsWithOneDuplicates;
	}
	
	
	@Override
	public String getLabel() {
		return "KEEP_ONLY_ONE_REDUNDANT";
	}
}
