package evaluation.redundancy;

import java.util.ArrayList;

import pdf.elements.PdfParagraph;

public abstract class RedundancyStrategy {

	public abstract ArrayList<PdfParagraph> handleRedundantParagraphs(ArrayList<PdfParagraph> pdfParagraphs);

	public abstract String getLabel();
	
	@Override
	public String toString() {
		return getLabel();
	}
	
	@Override
	public int hashCode() {
		return getLabel().hashCode();
	}
}
