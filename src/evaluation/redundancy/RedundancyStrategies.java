package evaluation.redundancy;

import java.util.LinkedList;
import java.util.List;

public class RedundancyStrategies {

	public static List<RedundancyStrategy> strategies=new LinkedList<>();
	
	static {
		strategies.add(new RedundancyRemoveAll());
		strategies.add(new RedundancyKeepAll());
		strategies.add(new RedundancyKeepOnlyOne());
	}
}
