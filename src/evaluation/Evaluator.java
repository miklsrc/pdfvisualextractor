package evaluation;

import java.util.ArrayList;

import data.Materials;
import evaluation.EvalResultContainer.PdfResult;
import evaluation.assignment.ParagraphAssignmentStrategy;
import evaluation.excel.ExcelCreator;
import evaluation.excel.sheetwriter.IntertextualTextComparisonWriter;
import evaluation.excel.sheetwriter.PdfPagesWriter;
import evaluation.excel.sheetwriter.TextCharacteristicsWriter;
import evaluation.excel.sheetwriter.TextSegmentsWriter;
import evaluation.excel.sheetwriter.TextStatisticsWriter;
import evaluation.redundancy.RedundancyStrategy;
import evaluation.similarity.metrics.NormalizedStringSimilarity;
import logic.PdfConnection;
import pdf.elements.PdfParagraph;
import util.Collection;

public class Evaluator {

	private EvalResultContainer results;
	private ArrayList<PdfParagraph> pdf1Paragraphs;
	private ArrayList<PdfParagraph> pdf2Paragraphs;
	private float threshold;
	private NormalizedStringSimilarity strSimMetric;
	private ParagraphAssignmentStrategy assignmentStrategy;
	private RedundancyStrategy redundancyHandler;

	public Evaluator(RedundancyStrategy redundancyHandler, NormalizedStringSimilarity strSimMetric, float threshold, ParagraphAssignmentStrategy assignmentStrategy) {
		this.threshold = threshold;
		this.strSimMetric = strSimMetric;
		this.assignmentStrategy = assignmentStrategy;
		this.redundancyHandler = redundancyHandler;
	}

	public EvalResultContainer run(PdfConnection pdfConnection1, PdfConnection pdfConnection2, String xlsBasePath) {

		results = new EvalResultContainer();

		results.pdf1.pagecount = pdfConnection1.getPageCount();
		results.pdf2.pagecount = pdfConnection2.getPageCount();

		ArrayList<PdfParagraph> pdf1AllParagraphs = PdfElementBrowser.collectParagraphs(pdfConnection1);
		ArrayList<PdfParagraph> pdf2AllParagraphs = PdfElementBrowser.collectParagraphs(pdfConnection2);
		ArrayList<PdfParagraph> redundantParagraphs1 = new ArrayList<PdfParagraph>(pdf1AllParagraphs);
		ArrayList<PdfParagraph> redundantParagraphs2 = new ArrayList<PdfParagraph>(pdf2AllParagraphs);

		pdf1Paragraphs = redundancyHandler.handleRedundantParagraphs(pdf1AllParagraphs);
		pdf2Paragraphs = redundancyHandler.handleRedundantParagraphs(pdf2AllParagraphs);

		redundantParagraphs1.removeAll(pdf1Paragraphs);
		redundantParagraphs2.removeAll(pdf2Paragraphs);

		results.pdf1.paragraphs = pdf1Paragraphs;
		results.pdf2.paragraphs = pdf2Paragraphs;

		collectIconicsStats(results.pdf1, pdfConnection1);
		collectIconicsStats(results.pdf2, pdfConnection2);

		results.pdf1.ignoreFormattings = Materials.isIgnoreFormatting(pdfConnection1.getPdfSourceFile());
		results.pdf2.ignoreFormattings = Materials.isIgnoreFormatting(pdfConnection2.getPdfSourceFile());

		SimilarityResult[][] simMatrix = createSimilarityMatrix();
		results.similarityResults = assignmentStrategy.assignParagraphs(simMatrix, threshold);

		if (xlsBasePath != null && xlsBasePath.length() > 0) {
			int scale1 = 1;
			int scale2 = 2;
			String duplicatesStr = redundancyHandler.getLabel();
			ExcelCreator excelCreator = new ExcelCreator();
			PdfPagesWriter pw = new PdfPagesWriter(pdfConnection1, scale1, redundantParagraphs1, pdfConnection2, scale2, redundantParagraphs1, results);
			IntertextualTextComparisonWriter itcw = new IntertextualTextComparisonWriter(pdfConnection1, scale1, pdfConnection2, scale2, results);
			TextSegmentsWriter writerTextSegments = new TextSegmentsWriter(pdf1Paragraphs, pdfConnection1, scale1, pdf2Paragraphs, pdfConnection2, scale2);
			TextStatisticsWriter tsw = new TextStatisticsWriter(Collection.createOneItemList(results.getResults()));
			TextCharacteristicsWriter tcw = new TextCharacteristicsWriter(pdfConnection1, pdfConnection2, results);

			pw.writeSheet(excelCreator.CreateSheet("pages"));
			writerTextSegments.writeSheet(excelCreator.CreateSheet("textsegments"));
			itcw.writeSheet(excelCreator.CreateSheet("comparison"));
			tsw.writeSheet(excelCreator.CreateSheet("statistics"));
			tcw.writeSheet(excelCreator.CreateSheet("text characteristics"));
			excelCreator.saveAsFile(xlsBasePath + "exp_verbal_" + duplicatesStr + ".xls");
		}
		return results;
	}

	private void collectIconicsStats(PdfResult pdf, PdfConnection pdfConnection) {
		pdf.countPictures = PdfElementBrowser.count(PdfElementBrowser.pictureCollector, pdfConnection);
		pdf.countCharts = PdfElementBrowser.count(PdfElementBrowser.chartCollector, pdfConnection);
		pdf.countTables = PdfElementBrowser.count(PdfElementBrowser.tableCollector, pdfConnection);
	}


	private SimilarityResult[][] createSimilarityMatrix() {
		SimilarityResult[][] similarities = new SimilarityResult[pdf1Paragraphs.size()][pdf2Paragraphs.size()];

		for (int p1 = 0; p1 < pdf1Paragraphs.size(); p1++) {
			PdfParagraph pdfPageParagraph1 = pdf1Paragraphs.get(p1);
			String s1 = pdfPageParagraph1.getParagraphText();
			pdfPageParagraph1.getParagraphText();
			
			for (int p2 = 0; p2 < pdf2Paragraphs.size(); p2++) {
				PdfParagraph pdfPageParagraph2 = pdf2Paragraphs.get(p2);
				String s2 = pdfPageParagraph2.getParagraphText();
				pdfPageParagraph2.getParagraphText();
				
				SimilarityResult sr = new SimilarityResult();
				sr.paragraph1 = pdfPageParagraph1;
				sr.paragraph2 = pdfPageParagraph2;
				sr.similarity = (float) strSimMetric.similarity(s1, s2);
				similarities[p1][p2] = sr;
			}
		}
		return similarities;
	}

}
