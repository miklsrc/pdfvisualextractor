package evaluation;

import pdf.elements.PdfParagraph;

public class SimilarityResult
{
	public float similarity;
	public PdfParagraph paragraph1;
	public PdfParagraph paragraph2;

	@Override
	public String toString() {
		String simStr = String.format("%.2f",similarity);
		return simStr+"---------\n"+paragraph1.getParagraphText()+"\n"+paragraph2.getParagraphText();

	}
	
	
}