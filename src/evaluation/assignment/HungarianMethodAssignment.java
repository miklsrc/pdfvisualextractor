package evaluation.assignment;

import java.util.ArrayList;

import evaluation.SimilarityResult;
import util.HungarianAlgorithm;
import util.MatrixUtils;

public class HungarianMethodAssignment implements ParagraphAssignmentStrategy {

	@Override
	public ArrayList<SimilarityResult> assignParagraphs(SimilarityResult[][] simMatrix, float threshold) {
		// invert similarity values, because Hungarian Algorithm working with "costs" => lower value is better assignment
		double[][] shrinkCostDoubleMatrix = MatrixUtils.convertToPrimitveDoubleArray(simMatrix, e -> 1d - e.similarity);
		HungarianAlgorithm hungarianAlgorithm = new HungarianAlgorithm(shrinkCostDoubleMatrix);
		int[] results = hungarianAlgorithm.execute();

		// TableViewer.showInViewer(simMatrix, e->(double)((SimilarityResult)e).similarity);

		// collect similarities
		ArrayList<SimilarityResult> similarities = new ArrayList<>();
		for (int i = 0; i < results.length; i++) {
			if (results[i] >= 0) {
				SimilarityResult simRes = simMatrix[i][results[i]];
				if (simRes.similarity > threshold)
					similarities.add(simRes);
			}
		}
		return similarities;
	}

	@Override
	public String toString() {
		return "Hungarian Method";
	}

	
	
}
