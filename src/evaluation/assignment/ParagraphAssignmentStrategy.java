package evaluation.assignment;

import java.util.ArrayList;

import evaluation.SimilarityResult;

public interface ParagraphAssignmentStrategy {


	ArrayList<SimilarityResult> assignParagraphs(SimilarityResult[][] simMatrix, float threshold);

}
