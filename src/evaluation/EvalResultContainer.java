package evaluation;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import annotation.definitions.Annotations;
import data.Materials;
import evaluation.PdfElementBrowser.TextCharacteristic;
import pdf.elements.PdfParagraph;
import pdf.elements.PdfString;
import util.Collection;
import util.MathUtils;
import util.interfaces.Convertible;

public class EvalResultContainer {

	public class PdfResult {

		public List<PdfParagraph> paragraphs;
		public int pagecount = 0;
		public int countPictures;
		public int countTables;
		public int countCharts;
		private Map<TextCharacteristic, Integer> textDecorations;
		private Convertible<SimilarityResult, PdfParagraph> pdfSelector;
		private int countIntertextualChars;
		private int countArrow;
		private int countList;
		private int countEnumeration;
		private double standardDeviationLength;
		public boolean ignoreFormattings = false;

		public PdfResult(Convertible<SimilarityResult, PdfParagraph> pdfSelector) {
			this.pdfSelector = pdfSelector;
		}

		private void calc() {
			textDecorations = PdfElementBrowser.countTextCharacteristics(paragraphs);
			countIntertextualChars = countIntertextualChars(pdfSelector);

			countArrow = paragraphs.stream().filter(e -> e.annotations.contains(Annotations.ARROW)).collect(Collectors.toList()).size();
			countList = paragraphs.stream().filter(e -> e.annotations.contains(Annotations.LIST)).collect(Collectors.toList()).size();
			countEnumeration = paragraphs.stream().filter(e -> e.annotations.contains(Annotations.ENUMERATION)).collect(Collectors.toList()).size();
			standardDeviationLength = calcLengthStandardDeviation();
		}

		private double calcLengthStandardDeviation() {
			List<Double> lengths = paragraphs.stream().map(e -> (double) e.getParagraphText().length()).collect(Collectors.toList());
			return MathUtils.calcStandardDeviation(lengths);
		}

		public Map<String, Integer> getResults(String prefix) {
			calc();
			LinkedHashMap<String, Integer> res = new LinkedHashMap<String, Integer>();
			res.put(prefix + "Seiten", pagecount);
			res.put(prefix + "Textsegmente", paragraphs.size());
			res.put(prefix + "Textsegmente enumeriert", countEnumeration);
			res.put(prefix + "Textsegmente anstrich", countList);
			res.put(prefix + "Textsegmente Pfeil", countArrow);

			res.put(prefix + "Zeichen", textDecorations.get(TextCharacteristic.allChar));
			res.put(prefix + "Textlänge Standardabweichung", (int) standardDeviationLength);

			res.put(prefix + "Zeichen sichtbar", textDecorations.get(TextCharacteristic.visibleChar));
			res.put(prefix + "Zeichen fett", ignoreFormattings ? -1 : textDecorations.get(TextCharacteristic.bold));
			res.put(prefix + "Zeichen kursiv", ignoreFormattings ? -1 : textDecorations.get(TextCharacteristic.italic));
			res.put(prefix + "Zeichen unterstrichen", ignoreFormattings ? -1 : textDecorations.get(TextCharacteristic.underline));
			res.put(prefix + "Zeichen intertextuell", countIntertextualChars);

			res.put(prefix + "Bilder", countPictures);
			res.put(prefix + "Tabellen", countTables);
			res.put(prefix + "zusammengesetzte Illustrationen", countCharts);

			res.put(prefix + "Anzahl Fonts", PdfElementBrowser.getFontStats(paragraphs).size());
			res.put(prefix + "Anzahl Schriftgrößen", PdfElementBrowser.getFontSizeStats(paragraphs).size());
			res.put(prefix + "Anzahl Farben", PdfElementBrowser.getColorStats(paragraphs).size());

			return res;
		}

		public void print() {
			calc();
			System.out.println("pages: " + pagecount);
			System.out.println("paragraphs count: " + paragraphs.size());

			System.out.println("characters all: " + textDecorations.get(TextCharacteristic.allChar));
			System.out.println("characters visible: " + textDecorations.get(TextCharacteristic.visibleChar));
			System.out.println("count  pictures: " + countPictures);
			System.out.println("count  charts: " + countCharts);
			System.out.println("count  tables: " + countTables);

			int visChars = textDecorations.get(TextCharacteristic.visibleChar);
			System.out.println("characters visible: " + visChars);
			System.out.println("characters bold: " + createPercentStr(visChars, ignoreFormattings ? -1 : textDecorations.get(TextCharacteristic.bold)));
			System.out.println("characters italic: " + createPercentStr(visChars, ignoreFormattings ? -1 : textDecorations.get(TextCharacteristic.italic)));
			System.out.println("characters underline: " + createPercentStr(visChars, ignoreFormattings ? -1 : textDecorations.get(TextCharacteristic.underline)));
			System.out.println("intertextual pairs: " + createPercentStr(paragraphs.size(), similarityResults.size()));
			System.out.println("intertextual chars:" + createPercentStr(pdf1.textDecorations.get(TextCharacteristic.allChar), countIntertextualChars));

			Map<String, Integer> fontStats = PdfElementBrowser.getFontStats(paragraphs);
			for (String font : fontStats.keySet()) {
				System.out.println("font: " + font + createPercentStr(visChars, fontStats.get(font)));
			}
		}
	}

	public List<SimilarityResult> similarityResults;
	public PdfResult pdf1 = new PdfResult(simRes -> {
		return simRes.paragraph1;
	});
	public PdfResult pdf2 = new PdfResult(simRes -> {
		return simRes.paragraph2;
	});

	private int countIntertextualChars(Convertible<SimilarityResult, PdfParagraph> pdfSelector) {
		int length = 0;
		for (SimilarityResult simResult : similarityResults) {
			PdfParagraph pdfPara = pdfSelector.convert(simResult);

			for (PdfString pdfString : pdfPara.pdfStrings) {
				length += pdfString.getPdfChars().size();
			}
		}
		return length;
	}

	public void print() {
		System.out.println("srs:" + similarityResults.size());

		System.out.println("[[pdf1]]");
		pdf1.print();
		System.out.println("[[pdf2]]");
		pdf2.print();

		// for (SimilarityResult similarityResult : similarityResults) {
		// System.out.println("__________________srs_____________" + similarityResult.similarity);
		// System.out.println(similarityResult.paragraph1.toString());
		// System.out.println(similarityResult.paragraph2.toString());
		// }
	}

	public LinkedHashMap<String, Integer> getResults() {
		LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
		res.put("Intertextuelle Textsegmente", similarityResults.size());
		res.put("Intertextuelle Textsegmente "+Config.intertextualThreshold+"-"+Config.intertextualThresholdHigh, similarityResults.stream().filter(e -> e.similarity > Config.intertextualThreshold && e.similarity <= Config.intertextualThresholdHigh).collect(Collectors.toList()).size());
		res.put("Intertextuelle Textsegmente "+Config.intertextualThresholdHigh+" -1.0", similarityResults.stream().filter(e -> e.similarity > Config.intertextualThresholdHigh).collect(Collectors.toList()).size());
		res.putAll(getIntertextualDifferencesStats());
		res.putAll(pdf1.getResults("1 "));
		res.putAll(pdf2.getResults("2 "));

		return res;
	}

	private LinkedHashMap<String, Integer> getIntertextualDifferencesStats() {
		LinkedHashMap<String, Integer> res = new LinkedHashMap<String, Integer>();
		
		boolean ignoreFormattings = pdf1.ignoreFormattings || pdf2.ignoreFormattings;
		
		Map<TextCharacteristic,Integer> differencesCount=new HashMap<>();
		int cntDifferentIntertexes=0;
		for (SimilarityResult simRes : similarityResults) {
			List<TextCharacteristic> differences = compare(simRes.paragraph1,simRes.paragraph2,ignoreFormattings);
			if (differences.size()>0) {
				cntDifferentIntertexes++;
				for (TextCharacteristic diffTextCharacteristic : differences) {
					Collection.increaseValue(differencesCount, diffTextCharacteristic);
				}
			}
		}
		res.put("unterschiedliche intertextuelle TS",cntDifferentIntertexes);
		for (TextCharacteristic tc :  TextCharacteristic.values()) {
			if (tc==TextCharacteristic.allChar) { //not included in data
				continue; 
			}
			res.put("unterschiedliche "+tc,differencesCount.containsKey(tc)?differencesCount.get(tc).intValue():0);
		}
		
		return res;
	}

	public static String createPercentStr(int all, int part) {
		double percentage = 100. / all * part;
		return String.format("%d/%d (%.2f", part, all, percentage) + "%)";
	}

	/**
	 * return a list with characteristics which differs between both paragraphs
	 * 
	 * @param para1
	 * @param para2
	 * @param isScannedFile
	 * @return
	 */
	public static List<TextCharacteristic> compare(PdfParagraph para1, PdfParagraph para2, boolean isScannedFile) {

		LinkedList<TextCharacteristic> differentCharacteristics = new LinkedList<>();

		List<PdfParagraph> paraList1 = Collection.createOneItemList(para1);
		List<PdfParagraph> paraList2 = Collection.createOneItemList(para2);
		Map<TextCharacteristic, Integer> textCharacteristics1 = PdfElementBrowser.countTextCharacteristics(paraList1);
		Map<TextCharacteristic, Integer> textCharacteristics2 = PdfElementBrowser.countTextCharacteristics(paraList2);

		List<TextCharacteristic> tcs;
		if (isScannedFile) {
			tcs = Materials.getTextCharacteristicsWithoutFormattings();
		} else {
			tcs = Materials.getCountableTextCharacteristics();
		}
		for (TextCharacteristic tc : tcs) {
			int i1 = textCharacteristics1.get(tc).intValue();
			int i2 = textCharacteristics2.get(tc).intValue();

			if (i1 != i2 && ((tc == TextCharacteristic.visibleChar) || (i1 + i2 > 0 && i1 * i2 == 0))) {
				differentCharacteristics.add(tc);
			}
		}

		if (!isScannedFile) {
			if (!hasSameCharacteristics(PdfElementBrowser.getFontStats(paraList1), PdfElementBrowser.getFontStats(paraList2))) {
				differentCharacteristics.add(TextCharacteristic.font);
			}
			if (!hasSameCharacteristics(PdfElementBrowser.getFontSizeStats(paraList1), PdfElementBrowser.getFontSizeStats(paraList2))) {
				differentCharacteristics.add(TextCharacteristic.fontSize);
			}
			if (!hasSameCharacteristics(PdfElementBrowser.getColorStats(paraList1), PdfElementBrowser.getColorStats(paraList2))) {
				differentCharacteristics.add(TextCharacteristic.color);
			}
		}
		
		if (para1.annotations.contains(Annotations.ARROW) != para2.annotations.contains(Annotations.ARROW)) 
		{
			differentCharacteristics.add(TextCharacteristic.arrow);
		}
		if (para1.annotations.contains(Annotations.LIST) != para2.annotations.contains(Annotations.LIST)) 
		{
			differentCharacteristics.add(TextCharacteristic.listelement);
		}
		if (para1.annotations.contains(Annotations.ENUMERATION) != para2.annotations.contains(Annotations.ENUMERATION)) 
		{
			differentCharacteristics.add(TextCharacteristic.enumeration);
		}
	
		return differentCharacteristics;

	}

	private static <T> boolean hasSameCharacteristics(Map<T, Integer> characteristics1, Map<T, Integer> characteristics2) {
		for (T charact1 : characteristics1.keySet()) {
			if (characteristics2.containsKey(charact1)) {
				int i1 = characteristics1.get(charact1).intValue();
				int i2 = characteristics2.get(charact1).intValue();
				if (!(i1 + i2 > 0 && i1 * i2 == 0)) { //check if one value is 0 and the other not -> different characteristics
					continue;
				}
			}
			return false;
		}
		return true;
	}
}
