package evaluation.excel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;

public class TwoColLayout {

	public static void Prepare2ColExcelSheet(ExcelSheetBridge excelBridge, int affectedRows, int indexSplitCol) {

		CellStyle rowStyle = excelBridge.createCellStyle();
		rowStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		rowStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		// rowStyle.setBorderBottom((short) 1);

		CellStyle splitColStyle = excelBridge.createCellStyle();
		splitColStyle.setFillForegroundColor(HSSFColor.WHITE.index);
		splitColStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		CellStyle splitColNeighbourStyle = excelBridge.createCellStyle();
		splitColNeighbourStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		splitColNeighbourStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		splitColNeighbourStyle.setBorderBottom((short) 1);

		for (int i = 0; i < affectedRows; i++) {

			Row row = excelBridge.sheet.getRow(i);
			if (row == null) {
				row = excelBridge.sheet.createRow(i);
			}

			Cell splitColCell = excelBridge.getCell(i, indexSplitCol);

			row.setRowStyle(rowStyle);

			splitColCell.setCellStyle(splitColStyle);

			Cell splitColLeftCell = excelBridge.getCell(i,indexSplitCol - 1);
			splitColLeftCell.setCellStyle(splitColNeighbourStyle);

			Cell splitColRightCell = excelBridge.getCell(i,indexSplitCol + 1);
			splitColRightCell.setCellStyle(splitColNeighbourStyle);

		}
		HSSFCell cell = excelBridge.getCell(0,indexSplitCol);

		cell.setCellValue("split");
	}
}
