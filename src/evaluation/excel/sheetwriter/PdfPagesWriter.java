package evaluation.excel.sheetwriter;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.util.HSSFColor;

import annotation.definitions.Annotations;
import evaluation.EvalResultContainer;
import evaluation.PdfElementBrowser;
import evaluation.SimilarityResult;
import evaluation.excel.ExcelRange;
import evaluation.excel.ExcelSheetBridge;
import evaluation.excel.TwoColLayout;
import logic.PdfConnection;
import pdf.elements.PdfPage;
import pdf.elements.PdfParagraph;

public class PdfPagesWriter implements Sheetwriter {

	public class Config {
		int IMG_Row_START = 1;
		short IMG_COL_START = 1;
		short IMG_COLS_WIDTH = 13;
		int IMG_ROWS_HEIGHT = 37;
		int IMG_ROWS_GAP = 1;
		int ROWS_OVERLAY = 100;
	}

	private Config configCol = new Config();
	private int maxImgsCount = 0;
	private int scales[]; // ={scale col1, scale col2}
	public ExcelSheetBridge xlsBridge;
	private PdfConnection pdfConnection1;
	private PdfConnection pdfConnection2;
	private EvalResultContainer results;
	private ArrayList<PdfParagraph> redundantParagraphs = new ArrayList<>();

	public static final int PIC_COL_SIDE_LEFT = 0;
	public static final int PIC_COL_SIDE_RIGHT = 1;

	public PdfPagesWriter(PdfConnection pdfConnection1, int scale1, ArrayList<PdfParagraph> redundantParagraphs1, PdfConnection pdfConnection2, int scale2, ArrayList<PdfParagraph> redundantParagraphs2, EvalResultContainer results) {
		this.scales = new int[] { scale1, scale2 };
		this.pdfConnection1 = pdfConnection1;
		this.pdfConnection2 = pdfConnection2;
		this.redundantParagraphs.addAll(redundantParagraphs1);
		this.redundantParagraphs.addAll(redundantParagraphs2);
		this.results = results;
	}

	@Override
	public void writeSheet(ExcelSheetBridge xlsBridge) {
		this.xlsBridge = xlsBridge;

		addPictures(pdfConnection1, PIC_COL_SIDE_LEFT);
		addPictures(pdfConnection2, PIC_COL_SIDE_RIGHT);
		printBoundaryForElements(pdfConnection1, PIC_COL_SIDE_LEFT);
		printBoundaryForElements(pdfConnection2, PIC_COL_SIDE_RIGHT);
		printSimiliarityResults(pdfConnection1, pdfConnection2, results);

		finalizeSheet();
	}

	private void addPictures(PdfConnection pdfConnection, int picCol) {
		for (int pageNr = 0; pageNr < pdfConnection.getPageCount(); pageNr++) {
			if (pdfConnection.containsPage(pageNr)) {

				maxImgsCount = Math.max(maxImgsCount, pageNr);
				PdfPage pdfPage = pdfConnection.getPdfPage(pageNr);

				ExcelRange imageExcelRange = getExcelRangeForNr(pageNr, picCol);

				xlsBridge.addPictureToSide(pdfPage.pageImage, imageExcelRange);

				int col;
				// add page nr cell
				if (picCol == PIC_COL_SIDE_LEFT) {
					col = 0;
				} else {
					col=configCol.IMG_COL_START + configCol.IMG_COLS_WIDTH + 3;
				}
				xlsBridge.getCell(imageExcelRange.rowStart, col).setCellValue((pageNr + 1) + "");
			}
		}
	}

	private void printBoundaryForElements(PdfConnection pdfConnection, int imgColNr) {
		for (int i = 0; i < pdfConnection.getPageCount(); i++) {
			PdfPage pdfPage = pdfConnection.getPdfPage(i);
			List<Rectangle> rects = pdfPage.pdfParagraphs.stream().filter(e -> !redundantParagraphs.contains(e)).map(e -> e.getRectangle()).collect(Collectors.toList());
			printRects(i, imgColNr, pdfPage.pageImage, rects, 1, Color.gray);

			List<Rectangle> redundantRects = pdfPage.pdfParagraphs.stream().filter(e -> redundantParagraphs.contains(e)).map(e -> e.getRectangle()).collect(Collectors.toList());
			printRects(i, imgColNr, pdfPage.pageImage, redundantRects, 1, Color.black,"duplicate");

			rects = pdfPage.pdfPictures.stream().map(e -> e.getRectangle()).collect(Collectors.toList());
			rects.addAll(pdfPage.pdfImages.stream().filter(e -> e.annotations.contains(Annotations.COMPOSITION_PICTURE)).map(e -> e.getRectangle()).collect(Collectors.toList()));
			printRects(i, imgColNr, pdfPage.pageImage, rects, 2, Color.RED);

			rects = pdfPage.pdfCompositions.stream().filter(e -> e.annotations.contains(Annotations.COMPOSITION_CHART)).map(e -> e.getRectangle()).collect(Collectors.toList());
			printRects(i, imgColNr, pdfPage.pageImage, rects, 2, Color.GREEN);

			rects = pdfPage.pdfCompositions.stream().filter(e -> e.annotations.contains(Annotations.COMPOSITION_TABLE)).map(e -> e.getRectangle()).collect(Collectors.toList());
			printRects(i, imgColNr, pdfPage.pageImage, rects, 2, Color.MAGENTA);
		}
	}

	private void printRects(int pageNr, int imcColNr, BufferedImage pageImage, List<Rectangle> rects, float lineWith, Color color) {
		printRects(pageNr, imcColNr, pageImage, rects, lineWith, color, "");
	}

	private void printRects(int pageNr, int imcColNr, BufferedImage pageImage, List<Rectangle> rects, float lineWith, Color color, String text) {
		for (Rectangle rect : rects) {
			ExcelRange imgXlsRange = getExcelRangeForNr(pageNr, imcColNr);
			HSSFSimpleShape shape = xlsBridge.createShapeRectangle(pageImage, imgXlsRange, rect, lineWith, color);
			HSSFRichTextString str = new HSSFRichTextString(text);
			HSSFFont hSSFFont = xlsBridge.createFont();
	        hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
	        hSSFFont.setFontHeightInPoints((short) 16);
	        hSSFFont.setBold(true);
	        hSSFFont.setColor(HSSFColor.ORANGE.index);
	        str.applyFont(hSSFFont);
			shape.setString(str);
		}
	}

	private void printSimiliarityResults(PdfConnection pdfConnection1, PdfConnection pdfConnection2, EvalResultContainer results) {
		int counter = 0;
		Color color = Color.blue;
		for (SimilarityResult sr : results.similarityResults) {
			counter++;

			int pageNr1 = PdfElementBrowser.findContainingPage(sr.paragraph1, pdfConnection1);
			int pageNr2 = PdfElementBrowser.findContainingPage(sr.paragraph2, pdfConnection2);

			BufferedImage img1 = pdfConnection1.getPdfPage(pageNr1).pageImage;
			BufferedImage img2 = pdfConnection2.getPdfPage(pageNr2).pageImage;

			ExcelRange img1XlsRange = getExcelRangeForNr(pageNr1, 0);
			ExcelRange img2XlsRange = getExcelRangeForNr(pageNr2, 1);

			Rectangle rect1 = sr.paragraph1.getRectangle();
			Rectangle rect2 = sr.paragraph2.getRectangle();

			String simStr = String.format("%.2f", sr.similarity);
			HSSFRichTextString str = new HSSFRichTextString("" + counter + "(" + simStr + ")");
			HSSFSimpleShape shape1 = xlsBridge.createShapeRectangle(img1, img1XlsRange, rect1, 2, color);
			HSSFSimpleShape shape2 = xlsBridge.createShapeRectangle(img2, img2XlsRange, rect2, 2, color);

			shape1.setString(str);
			shape2.setString(str);

			xlsBridge.createShapeLine(img1, new Point(rect1.x, rect1.y), img1XlsRange, img2, new Point(rect2.x, rect2.y), img2XlsRange, 1, color);
		}
	}

	public ExcelRange getExcelRangeForNr(int pageNr, int picCol) {
		int rowStart = configCol.IMG_Row_START + pageNr * (configCol.IMG_ROWS_HEIGHT * scales[picCol] + configCol.IMG_ROWS_GAP);
		int rowEnd = rowStart + configCol.IMG_ROWS_HEIGHT * scales[picCol];

		short colStart;
		if (picCol == 0) {
			colStart = (short) (configCol.IMG_COL_START + (configCol.IMG_COLS_WIDTH + 2) * picCol);

		} else {
			colStart = (short) (configCol.IMG_COL_START + (configCol.IMG_COLS_WIDTH * scales[0] + 3) * picCol+1);

		}
		short colEnd = (short) (colStart + configCol.IMG_COLS_WIDTH * scales[picCol]);// (short) (configCol.IMG_COL_START + configCol.IMG_COLS_WIDTH*scales[picCol]+(configCol.IMG_COLS_WIDTH+2)*picCol);
		ExcelRange imageExcelRange = new ExcelRange(rowStart, rowEnd, colStart, colEnd);
		return imageExcelRange;
	}

	public void finalizeSheet() {
		int affectedRows = configCol.IMG_Row_START + maxImgsCount * (configCol.IMG_ROWS_HEIGHT + configCol.IMG_ROWS_GAP) + configCol.ROWS_OVERLAY;
		int indexSplitCol = configCol.IMG_COL_START + configCol.IMG_COLS_WIDTH + 1;
		TwoColLayout.Prepare2ColExcelSheet(xlsBridge, affectedRows, indexSplitCol);
	}

}
