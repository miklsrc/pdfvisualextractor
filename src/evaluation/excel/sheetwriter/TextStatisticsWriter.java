package evaluation.excel.sheetwriter;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import evaluation.excel.ExcelSheetBridge;

public class TextStatisticsWriter implements Sheetwriter {

	private int curRow = 1;
	private ExcelSheetBridge xlsBridge;
	private List<LinkedHashMap<String, Integer>> results;

	@Override
	public void writeSheet(ExcelSheetBridge xlsBridge) {
		this.xlsBridge=xlsBridge;
		
		if (results.size()>0) {
			prepareExcelSheet(results.get(0).keySet());
		}
		for (LinkedHashMap<String, Integer> evalResult : results) {
			addResult(evalResult.values());
		}
	}
	
	public TextStatisticsWriter(List<LinkedHashMap<String, Integer>> results) {
		this.results=results;
	}

	public void addResult(Collection<Integer> values) {
		int cell = 0;
		for (Integer value : values) {
			xlsBridge.getCell(curRow, cell++).setCellValue(value);
		}
		curRow++;
	}

	private void prepareExcelSheet(Set<String> titles) {
		int cell = 0;

		for (String colTitle : titles) {
			xlsBridge.getCell(0, cell++).setCellValue(colTitle);
		}
	}
}
