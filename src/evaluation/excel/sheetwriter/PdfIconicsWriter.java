package evaluation.excel.sheetwriter;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.CellType;

import evaluation.excel.ExcelSheetBridge;
import evaluation.excel.TwoColLayout;
import logic.PdfConnection;
import pdf.elements.PdfPage;
import util.KeyValuePair;
import util.interfaces.Collector;

public class PdfIconicsWriter implements Sheetwriter {
	private static final int SCALE = 10;
	private PdfConnection pdfConnection1;
	private PdfConnection pdfConnection2;
	private Collector<BufferedImage, PdfPage> imgCollector;
	private boolean destinctable;

	public PdfIconicsWriter(PdfConnection pdfConnection1, PdfConnection pdfConnection2, Collector<BufferedImage, PdfPage> imgCollector, boolean destinctable) {
		this.pdfConnection1=pdfConnection1;
		this.pdfConnection2=pdfConnection2;
		this.imgCollector=imgCollector;
		this.destinctable=destinctable;
	}


	@Override
	public void writeSheet(ExcelSheetBridge xlsBridge) {

		KeyValuePair<Integer, Integer> writtenArea1 = extract(xlsBridge,  pdfConnection1, 0);
		int split = writtenArea1.getValue() + 1;
		KeyValuePair<Integer, Integer> writtenArea2 = extract(xlsBridge,  pdfConnection2, split + 2);
		int maxRow = Math.max(writtenArea1.getKey(), writtenArea2.getKey());
		TwoColLayout.Prepare2ColExcelSheet(xlsBridge, maxRow, split);
		
		xlsBridge.getCell(maxRow+3, split).setCellValue("copy:");
		xlsBridge.getCell(maxRow+4, split).setCellValue("changed:");
		
	}

	/**
	 * @param xlsBridge
	 * @param imageCollector
	 * @param pdfConnection
	 * @param imgColNr
	 * @return <biggestRow,biggestCol>
	 */
	private KeyValuePair<Integer, Integer> extract(ExcelSheetBridge xlsBridge,  PdfConnection pdfConnection, int imgColNr) {
		List<BufferedImage> pictures = new LinkedList<>();

		for (int i = 0; i < pdfConnection.getPageCount(); i++) {
			PdfPage pdfPage = pdfConnection.getPdfPage(i);
			pictures.addAll(imgCollector.collect(pdfPage));

		}
		xlsBridge.getCell(0, imgColNr).setCellValue("Nr");
		if (destinctable) {
			xlsBridge.getCell(0, imgColNr + 1).setCellValue("ID");
		}
		int nr = 0;
		int currentRow = 1;
		int maxCol = currentRow;
		for (BufferedImage picture : pictures) {
			nr++;
			xlsBridge.getCell(currentRow, imgColNr).setCellValue(nr + "");
			if (destinctable) {
				xlsBridge.getCell(currentRow, imgColNr + 1).setCellValue("?");
			}
			KeyValuePair<Integer, Integer> picturePos = xlsBridge.addPicture(picture, currentRow, imgColNr + 2, SCALE);
			currentRow = picturePos.getKey();
			currentRow += 2;
			maxCol = Math.max(maxCol, picturePos.getValue());
		}

		if (destinctable) {
			xlsBridge.getCell(currentRow + 1, imgColNr).setCellValue("destinct:");
			String letter = CellReference.convertNumToColString(imgColNr + 1);
			HSSFCell formulaCell = xlsBridge.getCell(currentRow + 1, imgColNr + 1);
			formulaCell.setCellType(CellType.FORMULA);
			formulaCell.setCellFormula("SUM(IF(FREQUENCY(" + letter + "$2:" + letter + currentRow + "," + letter + "$2:" + letter + currentRow + ")>0,1))");
		}
		return new KeyValuePair<>(currentRow, maxCol);
	}



}