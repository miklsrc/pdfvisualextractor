package evaluation.excel.sheetwriter;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.List;

import data.Materials;
import evaluation.EvalResultContainer;
import evaluation.PdfElementBrowser;
import evaluation.PdfElementBrowser.TextCharacteristic;
import evaluation.SimilarityResult;
import evaluation.excel.ExcelSheetBridge;
import evaluation.excel.TwoColLayout;
import logic.PdfConnection;
import pdf.elements.PdfParagraph;
import util.Collection;
import util.KeyValuePair;

public class IntertextualTextComparisonWriter extends AbstractParagraphStatisticsSheetWriter {

	private int scale1;
	private int scale2;
	private PdfConnection pdfConnection2;
	private PdfConnection pdfConnection1;
	private EvalResultContainer results;
	private ExcelSheetBridge xlsBridge;

	public IntertextualTextComparisonWriter(PdfConnection pdfConnection1, int scale1, PdfConnection pdfConnection2, int scale2, EvalResultContainer results) {
		this.scale1 = scale1;
		this.scale2 = scale2;
		this.pdfConnection1 = pdfConnection1;
		this.pdfConnection2 = pdfConnection2;
		this.results = results;
	}

	public void writeSheet(ExcelSheetBridge xlsBridge) {
		this.xlsBridge = xlsBridge;
		int splitCol = 15;

		int currentRow = 1;
		int nr = 1;
		for (SimilarityResult sim : results.similarityResults) {
			xlsBridge.getCell(currentRow, 1).setCellValue(sim.paragraph1.toString());
			xlsBridge.getCell(currentRow, splitCol + 2).setCellValue(sim.paragraph2.toString());

			KeyValuePair<Integer, Integer> imgPos1 = addImage(pdfConnection1, currentRow + 1, 1, scale1, sim.paragraph1);
			int row1 = addParagraphStatistics(xlsBridge, pdfConnection1, Collection.createOneItemList(sim.paragraph1), imgPos1.getKey() + 1, 2);
			KeyValuePair<Integer, Integer> imgPos2 = addImage(pdfConnection2, currentRow + 1, splitCol + 2, scale2, sim.paragraph2);
			int row2 = addParagraphStatistics(xlsBridge, pdfConnection2, Collection.createOneItemList(sim.paragraph2), imgPos2.getKey() + 1, splitCol + 2);

			int row3 = writeParagraphDifferences(pdfConnection1, sim.paragraph1, pdfConnection2, sim.paragraph2, currentRow + 1, splitCol);

			xlsBridge.getCell(currentRow, 0).setCellValue(nr + "");
			xlsBridge.getCell(currentRow, splitCol).setCellValue(sim.similarity + "");

			currentRow = Math.max(Math.max(row1, row2), row3) + 2;
			nr++;
		}

		TwoColLayout.Prepare2ColExcelSheet(xlsBridge, currentRow + 2, splitCol);
	}

	private int writeParagraphDifferences(PdfConnection pdfConnection12, PdfParagraph paragraph1, PdfConnection pdfConnection22, PdfParagraph paragraph2, int row, int col) {

		boolean ignoreFormatting = Materials.isIgnoreFormatting(pdfConnection1.getPdfSourceFile()) || Materials.isIgnoreFormatting(pdfConnection2.getPdfSourceFile());

		List<TextCharacteristic> differences = EvalResultContainer.compare(paragraph1, paragraph2, ignoreFormatting);
		if (differences.size() > 0) {
			row += 2;
			xlsBridge.getCell(row, col).setCellValue(":differences:");
			for (TextCharacteristic diff : differences) {
				xlsBridge.getCell(++row, col).setCellValue(diff+"");
			}
		}
		return row;
	}

	private KeyValuePair<Integer, Integer> addImage(PdfConnection pdfConnection1, int row, int col, int scale, PdfParagraph paragraph) {
		int pageNr = PdfElementBrowser.findContainingPage(paragraph, pdfConnection1);

		Rectangle area1 = paragraph.getRectangle();
		BufferedImage img1 = pdfConnection1.getPdfPage(pageNr).pageImage;
		BufferedImage subImage1 = util.Image.getSubImage(img1, area1);
		KeyValuePair<Integer, Integer> imgPos1 = xlsBridge.addPicture(subImage1, row, col, 20 * scale);
		return imgPos1;
	}

}