package evaluation.excel.sheetwriter;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import data.Materials;
import evaluation.PdfElementBrowser;
import evaluation.PdfElementBrowser.TextCharacteristic;
import evaluation.excel.ExcelSheetBridge;
import logic.PdfConnection;
import pdf.elements.PdfParagraph;

public abstract class AbstractParagraphStatisticsSheetWriter implements Sheetwriter{

	protected int addParagraphStatistics(ExcelSheetBridge xlsBridge,PdfConnection pdfConnection, List<PdfParagraph> paragraphList, Integer row, int col) {

		boolean ignoreFormatting = Materials.isIgnoreFormatting(pdfConnection.getPdfSourceFile());

		Map<TextCharacteristic, Integer> textCharacteristics = PdfElementBrowser.countTextCharacteristics(paragraphList);
		List<TextCharacteristic> tcs = Materials.getTextCharacteristicsWithoutFormattings();
		for (TextCharacteristic textCharateristic : textCharacteristics.keySet()) {
			if (!ignoreFormatting || tcs.contains(textCharateristic)) {
				xlsBridge.getCell(row, col).setCellValue(textCharacteristics.get(textCharateristic));
				xlsBridge.getCell(row++, col + 1).setCellValue(textCharateristic + "");
			}
		}

		if (!ignoreFormatting) {
			Map<String, Integer> fontStats = PdfElementBrowser.getFontStats(paragraphList);
			for (String fontName : fontStats.keySet()) {
				xlsBridge.getCell(row, col).setCellValue(fontStats.get(fontName));
				xlsBridge.getCell(row++, col + 1).setCellValue(TextCharacteristic.font+":"+ fontName);
			}

			Map<Float, Integer> fontSizeStats = PdfElementBrowser.getFontSizeStats(paragraphList);
			for (Float fontSize : fontSizeStats.keySet()) {
				xlsBridge.getCell(row, col).setCellValue(fontSizeStats.get(fontSize));
				xlsBridge.getCell(row++, col + 1).setCellValue(TextCharacteristic.fontSize+":"+ fontSize);
			}

			Map<Integer, Integer> colorStats = PdfElementBrowser.getColorStats(paragraphList);
			for (int rgbColor : colorStats.keySet()) {
				Color color = new Color(rgbColor);
				String colorStr = color.getRed() + "," + color.getGreen() + "," + color.getBlue();
				xlsBridge.getCell(row, col).setCellValue(colorStats.get(rgbColor));
				xlsBridge.getCell(row++, col + 1).setCellValue(TextCharacteristic.color+":"+ colorStr);
			}
		}
		return row;
	}
}
