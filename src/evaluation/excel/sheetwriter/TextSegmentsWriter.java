package evaluation.excel.sheetwriter;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import evaluation.PdfElementBrowser;
import evaluation.excel.ExcelSheetBridge;
import evaluation.excel.TwoColLayout;
import logic.PdfConnection;
import pdf.elements.PdfParagraph;
import util.Collection;
import util.KeyValuePair;

public class TextSegmentsWriter extends AbstractParagraphStatisticsSheetWriter {

	private int scale1;
	private int scale2;
	private PdfConnection pdfConnection1;
	private PdfConnection pdfConnection2;

	private ExcelSheetBridge xlsBridge;
	private ArrayList<PdfParagraph> pdf1Paragraphs;
	private ArrayList<PdfParagraph> pdf2Paragraphs;

	public TextSegmentsWriter(ArrayList<PdfParagraph> pdf1Paragraphs, PdfConnection pdfConnection1, int scale1, ArrayList<PdfParagraph> pdf2Paragraphs, PdfConnection pdfConnection2, int scale2) {
		this.scale1 = scale1;
		this.scale2 = scale2;
		this.pdfConnection1 = pdfConnection1;
		this.pdfConnection2 = pdfConnection2;
		this.pdf1Paragraphs=pdf1Paragraphs;
		this.pdf2Paragraphs=pdf2Paragraphs;
	}

	@Override
	public void writeSheet(ExcelSheetBridge xlsBridge) {
		this.xlsBridge = xlsBridge;
		int splitCol = 15;

		int maxRowsLeft = writePdfParagraphs(xlsBridge,pdf1Paragraphs,pdfConnection1,0,scale1);
		int maxRowsRight = writePdfParagraphs(xlsBridge,pdf2Paragraphs,pdfConnection2,splitCol+2,scale2);

		int affectedRows=Math.max(maxRowsLeft, maxRowsRight);

		TwoColLayout.Prepare2ColExcelSheet(xlsBridge, affectedRows + 2, splitCol);
	}

	private int writePdfParagraphs(ExcelSheetBridge xlsBridge, ArrayList<PdfParagraph> pdfParagraphs, PdfConnection pdfConnection, int col,int scale) {
		int nr = 1;
		int currentRow = 1;
		for (PdfParagraph pdfParagraph : pdfParagraphs) {
			
			xlsBridge.getCell(currentRow, col+1).setCellValue(pdfParagraph.toString());
			KeyValuePair<Integer, Integer> imgPos1 = addImage(pdfConnection, currentRow + 1, col+1, scale, pdfParagraph);
			int row = addParagraphStatistics(xlsBridge,pdfConnection, Collection.createOneItemList(pdfParagraph), imgPos1.getKey() + 1, col+2);
			
			xlsBridge.getCell(currentRow, col).setCellValue(nr + "");
			currentRow = row + 2;
			nr++;
		}
		return currentRow;
	}


	private KeyValuePair<Integer, Integer> addImage(PdfConnection pdfConnection1, int row, int col, int scale, PdfParagraph paragraph) {
		int pageNr = PdfElementBrowser.findContainingPage(paragraph, pdfConnection1);

		Rectangle area1 = paragraph.getRectangle();
		BufferedImage img1 = pdfConnection1.getPdfPage(pageNr).pageImage;
		BufferedImage subImage1 = util.Image.getSubImage(img1, area1);
		KeyValuePair<Integer, Integer> imgPos1 = xlsBridge.addPicture(subImage1, row, col, 20 * scale);
		return imgPos1;
	}

}