package evaluation.excel.sheetwriter;

import evaluation.excel.ExcelSheetBridge;

public interface Sheetwriter {
	void writeSheet(ExcelSheetBridge xlsBridge);
}
