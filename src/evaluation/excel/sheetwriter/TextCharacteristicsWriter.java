package evaluation.excel.sheetwriter;

import evaluation.EvalResultContainer;
import evaluation.excel.ExcelSheetBridge;
import logic.PdfConnection;

public class TextCharacteristicsWriter extends AbstractParagraphStatisticsSheetWriter {

	private PdfConnection pdfConnection2;
	private PdfConnection pdfConnection1;
	private EvalResultContainer results;
	private ExcelSheetBridge xlsBridge;
	
	public TextCharacteristicsWriter(PdfConnection pdfConnection2, PdfConnection pdfConnection1, EvalResultContainer results) {
		this.pdfConnection1 = pdfConnection1;
		this.pdfConnection2 = pdfConnection2;
		this.results = results;
	}
	
	@Override
	public void writeSheet(ExcelSheetBridge xlsBridge) {
		this.xlsBridge=xlsBridge;
		
		addParagraphStatistics(xlsBridge,pdfConnection1, results.pdf1.paragraphs, 1, 1);
		addParagraphStatistics(xlsBridge,pdfConnection2, results.pdf2.paragraphs, 1, 10);
		
	}

	
}
