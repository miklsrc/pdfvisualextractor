package evaluation.excel;

import evaluation.PdfElementBrowser;
import evaluation.excel.sheetwriter.PdfIconicsWriter;
import logic.PdfConnection;

public class PdfIconics2ExcelExporter extends ExcelCreator {

	public void exportToExcel(PdfConnection pdfConnection1, PdfConnection pdfConnection2) {
		PdfIconicsWriter pictureSheetWriter=new PdfIconicsWriter(pdfConnection1, pdfConnection2, PdfElementBrowser.pictureCollector, true);
		pictureSheetWriter.writeSheet(CreateSheet("pictures"));
		
		PdfIconicsWriter tablesSheetWriter=new PdfIconicsWriter(pdfConnection1, pdfConnection2, PdfElementBrowser.tableCollector, true);
		tablesSheetWriter.writeSheet(CreateSheet("tables"));
		
		PdfIconicsWriter chartsSheetWriter=new PdfIconicsWriter(pdfConnection1, pdfConnection2, PdfElementBrowser.chartCollector, true);
		chartsSheetWriter.writeSheet(CreateSheet("charts"));
	}
}
