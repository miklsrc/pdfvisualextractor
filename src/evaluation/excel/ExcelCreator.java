package evaluation.excel;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelCreator {

	protected HSSFWorkbook wb;

	public ExcelCreator() {
		wb = new HSSFWorkbook();
	}

	public ExcelSheetBridge CreateSheet(String sheetTitle) {
		ExcelSheetBridge excelBridge = new ExcelSheetBridge(wb,sheetTitle);
		return excelBridge;
	}
	
	public void saveAsFile(String outputFileName) {
		try {
			FileOutputStream fileOut = new FileOutputStream(outputFileName);
			wb.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
