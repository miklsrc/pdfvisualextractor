package evaluation.excel;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;

import util.KeyValuePair;

/**
 * @author mk
 *
 */
public class ExcelSheetBridge {

	private HSSFWorkbook wb;
	protected HSSFSheet sheet;
	protected HSSFPatriarch patriarch;

	public static final int DEFAULT_CELL_WIDTH = 1024;
	public static final int DEFAULT_CELL_HEIGHT = 256;

	public ExcelSheetBridge(HSSFWorkbook wb, String sheetTitle) {
		this.wb = wb;
		sheet = wb.createSheet(sheetTitle);
		patriarch = sheet.createDrawingPatriarch();
	}

	public HSSFCell getCell(int rowNr, int colNr) {
		HSSFRow row = sheet.getRow(rowNr);
		if (row == null) {
			row = sheet.createRow(rowNr);
		}
		HSSFCell cell = row.getCell(colNr);
		if (cell == null) {
			cell = row.createCell(colNr);
		}
		return cell;
	}

	public KeyValuePair<Integer, Integer> addPicture(BufferedImage img, int row, int col, int scale) {

		int index = addPicAndGetIndexPng(img);

		int width = img.getWidth() * scale;
		int height = img.getHeight() * scale;

		int newRowIndex = height / 256 + row;
		int newInRowCoord = height % 246;

		int newColIndex = width / 1024 + col;
		int newInColCoord = width % 1024;

		HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, newInColCoord, newInRowCoord, (short) col, row, (short) newColIndex, newRowIndex);
		patriarch.createPicture(anchor, index);

		return new KeyValuePair<>(newRowIndex, newColIndex);

	}

	public HSSFSimpleShape createShapeRectangle(BufferedImage pageImage, ExcelRange imageExcelRange, Rectangle rectangleInPix, float lineWidth, Color color) {
		Point p1 = new Point(rectangleInPix.x, rectangleInPix.y);
		Point p2 = new Point(rectangleInPix.x + rectangleInPix.width, rectangleInPix.y + rectangleInPix.height);

		return createShape(pageImage, imageExcelRange, p1, pageImage, imageExcelRange, p2, HSSFSimpleShape.OBJECT_TYPE_RECTANGLE, lineWidth, color);
	}

	public void createShapeLine(BufferedImage img1, Point p1, ExcelRange imgXlsRange1, BufferedImage img2, Point p2, ExcelRange imgXlsRange2, float lineWidth, Color color) {
		createShape(img1, imgXlsRange1, p1, img2, imgXlsRange2, p2, HSSFSimpleShape.OBJECT_TYPE_LINE, lineWidth, color);
	}

	public HSSFSimpleShape createShape(BufferedImage img1, ExcelRange imgXlsRange1, Point p1, BufferedImage img2, ExcelRange imgXlsRange2, Point p2, short shapeType, float lineWidth, Color color) {
		KeyValuePair<Integer, Integer> xStart = calcCellAndCoord(imgXlsRange1.colStart, imgXlsRange1.colEnd, img1.getWidth(), p1.x, DEFAULT_CELL_WIDTH);
		KeyValuePair<Integer, Integer> xEnd = calcCellAndCoord(imgXlsRange2.colStart, imgXlsRange2.colEnd, img2.getWidth(), p2.x, DEFAULT_CELL_WIDTH);

		KeyValuePair<Integer, Integer> yStart = calcCellAndCoord(imgXlsRange1.rowStart, imgXlsRange1.rowEnd, img1.getHeight(), p1.y, DEFAULT_CELL_HEIGHT);
		KeyValuePair<Integer, Integer> yEnd = calcCellAndCoord(imgXlsRange2.rowStart, imgXlsRange2.rowEnd, img2.getHeight(), p2.y, DEFAULT_CELL_HEIGHT);

		// correct coords (happens when in pdf objects are outside of the visible frame)
		yStart.setValue(yStart.getValue() < 0 ? 0 : yStart.getValue());
		yEnd.setValue(yEnd.getValue() > DEFAULT_CELL_HEIGHT ? DEFAULT_CELL_HEIGHT - 1 : yEnd.getValue());
		xStart.setValue(xStart.getValue() < 0 ? 0 : xStart.getValue());
		xEnd.setValue(xEnd.getValue() > DEFAULT_CELL_WIDTH ? DEFAULT_CELL_WIDTH - 1 : xEnd.getValue());

		HSSFClientAnchor anchor = null;
		anchor = new HSSFClientAnchor(xStart.getValue(), yStart.getValue(), xEnd.getValue(), yEnd.getValue(), xStart.getKey().shortValue(), yStart.getKey(), (xEnd.getKey().shortValue()), yEnd.getKey());

		HSSFSimpleShape shape = patriarch.createSimpleShape(anchor);
		shape.setShapeType(shapeType);
		shape.setLineStyleColor(color.getRed(), color.getGreen(), color.getBlue());
		shape.setNoFill(true);
		shape.setLineWidth((int) (HSSFShape.LINEWIDTH_ONE_PT * lineWidth));
		shape.setLineStyle(HSSFShape.LINESTYLE_SOLID);

		return shape;
	}

	/**
	 * calculates the coordinates of a object, relative on a scaled picture. can be used for x- and y-dimension
	 * 
	 * @param picStartCell
	 *            col/row the picture starts
	 * @param picEndCell
	 *            col/row the picture ends
	 * @param picPixSize
	 *            width/heigth of the picture in pixels
	 * @param coord
	 *            the coordinate which is related to the pixelsize of the picture
	 * @param cellDim
	 *            width/height of the overlayed cells
	 * @return
	 */
	public KeyValuePair<Integer, Integer> calcCellAndCoord(int picStartCell, int picEndCell, int picPixSize, int coord, int cellDim) {
		int picDim = (picEndCell - picStartCell) * cellDim;
		int inExcel = (int) ((double) picDim / (double) picPixSize * (double) coord);
		int newCellIndex = inExcel / cellDim + picStartCell;
		int newInCellCoord = inExcel % cellDim;
		return new KeyValuePair<Integer, Integer>(newCellIndex, newInCellCoord);
	}

	// able to handle pictures with alpha channel
	public int addPicAndGetIndexPng(BufferedImage image) {
		return addPicAndGetIndex(image, "png", HSSFWorkbook.PICTURE_TYPE_PNG);
	}

	public int addPicAndGetIndexJpg(BufferedImage image) {
		return addPicAndGetIndex(image, "jpg", HSSFWorkbook.PICTURE_TYPE_JPEG);
	}

	private int addPicAndGetIndex(BufferedImage image, String type, int pictureType) {
		int index = -1;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, type, baos);
			InputStream picIn = new ByteArrayInputStream(baos.toByteArray());
			byte[] picData = null;
			long length = baos.size();
			picData = new byte[(int) length];
			picIn.read(picData);
			picIn.close();
			index = wb.addPicture(picData, pictureType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return index;
	}

	public CellStyle createCellStyle() {
		return wb.createCellStyle();
	}

	public HSSFFont createFont() {
		return wb.createFont();
	}
	public void addPictureToSide(BufferedImage pageImage, ExcelRange imageExcelRange) {
		int index = addPicAndGetIndexJpg(pageImage);
		HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, imageExcelRange.colStart, imageExcelRange.rowStart, imageExcelRange.colEnd, imageExcelRange.rowEnd);
		patriarch.createPicture(anchor, index);

	}

}
