package evaluation.excel;

public class ExcelRange {
	public int rowStart;
	public int rowEnd;
	public short colStart;
	public short colEnd;

	public ExcelRange(int rowStart, int rowEnd, short colStart, short colEnd) {
		this.rowStart = rowStart;
		this.rowEnd = rowEnd;
		this.colStart = colStart;
		this.colEnd = colEnd;
	}
}