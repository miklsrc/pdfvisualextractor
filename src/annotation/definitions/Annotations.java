package annotation.definitions;

public class Annotations {

	//paragraph
	public static final String REMOVE_IDENTIFIER="ri";
	public static final String ARROW="ar";
	public static final String LIST="li";
	public static final String ENUMERATION="en";
	
	//composition
	public static final String COMPOSITION_PICTURE="pic";
	public static final String COMPOSITION_TABLE="tbl";
	public static final String COMPOSITION_CHART="cha";
}
