package annotation;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pdf.elements.AbstractPdfElement;

public class AnnotationManager {

	private static AnnotationManager instance;

	private HashMap<String, ArrayList<Annotation>> annotations;
	private HashMap<String, Annotation> annotationsPerValueId = new HashMap<>();
	private HashMap<String, AnnotationValue> valuesById = new HashMap<>();

	public static AnnotationManager getInstance() {
		if (instance == null) {
			instance = new AnnotationManager();
		}
		return instance;
	}

	private AnnotationManager() {
		annotations = loadAnnotations();

		for (String type : annotations.keySet()) {
			for (Annotation anno : annotations.get(type)) {
				for (AnnotationValue value : anno.values) {
					annotationsPerValueId.put(value.id, anno);
					valuesById.put(value.id, value);
				}
			}
		}
	}

	public Image getAnnotationIconByValue(String annotationValue) {
		return valuesById.get(annotationValue).img;
	}

	public List<Annotation> getAnnotations(String type) {
		return annotations.get(type);
	}

	private HashMap<String, ArrayList<Annotation>> loadAnnotations() {
		HashMap<String, ArrayList<Annotation>> annotationsAll = new HashMap<>();
		try {
			InputStream annotationsXml = getClass().getResourceAsStream("/annotation/definitions/annotations.xml");

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			Document doc = dBuilder.parse(annotationsXml);
			doc.getDocumentElement().normalize();

			NodeList elementNodes = doc.getElementsByTagName("element");
			for (int k = 0; k < elementNodes.getLength(); k++) {
				Node elementNode = elementNodes.item(k);
				if (elementNode.getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element elementElement = (Element) elementNode;
				String pdfElementType = elementElement.getAttribute("type");

				ArrayList<Annotation> annotiationsPerElement = new ArrayList<>();
				NodeList annotationNodes = elementElement.getElementsByTagName("annotation");

				for (int i = 0; i < annotationNodes.getLength(); i++) {
					Node annotationNode = annotationNodes.item(i);
					Annotation annotation = new Annotation();

					if (annotationNode.getNodeType() != Node.ELEMENT_NODE)
						continue;

					Element annotationElement = (Element) annotationNode;

					NodeList valueNodes = annotationElement.getElementsByTagName("value");
					for (int j = 0; j < valueNodes.getLength(); j++) {

						Node valueNode = valueNodes.item(j);
						if (valueNode.getNodeType() != Node.ELEMENT_NODE)
							continue;

						Element valueElement = (Element) valueNode;
						String name = valueElement.getAttribute("name");
						String id = valueElement.getAttribute("id");
						String imgPath = valueElement.getAttribute("img");
						String key = valueElement.getAttribute("key");

						Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/annotation/definitions/" + imgPath));

						annotation.values.add(new AnnotationValue(name, id, key.charAt(0), image));

					}

					annotiationsPerElement.add(annotation);
				}
				annotationsAll.put(pdfElementType, annotiationsPerElement);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annotationsAll;
	}

	public void changeAnnotationValue(AbstractPdfElement paragraph, String annotationId) {
		Annotation anno = annotationsPerValueId.get(annotationId);
		boolean contains = paragraph.annotations.contains(annotationId);

		for (AnnotationValue value : anno.values) {
			paragraph.annotations.remove(value.id);
		}

		if (!contains) {
			paragraph.annotations.add(annotationId);
		}
	}

}
