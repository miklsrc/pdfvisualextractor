package annotation;

import java.awt.Image;

public class AnnotationValue {
	public String name;
	public char key;
	public Image img;
	public String id;

	public AnnotationValue(String name, String id, char key, Image img) {
		super();
		this.name = name;
		this.key = key;
		this.img = img;
		this.id = id;
	}
}