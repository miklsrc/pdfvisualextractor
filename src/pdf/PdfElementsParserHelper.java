package pdf;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;

public class PdfElementsParserHelper {

	public static  BufferedImage applyTransferFunction(BufferedImage image, COSBase transfer) throws IOException {
		BufferedImage bim;
		if (image.getColorModel().hasAlpha()) {
			bim = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		} else {
			bim = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		}

		// prepare transfer functions (either one per color or one for all)
		// and maps (actually arrays[256] to be faster) to avoid calculating
		// values several times
		Integer rMap[], gMap[], bMap[];
		PDFunction rf, gf, bf;
		if (transfer instanceof COSArray) {
			COSArray ar = (COSArray) transfer;
			rf = PDFunction.create(ar.getObject(0));
			gf = PDFunction.create(ar.getObject(1));
			bf = PDFunction.create(ar.getObject(2));
			rMap = new Integer[256];
			gMap = new Integer[256];
			bMap = new Integer[256];
		} else {
			rf = PDFunction.create(transfer);
			gf = rf;
			bf = rf;
			rMap = new Integer[256];
			gMap = rMap;
			bMap = rMap;
		}

		// apply the transfer function to each color, but keep alpha
		float input[] = new float[1];
		for (int x = 0; x < image.getWidth(); ++x) {
			for (int y = 0; y < image.getHeight(); ++y) {
				int rgb = image.getRGB(x, y);
				int ri = (rgb >> 16) & 0xFF;
				int gi = (rgb >> 8) & 0xFF;
				int bi = rgb & 0xFF;
				int ro, go, bo;
				if (rMap[ri] != null) {
					ro = rMap[ri];
				} else {
					input[0] = (ri & 0xFF) / 255f;
					ro = (int) (rf.eval(input)[0] * 255);
					rMap[ri] = ro;
				}
				if (gMap[gi] != null) {
					go = gMap[gi];
				} else {
					input[0] = (gi & 0xFF) / 255f;
					go = (int) (gf.eval(input)[0] * 255);
					gMap[gi] = go;
				}
				if (bMap[bi] != null) {
					bo = bMap[bi];
				} else {
					input[0] = (bi & 0xFF) / 255f;
					bo = (int) (bf.eval(input)[0] * 255);
					bMap[bi] = bo;
				}
				bim.setRGB(x, y, (rgb & 0xFF000000) | (ro << 16) | (go << 8) | bo);
			}
		}
		return bim;
	}
}
