package pdf.elements;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import util.Geometry;

public class PdfPicture extends AbstractPdfElement {

	private List<PdfImage> pdfImages= new LinkedList<PdfImage>();
	private int[] pdfImagesIds; //saving for later persistance
	private Rectangle rectangle;
	public final static String TYPE_NAME="picture";

	
	public PdfPicture(List<PdfImage> origpdfImages, int[] selectedIndices) {
		this.pdfImagesIds=selectedIndices;
		if (selectedIndices.length > 0) {
			for (int stringId : selectedIndices) {
				pdfImages.add(origpdfImages.get(stringId));
			}
		} else {
			throw new RuntimeException("no selectedIndices");
		}
		
		List<Rectangle> rects= new LinkedList<Rectangle>();
		pdfImages.forEach(pdfImg ->rects.add(pdfImg.getRectangle()));
		this.rectangle= Geometry.merge(rects);
	}
	
	@Override
	public Rectangle getRectangle() {
		return rectangle;
	}
	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}
	@Override
	public Color getColor() {
		return Color.RED;
	}
	public List<PdfImage> getPdfImages() {
		return pdfImages;
	}

	public BufferedImage getPicture() {
		return merge(pdfImages);
	}
	
	public int[] getPdfImagesIds() {
		return pdfImagesIds;
	}

	@Override
	public String toString() {
		return rectangle.toString();
	}
	
	public static BufferedImage merge(List<PdfImage> pdfImages) {
		if (pdfImages.size() == 0) {
			return null;
		}
		if (pdfImages.size()==1)
		{
			return pdfImages.get(0).image;
		}
		List<Rectangle> rects = new LinkedList<Rectangle>();

		pdfImages.forEach(pdfImage -> rects.add(pdfImage.getRectangle()));
		Rectangle boundingBox = Geometry.merge(rects);
		BufferedImage img = new BufferedImage(boundingBox.width, boundingBox.height, BufferedImage.TYPE_INT_ARGB);

		Graphics2D graphics = img.createGraphics();
		graphics.setBackground(Color.WHITE);
		for (PdfImage pdfImage : pdfImages) {
			graphics.drawImage(pdfImage.image, (int) pdfImage.boundary.getMinX() - boundingBox.x, (int) pdfImage.boundary.getMinY() - boundingBox.y, (int) pdfImage.boundary.getWidth(), (int) pdfImage.boundary.getHeight(), null);
		}
		graphics.dispose();
		return img;
	}
	
	
}
