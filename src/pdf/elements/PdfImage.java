package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;


public class PdfImage extends AbstractPdfElement {
	public String objectName;
	public BufferedImage image;
	public final static String TYPE_NAME="image";
	public Rectangle2D boundary;
	
	public PdfImage(String objectName, float posX, float posY, float width, float height, BufferedImage image) {
		super();
		this.objectName = objectName;
	
		boundary=new Rectangle2D.Float(posX,posY,width,height);
		this.image = image;
	}


	@Override
	public String toString() {
		return "PdfImage [objectName=" + objectName + " " + boundary + "]";
	}

	@Override
	public Rectangle getRectangle() {
		return boundary.getBounds();
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public Color getColor() {
		
		return Color.ORANGE;
	}

	
}