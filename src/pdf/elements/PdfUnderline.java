package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;

public class PdfUnderline extends PdfLinepath {

	public PdfUnderline(Rectangle rect, int rgb) {
		super(rect, rgb);
	}

	public final static String TYPE_NAME = "underline";

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public Color getColor() {
		return Color.magenta;
	}

}
