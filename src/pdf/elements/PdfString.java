package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class PdfString extends AbstractPdfElement {
	public final static String TYPE_NAME = "String";

	public String string;
	public Rectangle2D shape;
	private List<PdfChar> pdfChars;

	public PdfString(String string, Rectangle2D shape, List<PdfChar> stringTextPositions) {
		this.string = string;
		this.shape = shape.getBounds2D();
		this.pdfChars = stringTextPositions;
	}

	public List<PdfChar> getPdfChars() {
		return pdfChars;
	}

	@Override
	public Rectangle getRectangle() {
		return shape.getBounds();
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public Color getColor() {
		return Color.CYAN;
	}

	@Override
	public String toString() {
		return "|" + string + "|";
	}

	public void analyzeString() {
		String str = "";
		for (PdfChar pdfChar : pdfChars) {
			
			str +=pdfChar.textPosition.getFontSizeInPt()+" ";
			str +=pdfChar.textPosition.getFontSize()+" ";
			str +=pdfChar.textPosition.getWidth()+ " ";
			str +=pdfChar.textPosition.getHeight()+ " ";
			str +=pdfChar.textPosition.getXScale()+" ";
			str +=pdfChar.textPosition.getYScale()+" ";
			
//			TextPosition position = pdfChar.textPosition;
//			str += position.getUnicode() + " [";
//			str += position.getFont().getName() + " ";
//			str += position.getFont().getFontDescriptor().getFontFamily() + " ";
//			str += position.getFont().getFontDescriptor().getFontName() + " ";
//			str += position.getFont().getFontDescriptor().getFontWeight() + " ";
//			str += position.getFont().getFontDescriptor().getItalicAngle() + " ";
//			str += position.getFont().getFontDescriptor().isForceBold() + " ";
//			str += position.getFont().getFontDescriptor().isItalic() + " ";
//			str += position.getFont().getAverageFontWidth() + " ";
//			Color color = new Color(pdfChar.rgbColor);
//			str += color.getRed() + "," + color.getGreen() + "," + color.getBlue();

			str +=pdfChar+" <"+pdfChar.textPosition.getUnicode()+">";
			str +=pdfChar.textPosition.getUnicode().codePointAt(0)+ "\n";
			
		}
		System.out.println(str);
	}

}