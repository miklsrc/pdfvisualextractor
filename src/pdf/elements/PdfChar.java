package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;

import org.apache.pdfbox.text.TextPosition;

import util.StringUtil;

public class PdfChar {
	public TextPosition textPosition;
	public int rgbColor;
	public boolean isBold = false;
	public boolean isItalic = false;
	public boolean isUnderlined = false;
	public String fontName;
	public PdfGlyph pdfGlyph;
	public float fontSizeInPt;

	public PdfChar(TextPosition textPosition, PdfGlyph pdfGlyph) {
		this.pdfGlyph = pdfGlyph;
		this.textPosition = textPosition;
		this.fontSizeInPt=textPosition.getFontSizeInPt();
	}

	public String toString() {
		Color color = new Color(rgbColor);
		return textPosition.getUnicode() + " " + fontName + (isBold ? " bold" : "") + (isItalic ? " italic" : "") + (isUnderlined ? " underline" : "") + " rgb:" + color.getRed() + "," + color.getGreen() + "," + color.getBlue();
	}

	public boolean isVisibleChar() {
		if (!StringUtil.isVisibleString(textPosition.getUnicode())) {
			return false;
		}

		// sometimes pdf including chars which are not detected by StringUtil.isVisibleString, but get not rendered
		Rectangle rect = pdfGlyph.getRectangle();
		if (rect.getWidth() <= 1 && rect.getHeight() <= 1) {
			return false;
		}

		return true;
	}


}