package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;

public class PdfGlyph extends AbstractPdfElement {
	public Shape shape;
	public final static String TYPE_NAME="glyph";
	
	public PdfGlyph(Shape shape) {
		this.shape = shape;
	}

	@Override
	public Rectangle getRectangle() {
		return shape.getBounds();
	}

	@Override
	public String getTypeName() {
		return "glyph";
	}

	@Override
	public Color getColor() {
		return Color.magenta;
	}

}