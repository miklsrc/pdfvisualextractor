package pdf.elements;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

public class PdfPage {
	public BufferedImage pageImage = null;
	
	public List<PdfString> pdfStrings = new LinkedList<>();
	public List<PdfImage> pdfImages = new LinkedList<>();
	public List<PdfParagraph> pdfParagraphs = new LinkedList<>();
	public List<PdfPicture> pdfPictures = new LinkedList<>();
	public List<PdfGlyph> pdfGlyphs = new LinkedList<>();
	public List<PdfRectangle> pdfRectangles = new LinkedList<>();
	public List<PdfUnderline> pdfUnderlines = new LinkedList<>();
	public List<PdfComposition> pdfCompositions = new LinkedList<>();

	public List<PdfLinepath> pdfLinePaths= new LinkedList<>();


	public List<AbstractPdfElement> getBoundaryElements() {
		List<AbstractPdfElement> boundaryElements = new LinkedList<AbstractPdfElement>();
		util.Collection.addAllToList(boundaryElements, pdfImages);
		util.Collection.addAllToList(boundaryElements, pdfStrings);
		util.Collection.addAllToList(boundaryElements, pdfParagraphs);
		util.Collection.addAllToList(boundaryElements, pdfPictures);
		util.Collection.addAllToList(boundaryElements, pdfCompositions);

//		util.Collection.addAllToList(boundaryElements, pdfGlyphs);
		util.Collection.addAllToList(boundaryElements, pdfLinePaths);
		util.Collection.addAllToList(boundaryElements, pdfRectangles);
		util.Collection.addAllToList(boundaryElements, pdfUnderlines);

		return boundaryElements;
	}

}