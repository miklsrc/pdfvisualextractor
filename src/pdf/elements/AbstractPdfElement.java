package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.HashSet;

public abstract class AbstractPdfElement
{
	public HashSet<String> annotations = new HashSet<>();
	
	
	public abstract Rectangle getRectangle();
	public abstract String getTypeName();
	public abstract Color getColor();
	
	protected String createAnnotationsStr() {
		String str = util.Collection.implode(",", this.annotations);
		if (str.length() > 0) {
			str = "        [" + str + "]";
		}
		return str;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getColor() == null) ? 0 : getColor().hashCode());
		result = prime * result + ((getRectangle() == null) ? 0 : getRectangle().hashCode());
		result = prime * result + ((getTypeName() == null) ? 0 : getTypeName().hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractPdfElement other = (AbstractPdfElement) obj;
		if (getColor() == null) {
			if (other.getColor() != null)
				return false;
		} else if (!getColor().equals(other.getColor()))
			return false;
		if (getRectangle() == null) {
			if (other.getRectangle() != null)
				return false;
		} else if (!getRectangle().equals(other.getRectangle()))
			return false;
		if (getTypeName() == null) {
			if (other.getTypeName() != null)
				return false;
		} else if (!getTypeName().equals(other.getTypeName()))
			return false;
		return true;
	}
	
	
}