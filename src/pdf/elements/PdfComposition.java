package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import util.Image;

public class PdfComposition extends AbstractPdfElement {

	private Rectangle rectangle;
	public BufferedImage compositionImage;
	public final static String TYPE_NAME="composition";

	
	public PdfComposition(BufferedImage pdfImage,Rectangle compositionArea) {
		rectangle=compositionArea;
		
		compositionImage = Image.getSubImage(pdfImage, compositionArea);
	}


	
	@Override
	public Rectangle getRectangle() {
		return rectangle;
	}
	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}
	@Override
	public Color getColor() {
		return Color.GREEN;
	}

	@Override
	public String toString() {
		return rectangle+createAnnotationsStr();
	}
	
	

}
