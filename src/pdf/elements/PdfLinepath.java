package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Shape;

public class PdfLinepath extends AbstractPdfGeometryElement {


	public Shape linePath; 	//usually a GeneralPath Object
	public final static String TYPE_NAME="linepath";

	public PdfLinepath(Shape linePath, int rgb) {
		this.linePath = linePath;
		rgbColor=rgb;
	}

	/* 
	 * !ATTENTION! this method only returns the bounding box rectangle of the line path!
	 */
	@Override
	public Rectangle getRectangle() {
		return linePath.getBounds();
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public Color getColor() {
		return Color.magenta;
	}

	@Override
	public String toString() {
		
		return getRectangle()+"("+super.toString()+")";
	}
	
	
}
