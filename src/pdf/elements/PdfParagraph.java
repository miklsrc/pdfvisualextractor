package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pdf.text.ParagraphTextExtractor;
import pdf.text.ParagraphTextExtractor.PdfStreamChar;
import util.Geometry;

public class PdfParagraph extends AbstractPdfElement {

	public List<PdfString> pdfStrings = new LinkedList<PdfString>();
	public int[] pdfStringIds; // saving for later persistence
	public boolean[] additionalSpaces; // spaced between the pdfStrings
	public final static String TYPE_NAME = "paragraph";
	private ParagraphTextExtractor paraTextBeautifier;
	private Rectangle rectangle;

	public PdfParagraph(List<PdfString> origPdfStrings, int[] selectedIndices, ParagraphTextExtractor paraTextBeautifier) {
		this.paraTextBeautifier = paraTextBeautifier;
		this.pdfStringIds = selectedIndices;

		if (selectedIndices.length > 0) {
			for (int stringId : selectedIndices) {
				pdfStrings.add(origPdfStrings.get(stringId));
			}
			additionalSpaces = new boolean[selectedIndices.length - 1];
			Arrays.fill(additionalSpaces, Boolean.FALSE);
		} else {
			throw new RuntimeException("no selectedIndices");
		}

		List<Rectangle> rects = new LinkedList<Rectangle>();
		for (PdfString pdfString : pdfStrings) {
			rects.add(pdfString.getRectangle());
		}
		this.rectangle = Geometry.merge(rects);
	}

	

	@Override
	public Rectangle getRectangle() {
		return rectangle;
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public Color getColor() {
		return Color.BLUE;
	}

	@Override
	public String toString() {
		return "|" + getParagraphText() + "|" + createAnnotationsStr();
	}

	public int[] getPdfStringIds() {
		return pdfStringIds;
	}

	public LinkedList<PdfStreamChar> getPdfChars()
	{
		return paraTextBeautifier.extractPdfCharList(this);
	}
	
	public String getParagraphText() {
		return paraTextBeautifier.extractText(this);
	}

}
