package pdf.elements;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;

//http://stackoverflow.com/questions/39962563/detect-bold-italic-and-strike-through-text-using-pdfbox-with-vb-net
public class PdfRectangle extends AbstractPdfGeometryElement {

	public final static String TYPE_NAME = "rectangle";
	public final Point2D[] points=new Point2D[4];
	public PdfRectangle(Point2D p0, Point2D p1, Point2D p2, Point2D p3, int rgb) {
		points[0] = p0;
		points[1] = p1;
		points[2] = p2;
		points[3] = p3;
		rgbColor=rgb;
	}

	@Override
	public Rectangle getRectangle() {
		Rectangle2D boundingRect = pointToRectangle(points[0]);
		boundingRect = boundingRect.createUnion(pointToRectangle(points[1]));
		boundingRect = boundingRect.createUnion(pointToRectangle(points[2]));
		boundingRect = boundingRect.createUnion(pointToRectangle(points[3]));
		return boundingRect.getBounds();
	}

	private Double pointToRectangle(Point2D p) {
		return new Rectangle2D.Double(p.getX(), p.getY(), 0, 0);
	}

	@Override
	public String getTypeName() {
		return TYPE_NAME;
	}

	@Override
	public Color getColor() {
		return Color.magenta;
	}

	@Override
	public String toString() {
		return getRectangle()+"("+super.toString()+")";
	}
}
