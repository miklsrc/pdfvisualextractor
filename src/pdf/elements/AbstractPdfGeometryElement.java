package pdf.elements;

import java.awt.Color;

public abstract class AbstractPdfGeometryElement extends AbstractPdfElement {

	public int rgbColor;

	@Override
	public String toString() {
		Color color = new Color(rgbColor);
		return color.getRed()+","+color.getGreen()+","+color.getBlue();
	}

	

}