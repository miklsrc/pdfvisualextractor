package pdf;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.pdfbox.contentstream.PDFGraphicsStreamEngine;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingColor;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingColorN;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingColorSpace;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingDeviceCMYKColor;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingDeviceGrayColor;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingDeviceRGBColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingColorN;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingColorSpace;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingDeviceCMYKColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingDeviceGrayColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingDeviceRGBColor;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.image.PDImage;

import pdf.elements.PdfLinepath;
import pdf.elements.PdfRectangle;

//http://stackoverflow.com/questions/39962563/detect-bold-italic-and-strike-through-text-using-pdfbox-with-vb-net
public class PdfGeometricsCollector extends PDFGraphicsStreamEngine {

	private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private List<PdfRectangle> rects = new LinkedList<>();
	private AffineTransform pageTransformation;
	private PdfRectangle lastRectangle;
    private GeneralPath linePath = new GeneralPath();
    private List<PdfLinepath> linePaths = new LinkedList<>();

	protected PdfGeometricsCollector(PDPage page) {
		super(page);
		addOperator(new SetStrokingColorSpace());
		addOperator(new SetNonStrokingColorSpace());
		addOperator(new SetStrokingDeviceCMYKColor());
		addOperator(new SetNonStrokingDeviceCMYKColor());
		addOperator(new SetNonStrokingDeviceRGBColor());
		addOperator(new SetStrokingDeviceRGBColor());
		addOperator(new SetNonStrokingDeviceGrayColor());
		addOperator(new SetStrokingDeviceGrayColor());
		addOperator(new SetStrokingColor());
		addOperator(new SetStrokingColorN());
		addOperator(new SetNonStrokingColor());
		addOperator(new SetNonStrokingColorN());

		pageTransformation = PageTransformation.createPageTransformation(page);
		
		try {
			processPage(page);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<PdfRectangle> getRects() {
		return rects;
	}
	
	public List<PdfLinepath> getLinePaths() {
		return linePaths;
	}

	@Override
	public void appendRectangle(Point2D p0, Point2D p1, Point2D p2, Point2D p3) throws IOException {
		logger.log(java.util.logging.Level.INFO, "appendRectangle before 1: " + p0 + " " + p1 + " " + p2 + " " + p3 + " ");
		pageTransformation.transform(p0, p0);
		pageTransformation.transform(p1, p1);
		pageTransformation.transform(p2, p2);
		pageTransformation.transform(p3, p3);

		logger.log(java.util.logging.Level.INFO, "appendRectangle after 1: " + p0 + " " + p1 + " " + p2 + " " + p3 + " ");

		lastRectangle = new PdfRectangle(p0, p1, p2, p3, getCurrentColor());
	}

	private int getCurrentColor() throws IOException {
		int rgb = 0;
		PDColor color = getGraphicsState().getNonStrokingColor();

		try {//this get thrown because org.apache.pdfbox.pdmodel.graphics.color.PDPattern.toRGB(PDPattern.java:95) is not overriden
			rgb = color.toRGB();
		} catch (UnsupportedOperationException e) {
			rgb = -1;
		}
		return rgb;
	}

	@Override
	public void drawImage(PDImage pdImage) throws IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public void clip(int windingRule) throws IOException {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.INFO, "clip");
	}

	@Override
	public void moveTo(float x, float y) throws IOException {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.INFO, "moveTo:"+x+","+y);
		linePath.moveTo(x, y);
	}

	@Override
	public void lineTo(float x, float y) throws IOException {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.INFO, "lineTo:"+x+","+y);
		linePath.lineTo(x, y);
	}

	@Override
	public void curveTo(float x1, float y1, float x2, float y2, float x3, float y3) throws IOException {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.INFO, "curveTo");
		linePath.curveTo(x1, y1, x2, y2, x3, y3);
	}

	@Override
	public Point2D getCurrentPoint() throws IOException {
		return linePath.getCurrentPoint();
	}

	@Override
	public void closePath() throws IOException {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.INFO, "closePath");
		linePath.closePath();
	}

	@Override
	public void endPath() throws IOException {
		logger.log(java.util.logging.Level.INFO, "endPath");

		lastRectangle = null;
		linePath.reset();
	}

	@Override
	public void strokePath() throws IOException {
		logger.log(java.util.logging.Level.INFO, "strokePath");
		addLastRectangle();
		
		//draw
		linePaths.add(new PdfLinepath(pageTransformation.createTransformedShape(linePath),getCurrentColor()));
		linePath.reset();
	}

	@Override
	public void fillPath(int windingRule) throws IOException {
		logger.log(java.util.logging.Level.INFO, "fillPath");
		addLastRectangle();
		
		//draw
		linePaths.add(new PdfLinepath(pageTransformation.createTransformedShape(linePath),getCurrentColor()));
		linePath.reset();
	}

	private void addLastRectangle() {
		logger.log(java.util.logging.Level.INFO, "lastRectangle:" + lastRectangle);
		if (lastRectangle != null)
			rects.add(lastRectangle);
	}

	@Override
	public void fillAndStrokePath(int windingRule) throws IOException {
		addLastRectangle();
	}

	@Override
	public void shadingFill(COSName shadingName) throws IOException {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.INFO, "shadingFill");
	}
}
