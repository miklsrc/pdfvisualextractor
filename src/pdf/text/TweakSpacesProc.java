package pdf.text;

import java.util.LinkedList;
import java.util.List;

import pdf.elements.PdfParagraph;
import pdf.text.ParagraphTextExtractor.PdfStreamChar;
import util.StringUtil;

public class TweakSpacesProc implements TextProcessor {

	public static final String TYPE_NAME="Tweak Spaces"; 
	
	@Override
	public void process(LinkedList<PdfStreamChar> str, PdfParagraph pdfParagraph) {
		normalizeWhitespaces(str);
		removeDuplicateWhitespaces(str);
		PdfStreamCharUtils.trimFront(str);
		PdfStreamCharUtils.trimEnd(str);
	}

	private void normalizeWhitespaces(LinkedList<PdfStreamChar> str) {
		for (PdfStreamChar pdfStreamChar : str) {
			pdfStreamChar.charText = util.StringUtil.replaceWhiteSpaces(pdfStreamChar.charText, ' ');
		}

	}

	private void removeDuplicateWhitespaces(List<PdfStreamChar> str) {
		int curPos = 0;
		boolean lastIsWhite = false;
		while (str.size() > curPos) {
			String c=str.get(curPos).charText;
			boolean isWhite = StringUtil.isWhitespace(str.get(curPos).charText.codePointAt(0));
			if (lastIsWhite && isWhite) {
				str.remove(curPos);
			} else {
				curPos++;
			}
			lastIsWhite = isWhite;
		}
	}
}
