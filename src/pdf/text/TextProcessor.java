package pdf.text;

import java.util.LinkedList;

import pdf.elements.PdfParagraph;
import pdf.text.ParagraphTextExtractor.PdfStreamChar;

public interface TextProcessor {

	void process(LinkedList<PdfStreamChar> str, PdfParagraph pdfParagraph);

}

