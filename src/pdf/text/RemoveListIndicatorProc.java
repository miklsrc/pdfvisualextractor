package pdf.text;

import java.util.LinkedList;
import java.util.List;

import annotation.definitions.Annotations;
import pdf.elements.PdfParagraph;
import pdf.text.ParagraphTextExtractor.PdfStreamChar;

public class RemoveListIndicatorProc implements TextProcessor {

	private List<String> pdfSpecificListIndicators;
	public static final String TYPE_NAME="Remove List/Enumeration/Arrow"; 
	
	
	public RemoveListIndicatorProc(List<String> pdfSpecificListIndicators) {
		this.pdfSpecificListIndicators=pdfSpecificListIndicators;
	}

	@Override
	public void process(LinkedList<PdfStreamChar> str, PdfParagraph pdfParagraph) {
		if (pdfParagraph.annotations.contains(Annotations.REMOVE_IDENTIFIER) && str.size() >= 2) {
			PdfStreamCharUtils.trimFront(str);
			if (!tryRemoveLeadingNumber(str)) {
				tryRemoveLeadingBullet(str);
			}
			PdfStreamCharUtils.trimFront(str);
		}
	}

	private int startsWithPdfSpecificListindicator(LinkedList<PdfStreamChar> str) {
		outer: for (String specificListIndicator : pdfSpecificListIndicators) {
			if (str.size() > specificListIndicator.length()) {
				int i;
				for (i = 0; i < specificListIndicator.length(); i++) {
					int listIndicatorCh = specificListIndicator.codePointAt(i);
					int ch = str.get(i).charText.codePointAt(0);
					if (listIndicatorCh != ch) {
						continue outer;
					}
				}
				return i;
			}
		}
		return 0;
	}

	private boolean tryRemoveLeadingBullet(LinkedList<PdfStreamChar> str) {

		int result = startsWithPdfSpecificListindicator(str);
		if (result > 0) {
			for (int i = 0; i < result; i++) {
				str.removeFirst();
			}
			return true;
		}

		int firstChar = str.getFirst().charText.codePointAt(0);
		if (!Character.isAlphabetic(firstChar) && !Character.isDigit(firstChar)) {
			str.removeFirst();
			return true;
		}
		return false;
	}

	private boolean tryRemoveLeadingNumber(LinkedList<PdfStreamChar> str) {
		boolean removed = false;

		// check if the first chars are digits (can be dot-separated like "2.1.3")
		while (str.size() > 0 && (Character.isDigit(str.getFirst().charText.codePointAt(0)) || (str.size() > 2 && str.getFirst().charText.equals(".") && Character.isDigit(str.get(1).charText.codePointAt(0)))

		)) {
			str.removeFirst();
			removed = true;
		}
		if (removed == true) {
			// check if invisible char is whitespace or dot
			int firstChar = str.getFirst().charText.codePointAt(0);
			if (!str.getFirst().isVisible() && str.size() > 1) {
				str.removeFirst();
			} else if (firstChar == '.' && str.size() > 1) {
				str.removeFirst();
			}
			firstChar = str.getFirst().charText.codePointAt(0);
			if (firstChar == ')' && str.size() > 1) {
				str.removeFirst();
			}
		}
		return removed;
	}
}
