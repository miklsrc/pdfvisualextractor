package pdf.text;

import java.util.LinkedList;

import pdf.text.ParagraphTextExtractor.PdfStreamChar;

public class PdfStreamCharUtils {

	public static void trimFront(LinkedList<PdfStreamChar> str) {
		while (str.size() > 0) {
			PdfStreamChar pdfChar = str.getFirst();
			if (!pdfChar.isVisible()) {
				str.removeFirst();
				continue;
			}
			break;
		}
	}
	
	public static  void trimEnd(LinkedList<PdfStreamChar> str) {
		while (str.size() > 0) {
			PdfStreamChar pdfChar = str.getLast();
			if (!pdfChar.isVisible()) {
				str.removeLast();
				continue;
			}
			break;
		}
	}
}
