package pdf.text;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pdf.elements.PdfChar;
import pdf.elements.PdfParagraph;
import pdf.elements.PdfString;
import util.StringUtil;

public class ParagraphTextExtractor {

	
	public List<TextProcessor> paragraphTextProcessors = new ArrayList<>();

	public static class PdfStreamChar {
		public String charText;
		public PdfChar pdfChar;

		public PdfStreamChar(String charText, PdfChar pdfChar) {
			this.charText = charText;
			this.pdfChar = pdfChar;
		}

		public boolean isVisible() {
			boolean visible1 = !StringUtil.isWhitespace(charText.codePointAt(0));
			boolean visible2 = pdfChar != null && pdfChar.isVisibleChar();
			return visible1 && visible2;
		}
	}

	public LinkedList<PdfStreamChar> extractPdfCharList(PdfParagraph pdfParagraph) {
		LinkedList<PdfStreamChar> str = createPdfCharStream(pdfParagraph);

		for (TextProcessor textProcessor : paragraphTextProcessors) {
			textProcessor.process(str, pdfParagraph);
		}
		return str;
	}

	public String extractText(PdfParagraph pdfParagraph) {
		LinkedList<PdfStreamChar> str = extractPdfCharList(pdfParagraph);

		// serialize as String for output
		String output = "";
		for (PdfStreamChar keyValuePair : str) {
			output += keyValuePair.charText;
		}
		return util.StringUtil.trim(output);
	}

	private LinkedList<PdfStreamChar> createPdfCharStream(PdfParagraph pdfParagraph) {
		LinkedList<PdfStreamChar> str = new LinkedList<>();
		for (int i = 0; i < pdfParagraph.pdfStrings.size(); i++) {
			if (i >= 1) {
				if (pdfParagraph.additionalSpaces[i - 1]) {
					str.add(new PdfStreamChar(" ", null));
				}
			}
			PdfString pdfString = pdfParagraph.pdfStrings.get(i);

			for (PdfChar pdfChar : pdfString.getPdfChars()) {
				str.add(new PdfStreamChar(pdfChar.textPosition.getUnicode().substring(0, 1), pdfChar));
			}
		}
		return str;
	}

}
