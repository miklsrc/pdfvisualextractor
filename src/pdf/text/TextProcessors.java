package pdf.text;

import logic.PdfConnection;


public class TextProcessors {
	
	public static String[] processorLabels = new String[]{TweakSpacesProc.TYPE_NAME,RemoveListIndicatorProc.TYPE_NAME};

	public static TextProcessor create(String textProcessorLabel, PdfConnection pdfConnection) {
		
		switch (textProcessorLabel) {
		case TweakSpacesProc.TYPE_NAME:
			return new TweakSpacesProc();
		case RemoveListIndicatorProc.TYPE_NAME:
			return new RemoveListIndicatorProc(pdfConnection.pdfSpecificListIndicators);
		}
		throw new RuntimeException("unknown TextProcessor");
	}

	
}
