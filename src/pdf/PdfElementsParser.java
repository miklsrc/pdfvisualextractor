package pdf;

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingColor;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingColorN;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingColorSpace;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingDeviceCMYKColor;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingDeviceGrayColor;
import org.apache.pdfbox.contentstream.operator.color.SetNonStrokingDeviceRGBColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingColorN;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingColorSpace;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingDeviceCMYKColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingDeviceGrayColor;
import org.apache.pdfbox.contentstream.operator.color.SetStrokingDeviceRGBColor;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDCIDFontType2;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDSimpleFont;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType3CharProc;
import org.apache.pdfbox.pdmodel.font.PDType3Font;
import org.apache.pdfbox.pdmodel.font.PDVectorFont;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.util.Vector;

import logic.TextBoundingCalc;
import pdf.elements.AbstractPdfGeometryElement;
import pdf.elements.PdfChar;
import pdf.elements.PdfGlyph;
import pdf.elements.PdfImage;
import pdf.elements.PdfPage;
import pdf.elements.PdfString;
import pdf.processing.BoldChecker;
import pdf.processing.FontChecker;
import pdf.processing.ItalicChecker;
import pdf.processing.PdfCharProcessor;
import pdf.processing.PositionTextfragmentExtractor;
import pdf.processing.TextColorChecker;
import pdf.processing.TextfragmentExtractor;
import pdf.processing.UnderlineChecker;
import util.KeyValuePair;

public class PdfElementsParser extends PDFTextStripper {

	public TextfragmentExtractor textfragmentExtractor;

	private List<PdfChar> pdfChars = new LinkedList<>();
	private PdfPage pdfPage = new PdfPage();
	private Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private List<PdfCharProcessor> PdfCharProcessors = new LinkedList<>();
	private PDDocument document;
	private int scale;
	private AffineTransform pageTransformation; // flips and rotates elements

	private PdfGlyph lastShowdGlyph; // saves the currently processed glyph

	public PdfElementsParser(PDDocument document) throws IOException {
		this(document, 1);
	}

	public PdfElementsParser(PDDocument document, int scale) throws IOException {
		this.document = document;
		this.scale = scale;

		setupOperators();
		loadDefaultProcessingStrategies();
	}

	private void loadDefaultProcessingStrategies() {
		addProcessTextPositionListener(new BoldChecker(this));
		addProcessTextPositionListener(new ItalicChecker());
		addProcessTextPositionListener(new UnderlineChecker());
		addProcessTextPositionListener(new TextColorChecker(this));
		addProcessTextPositionListener(new FontChecker());

		textfragmentExtractor = new PositionTextfragmentExtractor();
		// textfragmentExtractor = new SimpleTextfragmentExtractor();
	}

	private void setupOperators() {
		// color recognition
		addOperator(new SetStrokingColorSpace());
		addOperator(new SetNonStrokingColorSpace());
		addOperator(new SetStrokingDeviceCMYKColor());
		addOperator(new SetNonStrokingDeviceCMYKColor());
		addOperator(new SetNonStrokingDeviceRGBColor());
		addOperator(new SetStrokingDeviceRGBColor());
		addOperator(new SetNonStrokingDeviceGrayColor());
		addOperator(new SetStrokingDeviceGrayColor());
		addOperator(new SetStrokingColor());
		addOperator(new SetStrokingColorN());
		addOperator(new SetNonStrokingColor());
		addOperator(new SetNonStrokingColorN());
	}

	private void addProcessTextPositionListener(PdfCharProcessor listener) {
		PdfCharProcessors.add(listener);
	}

	private PdfImage createPdfImage(PDImageXObject pdImageXObject) throws IOException {
		BufferedImage image = pdImageXObject.getImage();

		// { don't know if this code is still required...copied from PageDraver
		// class
		COSBase transfer = getGraphicsState().getTransfer();
		if (transfer instanceof COSArray || transfer instanceof COSDictionary) {
			image = PdfElementsParserHelper.applyTransferFunction(image, transfer);
			logger.log(Level.INFO, "interesting... never called before...");
		}
		// }

		Matrix ctm = getGraphicsState().getCurrentTransformationMatrix();
		AffineTransform imageTransform = ctm.createAffineTransform();

		// extract rotation from imageTransform before scaling and translation
		double imageTransformRotationRad = Math.atan2(imageTransform.getShearY(), imageTransform.getScaleY());
		double pageRotationRad = Math.toRadians(this.getCurrentPage().getRotation());
		double rotationDifRad = pageRotationRad - imageTransformRotationRad;

		// setup image transformation... I have no clue what it is doing..
		// copied from PageDraver class
		int width = image.getWidth(null);
		int height = image.getHeight(null);
		imageTransform.scale(1.0 / width, -1.0 / height);
		imageTransform.translate(0, -height);

		// calculate image bounding box
		Rectangle imageBoundingBox = util.Image.toRect(pdImageXObject);
		Shape helperShape = imageTransform.createTransformedShape(imageBoundingBox); // apply image transformation
		helperShape = pageTransformation.createTransformedShape(helperShape); // apply page transformation
		imageBoundingBox = helperShape.getBounds();

		BufferedImage rotatedImage = util.Image.rotate(image, rotationDifRad);

		PdfImage pdfImage = new PdfImage("", (float) imageBoundingBox.getMinX(), (float) imageBoundingBox.getMinY(), (float) imageBoundingBox.getWidth(), (float) imageBoundingBox.getHeight(), rotatedImage);

		return pdfImage;
	}

	@Override
	protected void processOperator(Operator operator, List<COSBase> operands) throws IOException {
		String operation = operator.getName();

		if ("Do".equals(operation)) {
			COSName objectName = (COSName) operands.get(0);
			PDXObject xobject = getResources().getXObject(objectName);

			if (xobject instanceof PDImageXObject) {
				PDImageXObject imageObject = (PDImageXObject) xobject;

				PdfImage pdfImage = createPdfImage(imageObject);
				pdfImage.objectName = objectName.getName();
				pdfPage.pdfImages.add(pdfImage);

			} else if (xobject instanceof PDFormXObject) {
				PDFormXObject form = (PDFormXObject) xobject;
				showForm(form);
			} 
		} else {
			super.processOperator(operator, operands);
		}
	}

	@Override
	protected void showGlyph(Matrix textRenderingMatrix, PDFont font, int code, String unicode, Vector displacement) throws IOException {

		// show actual glyph bounds. This must be done here and not in
		// writeString(),
		// because writeString processes only the glyphs with unicode,
		// see e.g. the file in PDFBOX-3274
		Shape glyphShape = calculateGlyphBounds(textRenderingMatrix, font, code);

		if (glyphShape != null) {
			glyphShape = pageTransformation.createTransformedShape(glyphShape);

			PdfGlyph pdfGlyph = new PdfGlyph(glyphShape);
			pdfPage.pdfGlyphs.add(pdfGlyph);
			lastShowdGlyph = pdfGlyph;
		}
		super.showGlyph(textRenderingMatrix, font, code, unicode, displacement);
	}

	// this calculates the real individual glyph bounds
	private Shape calculateGlyphBounds(Matrix textRenderingMatrix, PDFont font, int code) throws IOException {
		GeneralPath path = null;
		AffineTransform at = textRenderingMatrix.createAffineTransform();
		at.concatenate(font.getFontMatrix().createAffineTransform());
		if (font instanceof PDType3Font) {
			PDType3Font t3Font = (PDType3Font) font;
			PDType3CharProc charProc = t3Font.getCharProc(code);
			if (charProc != null) {
				PDRectangle glyphBBox = charProc.getGlyphBBox();
				if (glyphBBox != null) {
					path = glyphBBox.toGeneralPath();
				}
			}
		} else if (font instanceof PDVectorFont) {
			PDVectorFont vectorFont = (PDVectorFont) font;
			path = vectorFont.getPath(code);

			if (font instanceof PDTrueTypeFont) {
				PDTrueTypeFont ttFont = (PDTrueTypeFont) font;
				int unitsPerEm = ttFont.getTrueTypeFont().getHeader().getUnitsPerEm();
				at.scale(1000d / unitsPerEm, 1000d / unitsPerEm);
			}
			if (font instanceof PDType0Font) {
				PDType0Font t0font = (PDType0Font) font;
				if (t0font.getDescendantFont() instanceof PDCIDFontType2) {
					int unitsPerEm = ((PDCIDFontType2) t0font.getDescendantFont()).getTrueTypeFont().getHeader().getUnitsPerEm();
					at.scale(1000d / unitsPerEm, 1000d / unitsPerEm);
				}
			}
		} else if (font instanceof PDSimpleFont) {
			PDSimpleFont simpleFont = (PDSimpleFont) font;

			// these two lines do not always work, e.g. for the TT fonts in file 032431.pdf
			// which is why PDVectorFont is tried first.
			String name = simpleFont.getEncoding().getName(code);
			path = simpleFont.getPath(name);
		} else {
			// shouldn't happen, please open issue in JIRA
			System.out.println("Unknown font class: " + font.getClass());
		}
		if (path == null) {
			return null;
		}
		return at.createTransformedShape(path.getBounds2D());
	}

	// http://stackoverflow.com/questions/21430341/identifying-the-text-based-on-the-output-in-pdf-using-pdfbox
	@Override
	protected void processTextPosition(TextPosition text) {
		PdfChar pdfChar = new PdfChar(text, lastShowdGlyph);
		pdfChars.add(pdfChar);
		PdfCharProcessors.forEach(m -> m.process(pdfChar));
		super.processTextPosition(text);
	}

	@Override
	protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
		List<PdfString> textfragments = textfragmentExtractor.extract(textPositions, pdfChars);
		pdfPage.pdfStrings.addAll(textfragments);
	}

	public PdfPage parsePage(int pageNr) throws IOException {
		pdfPage = new PdfPage();

		PDPage pdPage = document.getPage(pageNr);
		
		pageTransformation = PageTransformation.createPageTransformation(pdPage);

		setStartPage(pageNr + 1);
		setEndPage(pageNr + 1);

		PdfCharProcessors.forEach(m -> m.init());

		// don't know how it really works, but this 2 lines starting the parse process
		Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
		writeText(document, dummy);


		PdfGeometricsCollector pdfGeometricsCollector = new PdfGeometricsCollector(pdPage);
		pdfPage.pdfRectangles = pdfGeometricsCollector.getRects();
		pdfPage.pdfLinePaths = pdfGeometricsCollector.getLinePaths();
		
		List<AbstractPdfGeometryElement> pdfGeometries=new LinkedList<>(pdfPage.pdfRectangles);
		pdfGeometries.addAll(pdfPage.pdfLinePaths);
		pdfPage.pdfUnderlines = UnderlineChecker.getUnderlines(pdfGeometries, pdfPage.pdfStrings);

		postProcessCorrectStringBoundingBoxes();
		postProcessTextPositions();
		postProcessStringSort();
		postProcessScale();

		return pdfPage;
	}

	// try to sort strings in two dimensions:
	// 1. sort strings in rows by overlapping heights
	// 2. sort rows by y coordinate
	private void postProcessStringSort() {
		// step 1: sort strings in rows by overlapping string heights
		List<KeyValuePair<Rectangle, List<PdfString>>> rows = new ArrayList<>();
		jmp: for (PdfString curString : pdfPage.pdfStrings) {
			Rectangle strRect = curString.getRectangle();

			// 10% tolerance for comparing the height differences of the strings
			int toleranceY = (int) (strRect.height * 0.1);
			for (KeyValuePair<Rectangle, List<PdfString>> row : rows) {
				Rectangle boundingBoxRow = row.getKey();
				List<PdfString> rowElements = row.getValue();
				// only check vertical dimension
				if (util.Geometry.verticalIntersect(boundingBoxRow, strRect, toleranceY)) {
					rowElements.add(curString);
					row.setKey(boundingBoxRow.union(strRect));
					continue jmp;
				}
			}
			List<PdfString> newRow = new LinkedList<>();
			newRow.add(curString);
			rows.add(new KeyValuePair<Rectangle, List<PdfString>>(strRect, newRow));
		}

		// step 2: merge overlapping rows
		boolean merged;
		do {
			merged = false;
			Iterator<KeyValuePair<Rectangle, List<PdfString>>> row1it = rows.iterator();
			while (row1it.hasNext()) {
				KeyValuePair<Rectangle, List<PdfString>> row1 = row1it.next();
				KeyValuePair<Rectangle, List<PdfString>> overlappingRow = null;
				for (KeyValuePair<Rectangle, List<PdfString>> row2 : rows) {
					if (row1 != row2) {
						if (util.Geometry.verticalIntersect(row1.getKey(), row2.getKey())) {
							overlappingRow = row2;
							break;
						}
					}
				}
				if (overlappingRow != null) {
					merged = false;
					overlappingRow.setKey(overlappingRow.getKey().union(row1.getKey()));
					overlappingRow.getValue().addAll(row1.getValue());
					row1it.remove();
				}
			}
		} while (merged == true);

		// step3: write sorted strings back to pdfStrings
		pdfPage.pdfStrings = new LinkedList<>();
		rows.sort((r1, r2) -> r1.getKey().y - r2.getKey().y);
		for (KeyValuePair<Rectangle, List<PdfString>> row : rows) {
			List<PdfString> rowElements = row.getValue();
			rowElements.sort((x, y) -> x.getRectangle().x - y.getRectangle().x);
			pdfPage.pdfStrings.addAll(rowElements);
		}
	}

	private void postProcessTextPositions() {
		for (PdfChar pdfChar : pdfChars) {
			PdfCharProcessors.forEach(m -> m.postProcess(pdfChar, pdfPage));
		}
	}

	private void postProcessCorrectStringBoundingBoxes() {
		// correct bounding box, extend size to include protruding chars
		TextBoundingCalc tbc = new TextBoundingCalc(pdfPage.pdfGlyphs);
		pdfPage.pdfStrings.forEach(m -> m.shape = tbc.GetBorderForString(m.shape));
	}

	private void postProcessScale() {
		AffineTransform st = new AffineTransform();
		st.scale(scale, scale);
		pdfPage.pdfGlyphs.forEach(m -> m.shape = st.createTransformedShape(m.shape));
		pdfPage.pdfStrings.forEach(m -> m.shape = st.createTransformedShape(m.shape).getBounds2D());
		pdfPage.pdfImages.forEach(m -> m.boundary = st.createTransformedShape(m.boundary).getBounds2D());
		pdfPage.pdfRectangles.forEach(m -> Arrays.stream(m.points).forEach(n -> st.transform(n, n)));
	}

}
