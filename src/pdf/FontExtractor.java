package pdf;

import java.io.IOException;
import java.util.HashSet;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;

public class FontExtractor {

	
	private HashSet<String> embeddedFontNames = new HashSet<>();
	private PDDocument document;

	// http://stackoverflow.com/questions/21392432/how-to-check-fully-embedded-and-subset-embedded-font-using-pdfbox
	// private String getEmbeddedFontName()
	// {
	//
	// }
	
	
	
	public FontExtractor(PDDocument document) {
		this.document = document;
	}
	
	public void analyzeEmbeddedFonts() {
		embeddedFontNames.clear();
		
		PDPageTree pages = document.getPages();
		for (PDPage pdPage : pages) {
			PDResources resources = pdPage.getResources();
			Iterable<COSName> ite = resources.getFontNames();
			for (COSName cosName : ite) {
				PDFont font = null;
				try {
					font = resources.getFont(cosName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				boolean isEmbedded = font.isEmbedded();
				// System.out.println(font.getName()+" "+isEmbedded+"
				// "+font.getFontDescriptor().isForceBold());
				if (isEmbedded)
					embeddedFontNames.add(removeFontPrefixTag(font.getName()));
//				System.out.println("A: "+fontToStr(font));
			}
		}

		for (String string : embeddedFontNames) {
			System.out.println(string);
		}

	}

	// http://stackoverflow.com/questions/16580270/what-are-the-extra-characters-in-the-font-name-of-my-pdf
	public static String removeFontPrefixTag(String embededFontName) {
		if (embededFontName.length()>7 && embededFontName.charAt(6)=='+')
		{
			return embededFontName.substring(7);
		}
		return embededFontName;
	}

}
