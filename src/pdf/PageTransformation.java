package pdf;

import java.awt.geom.AffineTransform;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

public class PageTransformation {

	public static AffineTransform createPageTransformation(PDPage pdPage) {
		PDRectangle pageSize = pdPage.getCropBox();

		// flip y-axis: set up flip transformation
		AffineTransform flipAT = new AffineTransform();
		flipAT.translate(0, pageSize.getHeight());
		flipAT.scale(1, -1);
		flipAT.translate(-pageSize.getLowerLeftX(), -pageSize.getLowerLeftY()); // adjust

		// page may be rotated: set up rotation transformation
		AffineTransform rotateAT = new AffineTransform();
		int rotationAngle = pdPage.getRotation();
		if (rotationAngle != 0) {
			switch (rotationAngle) {
			case 90:
				rotateAT.translate(pageSize.getHeight(), 0);
				break;
			case 270:
				rotateAT.translate(0, pageSize.getWidth());
				break;
			case 180:
				rotateAT.translate(pageSize.getWidth(), pageSize.getHeight());

				break;
			}
			rotateAT.rotate((float) Math.toRadians(rotationAngle));
		}
		
		AffineTransform concatTransform=new AffineTransform(rotateAT );
		concatTransform.concatenate(flipAT);
		return concatTransform;
	}
}
