package pdf.processing;

import java.util.LinkedList;
import java.util.List;

import org.apache.pdfbox.text.TextPosition;

import pdf.elements.PdfChar;
import pdf.elements.PdfString;

public class SimpleTextfragmentExtractor extends TextfragmentExtractor {

	@Override
	public List<PdfString> extract(List<TextPosition> textPositions,List<PdfChar> pdfChars) {
		List<PdfString> textFragments=new LinkedList<>();
		
		LinkedList<PdfChar> str = new LinkedList<PdfChar>();
		TextPosition lastChar = null;
		for (TextPosition curChar : textPositions) {

			PdfChar pdfChar = pdfChars.stream().filter(c->c.textPosition==curChar).findFirst().get();

			if (lastChar != null && (((int) curChar.getYDirAdj()) != ((int) lastChar.getYDirAdj()) || (curChar.getXDirAdj() < lastChar.getXDirAdj()))) {
				addAsPdfString(textFragments,str);
				str = new LinkedList<PdfChar>();
			}

			str.add(pdfChar);
			lastChar = curChar;
		}
		addAsPdfString(textFragments,str);
		
		return textFragments;
	}
	
}
