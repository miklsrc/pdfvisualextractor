package pdf.processing;

import org.apache.pdfbox.text.TextPosition;
import org.apache.pdfbox.util.Matrix;

import pdf.elements.PdfChar;
import pdf.elements.PdfPage;

public class ItalicChecker implements PdfCharProcessor
{

	private boolean isItalic(TextPosition text) {
		// http://stackoverflow.com/questions/26571317/pdfbox-reading-a-pdf-line-by-line-and-extracting-text-properties/26642060#26642060
		Matrix textRenderMatrix = text.getTextMatrix();
		String fontName = text.getFont().getName();

		//it is no enough, to check only x-shearing. In some cases shearing is used to rotate the character.
		//-> subtract the y-shearing and check if there is still some x-shearing left
		float shearX = textRenderMatrix.getShearX();
		float shearY = textRenderMatrix.getShearY();
		float shearDifference = Math.abs(shearX)-Math.abs(shearY);
		boolean textIsSheared = shearDifference > 1.0;
		
		boolean idItalic = text.getFont().getFontDescriptor().isItalic();
		boolean fontNameIsItalic = fontName.toLowerCase().contains("italic");

		boolean isItalic = fontNameIsItalic || idItalic || textIsSheared;
		return isItalic;
	}


	@Override
	public void init() {
		// nothing to do
		
	}

	@Override
	public void process(PdfChar pdfChar) {
		pdfChar.isItalic= isItalic(pdfChar.textPosition);
	}

	@Override
	public void postProcess(PdfChar text, PdfPage pdfPage) {
		// nothing to do
		
	}
}