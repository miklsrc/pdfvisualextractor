package pdf.processing;

import pdf.elements.PdfChar;
import pdf.elements.PdfPage;

public class FontChecker implements PdfCharProcessor {

	@Override
	public void init() {
		// nothing to do

	}

	@Override
	public void process(PdfChar pdfChar) {
		pdfChar.fontName = pdfChar.textPosition.getFont().getName();
	}

	@Override
	public void postProcess(PdfChar pdfChar, PdfPage parseContext) {
		// nothing to do

	}

}
