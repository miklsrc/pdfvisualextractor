package pdf.processing;

import pdf.elements.PdfChar;
import pdf.elements.PdfPage;

public interface PdfCharProcessor {

	void init();
	
	void process(PdfChar pdfChar);
	
	void postProcess(PdfChar pdfChar,PdfPage parseContext);
}
