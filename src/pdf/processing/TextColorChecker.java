package pdf.processing;

import java.io.IOException;

import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.pdmodel.graphics.state.RenderingMode;

import pdf.elements.PdfChar;
import pdf.elements.PdfPage;

public class TextColorChecker implements PdfCharProcessor {

	private PDFStreamEngine pdfStreamEngine;

	public TextColorChecker(PDFStreamEngine pdfStreamEngine) {
		this.pdfStreamEngine = pdfStreamEngine;
	}

	@Override
	public void init() {
		// nothing to do
	}

	@Override
	public void process(PdfChar pdfChar) {
		RenderingMode renderingMode = pdfStreamEngine.getGraphicsState().getTextState().getRenderingMode();
		int rgb = 0;

		try {
			if (renderingMode.isFill()) {
				rgb = pdfStreamEngine.getGraphicsState().getNonStrokingColor().toRGB();
			}
			if (renderingMode.isStroke()) {
				rgb = pdfStreamEngine.getGraphicsState().getStrokingColor().toRGB();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		pdfChar.rgbColor = rgb;
	}

	@Override
	public void postProcess(PdfChar pdfChar, PdfPage parseContext) {
		// nothing to do
	}

}
