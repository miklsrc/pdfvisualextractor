package pdf.processing;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.pdfbox.text.TextPosition;

import pdf.elements.AbstractPdfGeometryElement;
import pdf.elements.PdfChar;
import pdf.elements.PdfPage;
import pdf.elements.PdfString;
import pdf.elements.PdfUnderline;

public class UnderlineChecker implements PdfCharProcessor {

	private HashSet<TextPosition> underlinedChars;

	private static void searchUnderlinedTextpositions(List<AbstractPdfGeometryElement> pdfGeometries, List<PdfString> pdfStrings, List<PdfUnderline> pdfUnderlines, HashSet<TextPosition> underlinedChars) {

		for (AbstractPdfGeometryElement pdfGeom : pdfGeometries) {
			label: for (PdfString pdfString : pdfStrings) {

				List<TextPosition> strTextPositions = pdfString.getPdfChars().stream().map(e -> e.textPosition).collect(Collectors.toList());
				Rectangle2D strBounds = util.Pdf.calculateBoundingBox(strTextPositions);
				Rectangle rectBounds = pdfGeom.getRectangle();

				// 1. check if rectangle located directly below text
				double maxFontSize = strTextPositions.stream().mapToDouble(e -> e.getFontSizeInPt()).max().getAsDouble();
				double textToRectVertDiff = rectBounds.getMinY() - strBounds.getMaxY();
				double maxTextToUnderlineDiff = maxFontSize * 0.30; // own definition: underline must be max 30% of textsize far away

				if (!(textToRectVertDiff > 0 && textToRectVertDiff < maxTextToUnderlineDiff)) {
					continue;
				}

				// 2. Check whether rectangle is small enough to be a line
				if (rectBounds.height > maxTextToUnderlineDiff) {
					continue;
				}

				// 3. Check if underline is located under the string and does
				// not overlap
				if (!(strBounds.getMinX() - maxTextToUnderlineDiff <= rectBounds.getMinX() && strBounds.getMaxX() + maxTextToUnderlineDiff >= rectBounds.getMaxX())) {
					continue;
				}

				// 4. check if underline is not colliding with other rectangles (to ensure that the rectangle is not part of a table construction)
				Rectangle thisBounds = pdfGeom.getRectangle();
				for (AbstractPdfGeometryElement otherPdfGeom : pdfGeometries) {
					Rectangle otherBounds = otherPdfGeom.getRectangle();

					// check if rect is not the same
					if (otherBounds.getX() != thisBounds.getX() || otherBounds.getY() != thisBounds.getY() || otherBounds.getWidth() != thisBounds.getWidth() || otherBounds.getHeight() != thisBounds.getHeight()) {
						// check if not one rect contains the other completely
						if (!otherBounds.contains(thisBounds) && !thisBounds.contains(otherBounds)) {
							if (otherBounds.intersects(thisBounds) && otherPdfGeom.rgbColor==pdfGeom.rgbColor) {
								
								continue label; //TODO: is this label required?
							}
						}
					}
				}

				//
				pdfUnderlines.add(new PdfUnderline(pdfGeom.getRectangle(), pdfGeom.rgbColor));

				for (TextPosition charTextPosition : strTextPositions) {
					if (rectBounds.getMinX() < charTextPosition.getTextMatrix().getTranslateX() && rectBounds.getMaxX() > charTextPosition.getTextMatrix().getTranslateX() + charTextPosition.getWidth() - charTextPosition.getFontSizeInPt() / 10.0) {
						underlinedChars.add(charTextPosition);
					}

				}
			}
		}
	}

	@Override
	public void init() {
		underlinedChars = null;
	}

	@Override
	public void process(PdfChar pdfChar) {
		// nothing to do
	}

	@Override
	public void postProcess(PdfChar pdfChar, PdfPage pdfPage) {
		if (underlinedChars == null) {
			underlinedChars = new HashSet<>();
			List<AbstractPdfGeometryElement> pdfGeometries=new LinkedList<>(pdfPage.pdfRectangles);
			pdfGeometries.addAll(pdfPage.pdfLinePaths);
			searchUnderlinedTextpositions(pdfGeometries, pdfPage.pdfStrings, new LinkedList<>(),underlinedChars);
		}
		boolean contasinUnderline = underlinedChars.contains(pdfChar.textPosition);
		pdfChar.isUnderlined = contasinUnderline;
	}

	public static List<PdfUnderline> getUnderlines(List<AbstractPdfGeometryElement> pdfRectangles, List<PdfString> pdfStrings)
	{
		LinkedList<PdfUnderline> underlines = new LinkedList<>();
		searchUnderlinedTextpositions(pdfRectangles, pdfStrings, underlines,new HashSet<TextPosition>());
		return underlines;
	}
	
}