package pdf.processing;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.pdfbox.text.TextPosition;

import pdf.elements.PdfChar;
import pdf.elements.PdfString;

public class PositionTextfragmentExtractor extends TextfragmentExtractor {

	@Override
	public List<PdfString> extract(List<TextPosition> textPositions, List<PdfChar> pdfChars) {
		
		List<PdfString> textFragments=new LinkedList<>();
		
		// categorize the textPositions by their y-coordinate (~ split different rows)
		HashMap<Float, List<TextPosition>> textPosByYCoord = new HashMap<>();
		for (TextPosition textPosition : textPositions) {
			Float yCoord = new Float(textPosition.getYDirAdj());
			if (!textPosByYCoord.containsKey(yCoord)) {
				textPosByYCoord.put(yCoord, new LinkedList<>());
			}
			textPosByYCoord.get(yCoord).add(textPosition);
		}
		for (List<TextPosition> textPositionsByY : textPosByYCoord.values()) {
			// sort by x-coord
			textPositionsByY.sort(new Comparator<TextPosition>() {
				public int compare(TextPosition o1, TextPosition o2) {
					return Float.compare(o1.getXDirAdj(), o2.getXDirAdj());
				}
			});

			LinkedList<PdfChar> curSubStr = new LinkedList<PdfChar>();
			TextPosition lastChar = null;
			float biggestSpaceSize = 0;
			for (TextPosition curChar : textPositionsByY) {

				PdfChar pdfChar = pdfChars.stream().filter(c->c.textPosition==curChar).findFirst().get();

				if (lastChar != null) {

					float spaceBetween = curChar.getXDirAdj() - (lastChar.getXDirAdj() + lastChar.getWidthDirAdj());

					if (spaceBetween > biggestSpaceSize || ((int) curChar.getYDirAdj()) != ((int) lastChar.getYDirAdj())) {
						addAsPdfString(textFragments,curSubStr);
						curSubStr = new LinkedList<PdfChar>();
						biggestSpaceSize = 0;
					}
				}
				curSubStr.add(pdfChar);
				lastChar = curChar;
				biggestSpaceSize = Math.max(curChar.getWidthOfSpace(), biggestSpaceSize);
			}
			addAsPdfString(textFragments,curSubStr);
		}
		
		return textFragments;
	}

}
