package pdf.processing;

import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.pdfbox.text.TextPosition;

import pdf.elements.PdfChar;
import pdf.elements.PdfString;

public abstract class TextfragmentExtractor {

	private String TextPosListToStr(List<PdfChar> string) {
		String s = "";
		for (PdfChar pdfChar : string) {
			s += pdfChar.textPosition.getUnicode();
		}
		return s;
	}

	public List<List<PdfChar>> explodeList(List<PdfChar> list, List<String> splitter) {
		List<List<PdfChar>> resultList=new LinkedList<>();
		List<PdfChar> listCopy=new LinkedList<PdfChar>(list);
		
		int indexOf;
		
		while ((indexOf=Collections.indexOfSubList(listCopy.stream().map(e->e.textPosition.getUnicode()).collect(Collectors.toList()) , splitter))>=0) {
			if (indexOf>0) {
				resultList.add(listCopy.subList(0, indexOf));
			}
			listCopy=listCopy.subList(indexOf+splitter.size(), listCopy.size());
		}
		if (listCopy.size()>0) {
			resultList.add(listCopy);
		}
		return resultList;
	}
	
	
	protected void addAsPdfString(List<PdfString> textFragments, LinkedList<PdfChar> pdfCharSequence) {

		List<String> splitterList = Arrays.asList(" "," "," "); //= 3 spaces
		List<List<PdfChar>> pdfCharSubsequences = explodeList(pdfCharSequence, splitterList); //split pdfCharSequence when 3 or more spaces are used in a string	 	
	
		for (List<PdfChar> pdfCharSubsequence : pdfCharSubsequences) {
			String pdfStr = TextPosListToStr(pdfCharSubsequence);
			if (pdfStr.trim().length() > 0) {

				List<TextPosition> textPositions = pdfCharSubsequence.stream().map(e -> e.textPosition).collect(Collectors.toList());
				Rectangle2D.Float boundingRect = util.Pdf.calculateBoundingBox(textPositions);
				textFragments.add(new PdfString(pdfStr, boundingRect, pdfCharSubsequence));
			}
		}

	}

	/**
	 * extracts several textfragments from a list of textPositions
	 * 
	 * 
	 * @param textPositions
	 *            pdfbox defines this as a text line of text... problem is sometimes this chars are not the correct order
	 * @param parseContext
	 * @return
	 */
	public abstract List<PdfString> extract(List<TextPosition> textPositions, List<PdfChar> pdfChars);

}