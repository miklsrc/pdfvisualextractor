package pdf.processing;

import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.graphics.state.RenderingMode;
import org.apache.pdfbox.text.TextPosition;

import pdf.elements.PdfChar;
import pdf.elements.PdfPage;

public class BoldChecker implements PdfCharProcessor {
	
 	private PDFStreamEngine pdfStreamEngine;
	
	public BoldChecker(PDFStreamEngine pdfStreamEngine) {
		this.pdfStreamEngine=pdfStreamEngine;
	}

	private boolean isBold(TextPosition text) {
		// http://stackoverflow.com/questions/19770987/how-to-extract-bold-text-from-pdf-using-pdfbox

		PDFont font = text.getFont();
		String fontName = font.getName();
		PDFontDescriptor fontDescriptor = font.getFontDescriptor();
		
		float fontWeight = 0;
		boolean isForcedBold = false;
		if (fontDescriptor != null) {
			fontWeight = fontDescriptor.getFontWeight();
			isForcedBold = fontDescriptor.isForceBold();
		}
		// double lineWidth = getGraphicsState().getLineWidth(); //don't know what to do with it
		RenderingMode renderingMode = pdfStreamEngine.getGraphicsState().getTextState().getRenderingMode();

		boolean fontNameIsBold = fontName.toLowerCase().contains("bold");
		boolean fontWeightIsBold = fontWeight >= 600;
		boolean renderingModeFILL_STROKE = renderingMode == RenderingMode.FILL_STROKE;

		return fontNameIsBold || fontWeightIsBold || renderingModeFILL_STROKE || isForcedBold;
	}

	@Override
	public void init() {
		// nothing to do
	}

	@Override
	public void process(PdfChar pdfChar) {
		pdfChar.isBold = isBold(pdfChar.textPosition);
	}

	@Override
	public void postProcess(PdfChar pdfChar, PdfPage pdfPage) {
		// nothing to do
	}
}